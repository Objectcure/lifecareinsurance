<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.dataTables.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/dataTables.bootstrap4.min.js', dirname(__FILE__) ) ) ?>"></script>
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/bootstrap.min.css', dirname(__FILE__) ) ) ?>">
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/dataTables.bootstrap4.min.css', dirname(__FILE__) ) ) ?>">
<div class="wrap">  
    <div class="container">
        <input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" class="admin-ajax" />
        <div class="row">
            <div class="col-10"><div class="float-left"><h1 class="wp-heading-inline">Rate Cards (Travel Insurance)</h1></div></div>
            <div class="alert alert-success reg-succ">
                <p>Travel card deleted successfully.</p>
            </div> 
            <div class="col-2"><div class="float-right"><a href="admin.php?page=create_rate_card" class="btn btn-success">Add</a></div></div>
        </div>
    </div>
    <div class="container" style="margin-top:30px;">
        <table id ="dtable" class="wp-list-table widefat fixed striped posts">
            <thead>
                <tr>
                    <th width="10%">No.</th> 
                    <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Product</span></th>
                    <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Trip Type</span></th> 
                    <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Age Range</span></th> 
                    <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Amount</span></th>
                    <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Created Date</span></th> 
                    <th width="30%" align="left" scope="col" id="author" class="manage-column">Actions</th>
                </tr>
            </thead>
            <?php
            $sl = 0;
            foreach ($get_cards as $row) {
                $pid = $row->p_id;
                $post = $wpdb->get_row("SELECT * FROM wp_lci_posts WHERE ID=$pid"); 
                $sl++;
                ?>
                <tr id="post-20" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized">
                    <td><?= $sl; ?></td> 
                    <td><?= $post->post_title; ?></td> 
                    <td><?= $row->trip_type; ?></td>
                    <td><?= $row->age_in.'-'.$row->age_bound; ?></td> 
                    <td><?= $row->amount; ?></td>
                    <td><?= $row->created_at; ?></td>
                    <td><a href="#" data-id="<?= $row->id; ?>" class="btn btn-danger delete-rate-card">Delete</a></td> 
                </tr> 
                <?php
            }
            ?>
        </table>
    </div>
</div>
<style>
.reg-succ{display : none;}
</style>
<script>

    $(document).ready(function () {
        jQuery('#dtable').DataTable({
            "pageLength": 30,
            columnDefs: [
                {orderable: false, targets: -1}
            ]
        });
        jQuery(".delete-rate-card").click(function (e) {
            var item_id = $(e.target).attr('data-id');  
            var ajax_url = $('.admin-ajax').val();
            if (confirm('Are you sure to delete this item ?')) { 
                $.ajax({
                    url: ajax_url,
                    data: {
                    'action': 'delete_travel_rate_card',
                    'item_id' : item_id 
                    },
                    type: 'POST',
                    success:function(data) {   
                        if(data == 1){   
                            $('.reg-succ').show();
                                setTimeout(function(){
                                    $('.reg-succ').hide(); 
                                    location.reload();
                                },1000);
                        }else{
                            alert('Server is not responding');
                            location.reload();  
                        }  
                    },
                    error: function(errorThrown){
                        console.log(errorThrown);
                    }
                }); 
            }

        });
    });


</script>