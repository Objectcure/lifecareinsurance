<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery-1.12.4.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.dataTables.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/dataTables.bootstrap.min.js', dirname(__FILE__) ) ) ?>"></script>
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/bootstrap.min.css', dirname(__FILE__) ) ) ?>">
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/dataTables.bootstrap.min.css', dirname(__FILE__) ) ) ?>">
<?php 

$posts_array = get_posts(
    array(
        'posts_per_page' => -1,
        'post_type' => 'page',
        'order' => 'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => 13,
            )
        )
    )
);

//echo '<pre>';print_r($posts_array);

?>
<style>
.reg-succ{ 
  display: none;
}
</style>
<div class="wrap">
    <?php if(!empty($_REQUEST['success']) && $_REQUEST['success'] == 1){?> 
    <div class="alert alert-success">
        <strong>Success!</strong> Order has been approved.
    </div>
    <?php }?>
     <?php if(!empty($_REQUEST['success']) && $_REQUEST['success'] == 2){?> 
    <div class="alert alert-success">
        <strong>Success!</strong> Order has been disapproved.
    </div>
    <?php }?>
    <h1 class="wp-heading-inline">Create Rate Card</h1> 
    <div class="container" style="margin-top:30px;">
        <div class="row">
            <div class="col-md-8 order-md-1">
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" class="admin-ajax" />
                        <input type="hidden" value="<?php echo get_admin_url(); ?>" class="admin-url" />
                        
                        <label for="firstName">Product</label>
                        <select id="product_sel" class="form-control product"> 
                            <option value="">Select Product</option>
                            <?php foreach($posts_array as $page): ?>
                            <option value="<?php echo $page->ID; ?>"><?php echo $page->post_title; ?></option> 
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback product_error">
                            This field is required.
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="firstName">Trip Type</label>
                        <select id="trip_type_travelance" class="form-control trip_type_travelance">
                            <option value="single">Single Trip</option>
                            <option value="multiple">Multi Trip</option>
                        </select> 
                        <select id="trip_type_destination" style="display:none;" class="form-control trip_type_destination">
                            <option value="premier single">Premier - Single Trip</option>
                            <option value="premier annual multiple">Premier - Annual Multi Trip</option>
                            <option value="ultra single">Ultra Preferred - Single Trip</option>
                            <option value="ultra annual multiple">Ultra Preferred - Annual Multi Trip</option>
                            <option value="super single">Super Preferred - Single Multi Trip</option>
                            <option value="super annual multiple">Super Preferred - Annual Multi Trip</option>
                            <option value="elite single multiple">Elite Preferred - Single Multi Trip</option>
                            <option value="elite annual multiple">Elite Preferred - Annual Multi Trip</option>
                        </select> 
                        <select id="trip_type_manulife" style="display:none;" class="form-control trip_type_manulife">
                            <option value="single">Single Trip Emergency Medical</option>
                            <option value="multiple">Multi Trip Emergency Medical</option>
                        </select>
                    </div>
                    <div class="col-md-6 mb-3 trip_type_manulife_category" style="display:none;">
                        <label for="lastName">Select Category</label>
                        <select id="trip_type_manulife_cate" class="form-control trip_type_manulife_cate">
                            <option value="category a">Category A-Applicants under 60 years</option>
                            <option value="category b">Category B-Applicants over 60 years</option>
                            <option value="category c">Category C-Applicants over 60 years</option>
                        </select> 
                    </div>
                    <div class="col-md-6 mb-3 trip_type_tugo_category" style="display:none;">
                        <label for="lastName">Select Destination</label>
                        <select id="trip_type_tugo_cate" class="form-control trip_type_tugo_cate">
                            <option value="canada">Within Canada</option>
                            <option value="international">Worldwide (Excluding USA)</option>
                            <option value="usa">Worldwide</option>
                        </select> 
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="lastName">Age Range</label>
                        <input type="text" class="form-control age_range" id="lastName" placeholder="Age range" value="" />
                        <div class="invalid-feedback age_range_error">
                            This field is required.
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="lastName">Travel Days</label>
                        <input type="text" class="form-control travel_days" id="lastName" placeholder="Travel Days" value="" />
                        <div class="invalid-feedback travel_days_error">
                            This field is required.
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="lastName">Amount (in CAD)</label>
                        <input type="text" class="form-control amount" id="lastName" placeholder="Amount" value="" />
                        <div class="invalid-feedback amount_error">
                            This field is required.
                        </div>
                    </div> 
                    <div class="col-md-6 mb-3">
                        <label for="sum_assured">Sum Assured (in CAD)</label>
                        <input type="text" class="form-control sum_assured" id="sum_assured" placeholder="Sum assured" value="" /> 
                    </div>  
                    <div class="col-md-12 mb-3"> 
                        <label for="lastName"></label>
                        <button type="button" class="btn btn-primary float-right add_rate_card" style="margin-top:30px;">Save</button>
                        <div class="invalid-feedback"></div>
                    </div>
                    <div>
                        <div class="alert alert-success reg-succ" role="alert">
                            Data saved successfully.
                        </div>
                    </div>

                </div> 
            </div>
        </div>
     </div>
     
</div>
<script>
$(document).ready(function () { 
    var trip_type = '';
    var trip_cat = '';
    // Get product select option handler
    $("#product_sel" ).change(function() {
        var product = $('#product_sel').val();
        //alert(product);
        if(product == 59){ 
            $('#trip_type_destination').show();
            $('#trip_type_travelance').hide();
            $('#trip_type_manulife').hide(); 
            $('.trip_type_manulife_category').hide();  
            $('.trip_type_tugo_category').hide();           
        }else if(product == 46){ 
            $('#trip_type_destination').hide();
            $('#trip_type_travelance').show(); 
            $('#trip_type_manulife').hide(); 
            $('.trip_type_manulife_category').hide();
            $('.trip_type_tugo_category').hide();   
        }else if(product == 92){ 
            $('#trip_type_destination').hide();
            $('#trip_type_travelance').hide();
            $('#trip_type_manulife').show(); 
            $('.trip_type_manulife_category').show();
            $('.trip_type_tugo_category').hide();   
        }else if(product == 96){ 
            $('#trip_type_destination').hide();
            $('#trip_type_travelance').show();
            $('#trip_type_manulife').hide(); 
            $('.trip_type_manulife_category').hide();
            $('.trip_type_tugo_category').show(); 
        }
    }); 

    $('.add_rate_card').click(function () {
        var admin_url = $('.admin-url').val();
        var ajax_url = $('.admin-ajax').val();
        // data validation 
        var product = $('#product_sel').val();
        //alert(product);
        if(product == 59){  
            trip_type = $('.trip_type_destination').val();
        }else if(product == 46){  
            trip_type = $('.trip_type_travelance').val();
        }else if(product == 92){  
            trip_type = $('.trip_type_manulife').val();
            trip_cat = $('.trip_type_manulife_cate').val();
        }else if(product == 96){  
            trip_type = $('.trip_type_travelance').val();
            trip_cat = $('.trip_type_tugo_cate').val();
        }

        var product = $('.product').val(); if(product == ''){$('.product_error').show();return false;}else{$('.product_error').hide();}
        //var trip_type = $('.trip_type').val();  
        var age_range = $('.age_range').val(); if(age_range == ''){$('.age_range_error').show();return false;}else{$('.age_range_error').hide();}
        var travel_days = $('.travel_days').val(); if(travel_days == ''){$('.travel_days_error').show();return false;}else{$('.travel_days_error').hide();}
        var amount = $('.amount').val(); if(amount == ''){$('.amount_error').show();return false;}else{$('.amount_error').hide();}
        var sum_assured = $('.sum_assured').val();
        if(sum_assured == ''){sum_assured = 0} 

        $.ajax({
            url: ajax_url,  
            data: {
                'action': 'add_rate_travel_insurance',
                'product' : product,
                'trip_type' : trip_type,
                'age_range' : age_range,
                'trip_cat'  : trip_cat,
                'travel_days' : travel_days,
                'amount' : amount,
                'sum_assured' : sum_assured
            },
            type: 'POST', 
            success:function(data) { 
                if(data == 1){   
                    $('.reg-succ').show();
                    setTimeout(function(){
                        $('.reg-succ').hide(); 
                        //location.reload();
                        window.location.replace(admin_url+'admin.php?page=travel_rate_cards');
                    },2000);
                }else if(data == 0){
                    alert('Server is not responding'); 
                } 
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        }); 

    });
});
</script>
