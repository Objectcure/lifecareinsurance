<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/popper.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/bootstrap.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.dataTables.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/dataTables.bootstrap4.min.js', dirname(__FILE__) ) ) ?>"></script>
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/bootstrap.min.css', dirname(__FILE__) ) ) ?>">
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/dataTables.bootstrap4.min.css', dirname(__FILE__) ) ) ?>">
<div class="wrap">  
    <div class="container">
        <input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" class="admin-ajax" />
        <div class="row">
            <div class="col-10"><div class="float-left"><h1 class="wp-heading-inline">Referal List</h1></div></div> 
            <div class="col-2">
                <div class="float-right">
                    <a href="#" class="btn btn-info" data-toggle="modal" data-target="#exampleModal">Add</a>
                </div>
            </div>
            <div class="alert alert-success reg-succ-del">
                <p>Referal deleted successfully.</p>
            </div> 
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add New Referal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" class="form-control title" placeholder="Title" />
                    </div>
                    <span class="error-title">This is a mandatory field</span>
                    <div class="form-group"> 
                        <label for="">Description</label>
                        <textarea class="form-control description" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div> 
                    <span class="error-description">This is a mandatory field</span>
                    <div class="alert alert-success reg-succ text-center" role="alert">
                        Referal has been saved.
                    </div> 
                </div>
                <div class="modal-footer"> 
                    <a onclick="return false;" class="btn btn-primary save-referal">Save</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="margin-top:30px;">
        <table id ="dtable" class="wp-list-table widefat fixed striped posts">
            <thead>
                <tr>
                    <th width="10%">No.</th> 
                    <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Referal Code</span></th>
                    <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Description</span></th> 
                    <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Created on</span></th> 
                    <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Status</span></th> 
                    <th width="30%" align="left" scope="col" id="author" class="manage-column">Actions</th>
                </tr>
            </thead>
            <?php
            $sl = 0;
            foreach ($get_referals as $row) { 
                if($row->status == 1){ $status = 'Active';}
                $sl++;
                ?>
                <tr id="post-20" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized">
                    <td><?= $sl; ?></td>  
                    <td><?= $row->referal_title; ?></td> 
                    <td><?= $row->description; ?></td>
                    <td><?= $row->created_at; ?></td>
                    <td><span style="color:green;"><?= $status; ?></span></td>
                    <td><a href="#" data-id="<?= $row->id; ?>" class="btn btn-danger delete-referal">Delete</a></td> 
                </tr> 
                <?php
            }
            ?>
        </table>
    </div>
</div>
<style>
.reg-succ, .reg-succ-del {display : none;}   
.error-description, .error-title {display : none; color:red;} 
</style>
<script> 
    $(document).ready(function () {
        jQuery('#dtable').DataTable({
            columnDefs: [
                {orderable: false, targets: -1}
            ]
        });

        $('.save-referal').click(function () {  
            var description = $('.description').val();
            var ajax_url = $('.admin-ajax').val();
            var title = $('.title').val();
            if(title == ''){$('.error-title').show();return false;}else{$('.error-title').hide();} 
            if(description == ''){$('.error-description').show();return false;}else{$('.error-description').hide();} 

            $.ajax({
                url: ajax_url,
                data: {
                    'action': 'add_referal',
                    'description' : description, 
                    'title' : title 
                },
                type: 'POST',
                success:function(data) {   
                    if(data == 1){   
                        $('.description').val('');
                        $('.title').val('');
                        $('.reg-succ').show();
                        setTimeout(function(){
                            $('.reg-succ').hide(); 
                            location.reload();
                        },1000); 
                    }else if(data == 0){
                        alert('Server is not responding');
                        //location.reload();  
                    }  
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            });   
            
        });

        jQuery(".delete-referal").click(function (e) {
            var item_id = $(e.target).attr('data-id');  
            var ajax_url = $('.admin-ajax').val();
            if (confirm('Are you sure to delete this item ?')) { 
                $.ajax({
                    url: ajax_url,
                    data: {
                    'action': 'delete_referal',
                    'item_id' : item_id 
                    },
                    type: 'POST',
                    success:function(data) {   
                        if(data == 1){   
                            $('.reg-succ-del').show();
                            setTimeout(function(){
                                $('.reg-succ-del').hide(); 
                                location.reload();
                            },1000);
                        }else{
                            alert('Server is not responding');
                            location.reload();  
                        }  
                    },
                    error: function(errorThrown){
                        console.log(errorThrown);
                    }
                }); 
            }

        });
    });


</script>