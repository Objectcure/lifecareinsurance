<?php
/**
 * Plugin Name: Life Care Insurance Referal System
 * Plugin URI: www.lifecareinsuarnce.com
 * Description: Plugin for Referal System
 * Version: 1.0
 * Author: Jayaprakash <ideas.to.jp@gmail.com> 
 * 
 * */

class ReferalSystem
{

    public $plugin_name;

    function __construct()
    {
        global $wpdb; 
        $this->plugin_name = plugin_basename(__FILE__);
        //add_filter('the_content', array($this, 'hw_add_post_content'));
        add_action('admin_menu', array($this, 'add_admin_pages'));
        //add_action("add_meta_boxes", array($this, 'add_product_suggestion_meta_box'));
        //add_action("save_post", array($this, 'save_product_suggestion_meta_box'));
        //add_filter('manage_posts_columns', array($this, 'hw_suggestion_columns_head'));
        add_action('manage_posts_custom_column', array($this, 'hw_suggestion_columns_content'), 10, 2);
        add_action('wp_ajax_get_product_suggestion_ajax', array($this, 'get_product_suggestion_ajax')); 
    }

    public function add_admin_pages()  
    {
        add_menu_page('Referral System', 'Referral System', 'manage_options', 'referal_systems', array($this, 'referal_systems'), 'dashicons-networking', 10);
        add_submenu_page( 'referal_systems', 'Referrals', 'Referrals',
        'manage_options', 'referals', array($this, 'referals'), '', 10);  
        add_submenu_page(null, __('view'), __('view'), 'edit_themes', 'create_rate_card', array($this, 'create_rate_card'), 'my_submenu_render'); 
    } 

    function hw_suggestion_columns_content($column_name, $post_ID)
    {
        if ($column_name == 'product_suggestion') {

            global $wpdb;
            $product_suggestion = $wpdb->get_results("select * FROM wp_hw_product_suggestion_relations JOIN wp_hw_product_suggestion ON wp_hw_product_suggestion.id = wp_hw_product_suggestion_relations.product_suggestion_id WHERE post_id =" . $post_ID);
            if (!empty($product_suggestion)) {
                echo '<a href="admin.php?page=hw_product_suggestion_edit&id=' . $product_suggestion[0]->id . '">' . $product_suggestion[0]->name . '</a>';
            } else {
                echo '—';
            }
        }
    }

    function rate_card()
    {
        global $wpdb; 
        $get_cards = $wpdb->get_results("SELECT * FROM crm_rate_card ORDER BY id DESC"); 
        require_once plugin_dir_path(__FILE__) . 'templates/rate_card_list.php';
    }


    function create_rate_card()
    {
        global $wpdb; 
        /*$get_inquiries = $wpdb->get_results("SELECT crm_customers.first_name, crm_customers.last_name,crm_queries.effective_date,crm_queries.id,crm_queries.expiry_date,crm_queries.departure_city FROM crm_queries LEFT JOIN crm_customers ON crm_customers.id = crm_queries.customer_id ORDER BY crm_queries.id DESC"); */
        //require_once plugin_dir_path(__FILE__) . 'templates/add_rate_card.php';
    }

    function customer_details(){
        global $wpdb;
        $customer_id = $_REQUEST['id'];
        $get_customer_details = $wpdb->get_results("SELECT * FROM crm_customers WHERE id=$customer_id"); 
        require_once plugin_dir_path(__FILE__) . 'templates/customer_details.php'; 
    }

    function inquiry_details(){
        global $wpdb;
        $inquiry_id = $_REQUEST['id'];
        $get_inquiry_details = $wpdb->get_results("SELECT crm_customers.first_name, crm_customers.last_name,crm_customers.email,crm_customers.phone,crm_customers.dob,crm_queries.id,crm_queries.effective_date,crm_queries.expiry_date,crm_queries.departure_city,crm_queries.province,crm_queries.benefits,crm_queries.deductable_price,crm_queries.pre_existing_medical,crm_queries.query_status,crm_queries.created_at,crm_queries.query_status FROM crm_queries LEFT JOIN crm_customers ON crm_customers.id = crm_queries.customer_id WHERE crm_queries.id=$inquiry_id");  
        require_once plugin_dir_path(__FILE__) . 'templates/inquiry_details.php'; 
    }

    function edit_contact(){ 
        global $wpdb;
        $customer_id = $_REQUEST['id'];
        $get_customer_details = $wpdb->get_results("SELECT * FROM crm_customers WHERE id=$customer_id"); 
        require_once plugin_dir_path(__FILE__) . 'templates/edit_contact.php'; 
    } 

    function referal_systems(){ 
        global $wpdb; 
        $get_referals = $wpdb->get_results("SELECT * FROM crm_referals ORDER BY id DESC"); 
        require_once plugin_dir_path(__FILE__) . 'templates/referal_list.php'; 
    }

    function referals(){ 
        global $wpdb; 
        $get_referal_activities = $wpdb->get_results("SELECT * FROM crm_referal_activities ORDER BY id DESC"); 
        require_once plugin_dir_path(__FILE__) . 'templates/referal_activities.php'; 
    }

    function create_message(){
        require_once plugin_dir_path(__FILE__) . 'templates/send_message.php';
    }
 

}

if (class_exists('ReferalSystem')) {

    $ReferalSystem = new ReferalSystem();
}


register_activation_hook(__FILE__, array($ReferalSystem, 'activate'));


register_deactivation_hook(__FILE__, array($ReferalSystem, 'deactivate'));
