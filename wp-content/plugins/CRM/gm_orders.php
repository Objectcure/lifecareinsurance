<?php
/**
 * Plugin Name: Life Care Insurance CRM
 * Plugin URI: www.lifecareinsuarnce.com
 * Description: Plugin for Customer Relationship Management (CRM)
 * Version: 1.0
 * Author: Jayaprakash <ideas.to.jp@gmail.com> 
 * 
 * */

class GEMMOrders
{

    public $plugin_name;

    function __construct()
    {
        global $wpdb; 
        $this->plugin_name = plugin_basename(__FILE__);
        //add_filter('the_content', array($this, 'hw_add_post_content'));
        add_action('admin_menu', array($this, 'add_admin_pages'));
        //add_action("add_meta_boxes", array($this, 'add_product_suggestion_meta_box'));
        //add_action("save_post", array($this, 'save_product_suggestion_meta_box'));
        //add_filter('manage_posts_columns', array($this, 'hw_suggestion_columns_head'));
        add_action('manage_posts_custom_column', array($this, 'hw_suggestion_columns_content'), 10, 2);
        add_action('wp_ajax_get_product_suggestion_ajax', array($this, 'get_product_suggestion_ajax')); 
    }

    public function add_admin_pages()  
    {
        add_menu_page('CRM', 'CRM', 'manage_options', 'lci_crm', array($this, 'customers_list'), 'dashicons-id', 10);
        add_submenu_page( 'lci_crm', 'Customers', 'Customers',
        'manage_options', 'crm_customers', array($this, 'customers_list'), null, 10);
        /*add_submenu_page( 'lci_crm', 'Inquiries', 'Inquiries',
        'manage_options', 'crm_inquiries', array($this, 'inquiries_list'), '', 10); */
        add_submenu_page( 'lci_crm', 'Quotes', 'Quotes',
        'manage_options', 'crm_quotes', array($this, 'customers_quotes'), null, 10);
        add_submenu_page(null, __('view'), __('view'), 'edit_themes', 'customer_details', array($this, 'customer_details'), 'my_submenu_render');   
        add_submenu_page(null, __('view'), __('view'), 'edit_themes', 'inquiry_details', array($this, 'inquiry_details'), 'my_submenu_render'); 
        add_submenu_page(null, __('view'), __('view'), 'edit_themes', 'quote_details', array($this, 'quote_details'), 'my_submenu_render');
        add_submenu_page(null, __('view'), __('view'), 'edit_themes', 'applicants_details', array($this, 'applicants_details'), 'my_submenu_render'); 
        add_submenu_page(null, __('view'), __('view'), 'edit_themes', 'edit_contact', array($this, 'edit_contact'), 'my_submenu_render'); 
        add_submenu_page(null, __('view'), __('view'), 'edit_themes', 'notes_list', array($this, 'notes_list'), 'my_submenu_render'); 
        add_submenu_page(null, __('view'), __('view'), 'edit_themes', 'create_message', array($this, 'create_message'), 'my_submenu_render'); 
    } 

    function hw_suggestion_columns_content($column_name, $post_ID)
    {
        if ($column_name == 'product_suggestion') {

            global $wpdb;
            $product_suggestion = $wpdb->get_results("select * FROM wp_hw_product_suggestion_relations JOIN wp_hw_product_suggestion ON wp_hw_product_suggestion.id = wp_hw_product_suggestion_relations.product_suggestion_id WHERE post_id =" . $post_ID);
            if (!empty($product_suggestion)) {
                echo '<a href="admin.php?page=hw_product_suggestion_edit&id=' . $product_suggestion[0]->id . '">' . $product_suggestion[0]->name . '</a>';
            } else {
                echo '—';
            }
        }
    }

    function customers_list()
    {
        global $wpdb; 
        $get_customers = $wpdb->get_results("SELECT * FROM crm_customers ORDER BY id DESC"); 
        require_once plugin_dir_path(__FILE__) . 'templates/customers_list.php';
    }


    function inquiries_list()
    {
        global $wpdb; 
        $get_inquiries = $wpdb->get_results("SELECT crm_customers.first_name, crm_customers.last_name,crm_queries.effective_date,crm_queries.id,crm_queries.expiry_date,crm_queries.departure_city FROM crm_queries LEFT JOIN crm_customers ON crm_customers.id = crm_queries.customer_id ORDER BY crm_queries.id DESC"); 
        require_once plugin_dir_path(__FILE__) . 'templates/inquiries_list.php';
    }

    function customer_details(){
        global $wpdb;
        $customer_id = $_REQUEST['id'];
        $get_customer_details = $wpdb->get_results("SELECT * FROM crm_customers WHERE id=$customer_id"); 
        require_once plugin_dir_path(__FILE__) . 'templates/customer_details.php'; 
    }

    function inquiry_details(){
        global $wpdb;
        $inquiry_id = $_REQUEST['id'];
        $get_inquiry_details = $wpdb->get_results("SELECT crm_customers.first_name, crm_customers.last_name,crm_customers.email,crm_customers.phone,crm_customers.dob,crm_queries.id,crm_queries.effective_date,crm_queries.expiry_date,crm_queries.departure_city,crm_queries.province,crm_queries.benefits,crm_queries.deductable_price,crm_queries.pre_existing_medical,crm_queries.query_status,crm_queries.created_at,crm_queries.query_status FROM crm_queries LEFT JOIN crm_customers ON crm_customers.id = crm_queries.customer_id WHERE crm_queries.id=$inquiry_id");  
        require_once plugin_dir_path(__FILE__) . 'templates/inquiry_details.php'; 
    }

    function edit_contact(){ 
        global $wpdb;
        $customer_id = $_REQUEST['id'];
        $get_customer_details = $wpdb->get_results("SELECT * FROM crm_customers WHERE id=$customer_id"); 
        require_once plugin_dir_path(__FILE__) . 'templates/edit_contact.php'; 
    } 

    function notes_list(){ 
        global $wpdb;
        $inquiry_id = $_REQUEST['id'];
        $get_notes = $wpdb->get_results("SELECT * FROM crm_notes WHERE inquiry_id=$inquiry_id"); 
        require_once plugin_dir_path(__FILE__) . 'templates/notes_list.php'; 
    }

    function create_message(){
        require_once plugin_dir_path(__FILE__) . 'templates/send_message.php';
    }

    function customers_quotes(){
        global $wpdb; 
        $get_customer_quotes = $wpdb->get_results("SELECT * FROM crm_quotes ORDER BY id DESC"); 
        require_once plugin_dir_path(__FILE__) . 'templates/quotes_list.php'; 
    }

    function quote_details(){
        global $wpdb;
        $inquiry_id = $_REQUEST['id'];
        $get_inquiry_details = $wpdb->get_results("SELECT crm_customers.first_name, crm_customers.last_name,crm_customers.email,crm_customers.phone,crm_customers.dob,crm_quotes.id,crm_quotes.quote_id,crm_quotes.dob_1,crm_quotes.pre_existing_1,crm_quotes.dob_2,crm_quotes.pre_existing_2,crm_quotes.trip_type,crm_quotes.trip_destination,crm_quotes.departure_date,crm_quotes.return_date,crm_quotes.created_at,crm_quotes.status FROM crm_quotes LEFT JOIN crm_customers ON crm_customers.id = crm_quotes.customer_id WHERE crm_quotes.id=$inquiry_id");  
        require_once plugin_dir_path(__FILE__) . 'templates/quote_details.php'; 
    }

    function applicants_details(){
        global $wpdb;
        $inquiry_id = $_REQUEST['id'];
        $get_quotes = $wpdb->get_row("SELECT * FROM crm_quotes WHERE quote_id='$inquiry_id' ");
        if(empty($get_quotes->dob_2)){
            $get_quote_details['applicant_1'] = $wpdb->get_results("SELECT * FROM crm_quotes_items WHERE quote_id='$inquiry_id' AND applicant='applicant1' ");  
        }else{
            $get_quote_details['applicant_1'] = $wpdb->get_results("SELECT * FROM crm_quotes_items WHERE quote_id='$inquiry_id' AND applicant='applicant1' ");  
            $get_quote_details['applicant_2'] = $wpdb->get_results("SELECT * FROM crm_quotes_items WHERE quote_id='$inquiry_id' AND applicant='applicant2' ");  
        }
        require_once plugin_dir_path(__FILE__) . 'templates/applicants_details.php'; 
    }
 

}

if (class_exists('GEMMOrders')) {

    $gemmorders = new GEMMOrders();
}


register_activation_hook(__FILE__, array($gemmorders, 'activate'));


register_deactivation_hook(__FILE__, array($gemmorders, 'deactivate'));
