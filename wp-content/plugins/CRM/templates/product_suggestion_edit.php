<div class="wrap">
    <h1 class="wp-heading-inline">Edit Product suggestion</h1>

    <div id="wp-content-editor-tools" class="wp-editor-tools hide-if-no-js" style="position: absolute; top: 0px; width: 847px;">

    </div>


    <form name="post" method="post" id="post">

        <div id="poststuff">
            <div id="post-body" class="metabox-holder columns-2">
                <div id="post-body-content" style="position: relative;">

                    <div id="titlediv">
                        <div id="titlewrap">
                            <p class="label">
                                <label>Product Suggestion Name</label>
                            </p>
                            <input type="hidden" name="product_suggestion_id" size="30" value="<?= $product_suggestions[0]->id ?>" spellcheck="true" autocomplete="off">
                            <input type="text" name="product_suggestion_name" size="30" id="title" value="<?= $product_suggestions[0]->name ?>" spellcheck="true" autocomplete="off">
                        </div>

                    </div>
                    <div>
                        <p class="label">
                            <label for="field-suggestion">Suggestion Details</label>
                        </p>

                        <?php
                        $settings = array(
                            'tinymce' => array(
                                'setup' => 'function (ed) {
                                    tinymce.documentBaseURL = "' . get_admin_url() . '";
                                }',
                            ),
                            'quicktags' => TRUE,
                            'editor_class' => 'frontend-article-editor',
                            'textarea_rows' => 10,
                            'media_buttons' => FALSE,
                        );
                        wp_editor($product_suggestions[0]->value, 'product_suggestion_details', $settings);
                        ?>
                    </div>
                    <div id="publishing-action">
                        <input type="submit" name="publish" id="publish" class="button button-primary button-large push-right" value="Update">
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>

<style>
    textarea {
        width: 100%;
        padding: 5px;
    }

    .label {
        color: #333333;
        font-size: 13px;
        line-height: 1.5em;
        font-weight: bold;
        padding: 0;
        margin: 0 0 3px;
        display: block;
        vertical-align: text-bottom;
    }
</style>

