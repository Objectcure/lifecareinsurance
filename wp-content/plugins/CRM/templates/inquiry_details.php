<script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
<script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> 
<script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css"> 
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">Inquiry Details</h5>
      <nav class="my-2 my-md-0 mr-md-3"> 
        <a href="admin.php?page=create_message&id=<?php echo $get_inquiry_details[0]->id; ?>" class="btn btn-danger">Send Message</a> 
      </nav>
      <nav class="my-2 my-md-0 mr-md-3">
        <a href="admin.php?page=notes_list&id=<?php echo $get_inquiry_details[0]->id; ?>" class="btn btn-secondary">Notes</a> 
      </nav>
      <nav class="my-2 my-md-0 mr-md-3"> 
        <div class="btn-group">
            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Action
            </button>
            <div class="dropdown-menu"> 
            <a class="dropdown-item" href="#" id="Pending" style="color:orange;">Pending 
            <?php if($get_inquiry_details[0]->query_status == 'Pending'){ ?><span class="dashicons dashicons-yes"> <?php } ?></span></a>
            <a class="dropdown-item" href="#" id="Communicated" style="color:purple;">Communicated <?php if($get_inquiry_details[0]->query_status == 'Communicated'){ ?><span class="dashicons dashicons-yes"> <?php } ?></span></a>
            <a class="dropdown-item" href="#" id="Closed" style="color:green;">Closed <?php if($get_inquiry_details[0]->query_status == 'Closed'){ ?><span class="dashicons dashicons-yes"> <?php } ?></span></a>
        </div>  
        <input type="hidden" value="<?php echo $get_inquiry_details[0]->id; ?>" class="inq_id" />
        <input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" class="admin-ajax" />
      </nav> 
        <a class="btn btn-outline-primary" onclick="window.history.go(-1); return false;" href="#">Back</a>
    </div> 

    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="alert alert-success reg-succ" style="display:none;" role="alert"> 
              Status updated successfully.
          </div>
        </div>
        <div class="col-sm-4">
          <?php if($get_inquiry_details[0]->query_status == 'Pending'){ ?>
            <div class="alert alert-warning text-center" role="alert">
                Status : Pending
            </div>
          <?php }elseif($get_inquiry_details[0]->query_status == 'Communicated'){ ?>
            <div class="alert alert-info text-center" role="alert">
                Status : Communicated
            </div> 
          <?php }else{ ?>
            <div class="alert alert-success text-center" role="alert">
                Status : Closed
            </div>  
          <?php } ?>
        </div> 
      </div>
      
      <div class="card-deck mb-3">      
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal text-center">Customer Details</h4>
          </div>
          <div class="card-body text-left"> 
            <ul class="list-group mt-3 mb-4">
                <li class="list-group-item">First name : <span><?php echo $get_inquiry_details[0]->first_name; ?></span> </li>
                <li class="list-group-item">Last name : <span><?php echo $get_inquiry_details[0]->last_name; ?></span> </li>
                <li class="list-group-item">Email : <span><?php echo $get_inquiry_details[0]->email; ?></span> </li> 
                <li class="list-group-item">Phone Number : <span><?php echo $get_inquiry_details[0]->phone; ?></span> </li> 
                <li class="list-group-item">Status : <span style="color:green;">Active</span></li>
            </ul> 
          </div>
        </div> 
 
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal text-center">Inquiry Details</h4>
          </div>
          <div class="card-body text-left"> 
            <ul class="list-group mt-3 mb-4">
                <li class="list-group-item">Business Type : <span>Super Visa Insurance</span> </li>
                <li class="list-group-item">Insured Count : <span>1</span></li> 
                <li class="list-group-item">Effective Date : <span><?php echo $get_inquiry_details[0]->effective_date; ?></span> </li> 
                <li class="list-group-item">Expiry Date : <span><?php echo $get_inquiry_details[0]->expiry_date; ?></span></li>
                <li class="list-group-item">Departure City : <span><?php echo $get_inquiry_details[0]->departure_city; ?></span></li>
                <li class="list-group-item">Province : <span><?php echo $get_inquiry_details[0]->province; ?></span></li>
                <li class="list-group-item">Benefits : <span><?php echo $get_inquiry_details[0]->benefits; ?></span></li>
                <li class="list-group-item">Deductable Price : <span><?php echo $get_inquiry_details[0]->deductable_price; ?></span></li>
                <li class="list-group-item">Pre Existing Conditions : <span><?php echo $get_inquiry_details[0]->pre_existing_medical; ?></span></li>
                <li class="list-group-item">Inquired Date : <span><?php echo $get_inquiry_details[0]->created_at; ?></span></li>
                <li class="list-group-item">Status : <span style="color:green;"><?php echo $get_inquiry_details[0]->query_status; ?></span></li>
            </ul> 
          </div>
        </div> 

      </div> 

    </div>
  
<style>
.page-text-style {font-size: 17px;}    
</style>
<script>
    $(document).ready(function() {  
      $(".dropdown-item").click(function(e) {
        e.preventDefault();
        var status = this.id;
        var inq_id = $('.inq_id').val(); 
        var ajax_url = $('.admin-ajax').val();
 
        $.ajax({
            url: ajax_url, // or example_ajax_obj.ajaxurl if using on frontend
            data: {
                'action': 'update_inquiry_status',
				        'inq_id' : inq_id, 
			        	'status' : status 
            },
            type: 'POST',
            success:function(data) {   
                if(data == 1){   
                    $('.reg-succ').show();
                        setTimeout(function(){
                            $('.reg-succ').hide(); 
                            location.reload();
                        },3000); 
                }else if(data == 0){
                    alert('Server is not responding');
                    //location.reload();  
                }  
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        });   


      });

    }); 
</script>