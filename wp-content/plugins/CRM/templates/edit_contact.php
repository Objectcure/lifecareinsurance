<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/vendor/bootstrap-datepicker3.min.css" />
<Script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/bootstrap-datepicker.min.js"> </script>

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">Customer Details</h5> 
      <a class="btn btn-outline-primary" onclick="window.history.go(-1); return false;" href="#">Back</a>
    </div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h3 class="display-5 font-weight-bold">
      <?php echo $get_customer_details[0]->first_name; ?>  <?php echo $get_customer_details[0]->last_name; ?> </h3> 
    </div> 
    <div class="container">
      <div class="card-deck mb-12">
        <div class="card mb-12 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal text-center">Personal Details</h4>
          </div>
          <div class="card-body text-left"> 
            <ul class="list-group mt-3 mb-4"> 
            <input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" class="admin-ajax" />
                <input type="hidden" value="<?php echo $get_customer_details[0]->id; ?>" class="customer_id" />
                <li class="list-group-item">   
                    Date of Birth :    
                    <div class="form-group date-container1" style="">
                    <span> 
                    <input type="text" class="form-control datepicker1 dob" id="input-id-1" data-date-end-date="0d" placeholder="Select Date of Birth" value="<?php echo $get_customer_details[0]->dob; ?>">
                    <span class="error-message-field-dob">This is a mandatory field</span> 
                    </span> 
                    </div>
                </li>
                <li class="list-group-item">
                    Email id :
                    <div class="form-group" style="">     
                    <span>
                    <input type="text" value="<?php echo $get_customer_details[0]->email; ?>" name="email" class="email" /> </span> <br>
                    <span class="error-message-field-email">This is a mandatory field</span>
                    <span class="error-message-valid-email">Please enter a valid email</span>                   
                    </span>
                    </div> 
                </li>
                <li class="list-group-item">
                    Phone Number : 
                    <div class="form-group" style=""> 
                    <span><input type="text" value="<?php echo $get_customer_details[0]->phone; ?>" name="phone" class="phone" /> </span> <br>
                    <span class="error-message-field-phone">This is a mandatory field</span>
                    </span>
                    </div> 
                 </li>
                <li class="list-group-item"> <button type="submit" class="btn btn-primary save-customer">Save</button> </li>
            </ul> 
            <div class="col-sm-12 col-md-12">
                <div class="alert alert-success reg-succ" style="display:none;" role="alert"> 
                    Data updated successfully.
                </div>

                <div class="alert alert-danger reg-alert" style="display:none;" role="alert"> 
                    Email id already exist. Please try another.
                </div>
            </div>
          </div>
        </div>
    
      </div>       
    </div>
  
<style>
.page-text-style {font-size: 17px;}    
.error-message-field-dob,.error-message-field-email, .error-message-field-phone, .error-message-valid-email {color:red; display: none;}
</style>

<script>
    $(document).ready(function() {  
        $('.datepicker1').datepicker({
            format: 'mm/dd/yyyy',
            container: '.date-container1'
        });

        $('.save-customer').click(function () {   

        var ajax_url = $('.admin-ajax').val();
        // data validation
        var dob = $('.dob').val(); if(dob == ''){$('.error-message-field-dob').show();return false;}else{$('.error-message-field-dob').hide();} 
        var email = $('.email').val(); if(email == ''){$('.error-message-field-email').show();return false;}else{$('.error-message-field-email').hide();}
        var emailvalidate = validateEmail(email);
        if(emailvalidate === false){$('.error-message-valid-email').show();return false;}else{$('.error-message-valid-email').hide();}
        var phone = $('.phone').val(); if(phone == ''){$('.error-message-field-phone').show();return false;}else{$('.error-message-field-phone').hide();} 
          
        var customer_id  = $('.customer_id').val();

        $.ajax({
            url: ajax_url, // or example_ajax_obj.ajaxurl if using on frontend
            data: {
                'action': 'update_contact',
				'dob' : dob, 
				'email' : email,
                'phone' : phone,
                'customer_id' : customer_id
            },
            type: 'POST',
            success:function(data) {  
                if(data == 1){  
                    //$(this).closest('form').find("input[type=text], input[type=email]").val("");
                    $('.reg-succ').show();
                        setTimeout(function(){
                            $('.reg-succ').hide(); 
                            location.reload();
                        },3000);
                }else if(data == 2){
                        $('.reg-alert').show();
                        setTimeout(function(){
                            $('.reg-alert').hide(); 
                            //location.reload();
                        },3000);

                }else if(data == 0){
                    alert('Server is not responding');
                    //location.reload();  
                }  
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        });   
            
        });



    // Email validate function
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

            
    });
</script>