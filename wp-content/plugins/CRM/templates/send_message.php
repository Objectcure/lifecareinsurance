<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div class="wrap">
    <div class="">
        <div class="row">
            <div class="col-sm-8"> 
                <h1 class="wp-heading-inline">Send Message</h1> 
            </div>
            <div class="col-sm-2"> 
                <nav class="my-2 my-md-0 mr-md-3">   
                    <a class="btn btn-success" onclick="window.history.go(-1); return false;" href="#">Back</a>  
                    <input type="hidden" value=" <?php echo $_REQUEST['id']; ?>" class="inq_id" />
                    <input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" class="admin-ajax" />
                </nav> 
            </div> 
        </div>
        <div class="row"> 
            <div class="col-md-12">  
                <div class="row">
                    <div class="col-md-8 mb-3">
                        <label for="firstName">Subject</label>
                        <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
                        <div class="invalid-feedback">
                        
                        </div>
                    </div> 
                </div> 
                <div class="row">
                    <div class="col-md-8 mb-3">
                        <label for="firstName">Message</label>
                        <div class="form-group"> 
                        <textarea class="form-control note" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <div class="invalid-feedback">
                         
                        </div>
                    </div> 
                </div>

                <div class="row">
                    <div class="col-md-8 mb-3">
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile02">
                                <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
                            </div> 
                        </div> 
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8 mb-3 text-right">
                        <a onclick="return false;" class="btn btn-primary send-message">Send</a>
                    </div> 
                </div>
            </div>
        </div> 
    </div> 
</div>
<style>
.reg-succ {display : none;}    
</style>

<script>
    $(document).ready(function () { 

        jQuery('#dtable').DataTable({
            columnDefs: [
                {orderable: false, targets: -1}
            ]
        });

        $('.save-note').click(function () {  
            var inq_id = $('.inq_id').val();
            var ajax_url = $('.admin-ajax').val();
            var note = $('.note').val();

            $.ajax({
                url: ajax_url,
                data: {
                    'action': 'add_note',
                    'inq_id' : inq_id, 
                    'note' : note 
                },
                type: 'POST',
                success:function(data) {   
                    if(data == 1){   
                        $('.note').val('');
                        $('.reg-succ').show();
                            setTimeout(function(){
                                $('.reg-succ').hide(); 
                                location.reload();
                            },3000); 
                    }else if(data == 0){
                        alert('Server is not responding');
                        //location.reload();  
                    }  
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            });   
            
        });

    });
</script>