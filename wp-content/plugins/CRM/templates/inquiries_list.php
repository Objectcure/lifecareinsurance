<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
<div class="wrap">
    
    <?php if(!empty($_REQUEST['success']) && $_REQUEST['success'] == 1){?> 
    <div class="alert alert-success">
        <strong>Success!</strong> Order has been approved.
    </div>
    <?php }?>
     <?php if(!empty($_REQUEST['success']) && $_REQUEST['success'] == 2){?> 
    <div class="alert alert-success">
        <strong>Success!</strong> Order has been disapproved.
    </div>
    <?php }?>
    <h1 class="wp-heading-inline">Customer Inquiries</h1> 
    <table id ="dtable" class="wp-list-table widefat fixed striped posts">
        <thead>
            <tr>
                <th width="10%">No.</th>
                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Customer name</span></th>
                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Business Type</span></th>
                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Insured Count</span></th> 
                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Effective Date</span></th>
                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Expiry Date</span></th> 
                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Departure City</span></th> 
                <th width="30%" align="left" scope="col" id="author" class="manage-column">Actions</th>
            </tr>
        </thead>
        <?php
            $sl = 0;
            foreach ($get_inquiries as $row) {
                $sl++;
                ?>
                <tr id="post-20" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized">
                    <td><?= $sl; ?></td>
                    <td><?= $row->first_name; ?></td>
                    <td>Super Visa Insurance</td>
                    <td>1</td>
                    <td><?= $row->effective_date; ?></td>
                    <td><?= $row->expiry_date; ?></td>
                    <td><?= $row->departure_city; ?></td>
                    <td><a href="admin.php?page=inquiry_details&id=<?= $row->id; ?>" class="btn btn-success">View</a></td> 
                </tr> 
                <?php
            }
        ?>
    </table>
</div>
<script>

    $(document).ready(function () {
        jQuery('#dtable').DataTable({
            columnDefs: [
                {orderable: false, targets: -1}
            ]
        });
    });


</script>