<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/popper.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/bootstrap.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.dataTables.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/dataTables.bootstrap4.min.js', dirname(__FILE__) ) ) ?>"></script>
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/bootstrap.min.css', dirname(__FILE__) ) ) ?>">
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/dataTables.bootstrap4.min.css', dirname(__FILE__) ) ) ?>">

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">Applicants Details</h5>   
        <a class="btn btn-outline-primary" onclick="window.history.go(-1); return false;" href="#">Back</a>
    </div> 

    <div class="container"> 
      <div class="card-deck mb-3">      
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal text-center">Applicant 1</h4>
          </div>
          <div class="card-body text-left"> 
            <ul class="list-group mt-3 mb-4">
                <?php if(!empty($get_quote_details['applicant_1'])){ foreach($get_quote_details['applicant_1'] as $quotes){
                    $question = str_replace('_', ' ', $quotes->question);
                    $question_ltrim1 = ltrim($question, '1'); 
                    ?>
                    <li class="list-group-item">
                        <span><?php echo ucwords($question_ltrim1); ?> : <span style="color:purple;"><?php echo ucfirst($quotes->answer); ?></span></span> 
                    </li>
                <?php } } ?> 
            </ul> 
          </div>
        </div> 
         <?php if(!empty($get_quote_details['applicant_2'])){  ?>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal text-center">Applicant 2</h4>
          </div>
          <div class="card-body text-left"> 
            <ul class="list-group mt-3 mb-4">
            <?php if(!empty($get_quote_details['applicant_2'])){ foreach($get_quote_details['applicant_2'] as $quotes){
                    $question = str_replace('_', ' ', $quotes->question);
                    $question_ltrim2 = ltrim($question, '2');
                    ?>
                    <li class="list-group-item">
                        <span><?php echo ucwords($question_ltrim2); ?> : <span style="color:purple;"><?php echo ucfirst($quotes->answer); ?></span></span> 
                    </li>
                <?php } } ?> 
            </ul> 
          </div>
        </div> 
        <?php }  ?>   
      </div> 

    </div>
  
<style>
.page-text-style {font-size: 17px;}    
</style>
<script>
    $(document).ready(function() {  
      $(".dropdown-item").click(function(e) {
        e.preventDefault();
        var status = this.id; 
        var inq_id = $('.inq_id').val(); 
        var ajax_url = $('.admin-ajax').val();
 
        $.ajax({
            url: ajax_url, // or example_ajax_obj.ajaxurl if using on frontend
            data: {
                'action': 'update_inquiry_status',
				        'inq_id' : inq_id, 
			        	'status' : status 
            },
            type: 'POST',
            success:function(data) {   
                if(data == 1){   
                    $('.reg-succ').show();
                        setTimeout(function(){
                            $('.reg-succ').hide(); 
                            location.reload();
                        },3000); 
                }else if(data == 0){
                    alert('Server is not responding');
                    //location.reload();  
                }  
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        });   


      });

    }); 
</script>