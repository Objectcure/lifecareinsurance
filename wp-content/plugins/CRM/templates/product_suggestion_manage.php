<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

<div class="wrap">
    <h1 class="wp-heading-inline">Manage Product Suggestion</h1>
    <hr class="wp-header-end">
    <ul class="subsubsub">
        <li class="all"><a href="admin.php?page=hw_product_suggestion_manage">All</a> |</li>
        <li class="assigned"><a href="admin.php?page=hw_product_suggestion_manage&status=assigned" >Assigned Posts</a> |</li>
        <li class="unassigned"><a href="admin.php?page=hw_product_suggestion_manage&status=unassigned">Unassigned Posts</a></li>
    </ul>
    <div class="clear"></div>
    <div style="padding-top: 25px;">
        <form name="post" method="post" id="post">
            <div id="tableDomainsList_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                <div class="listtable">
                    <table id ="dtable" class="wp-list-table widefat fixed striped posts">
                        <thead>
                            <tr>
                                <th width="5%"></th>
                                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Post title</span></th>
                                <th width="30%" align="left" scope="col" id="author" class="manage-column">Post name</th>
                                <th width="35%" align="left" scope="col" id="author" class="manage-column">Assigned Product Suggestion</th>
                            </tr>
                        </thead>
                        <?php
                        foreach ($posts as $row) {
                            $sl++;
                            ?>
                            <tr id="post-20" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized">
                                <td><input type="checkbox" name="post[]" value="<?= $row->ID ?>"></td>
                                <td><a href="post.php?post=<?= $row->ID ?>&action=edit"><?= $row->post_title; ?></</td>
                                <td><?= $row->post_name; ?></td>
                                <td><a href="admin.php?page=hw_product_suggestion_edit&id=1" target="blank"><?= $row->product_suggestions ?></a></td>
                            </tr>

                            <?php
                        }
                        ?>

                    </table>
                    <span style="padding-top: 10px">
                        <input name="submit" type="submit" id="doaction2" class="button button-primary button-hero load-customize hide-if-no-customize" value="Bulk update">
                    </span>
                </div>
            </div>
        </form>
    </div>
</div>
<script>

    $(document).ready(function () {
        $('#dtable').DataTable();
    });

</script>