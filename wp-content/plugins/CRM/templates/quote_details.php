<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/popper.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/bootstrap.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.dataTables.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/dataTables.bootstrap4.min.js', dirname(__FILE__) ) ) ?>"></script>
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/bootstrap.min.css', dirname(__FILE__) ) ) ?>">
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/dataTables.bootstrap4.min.css', dirname(__FILE__) ) ) ?>">

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">Quote Details</h5>
      <nav class="my-2 my-md-0 mr-md-3"> 
        <a href="admin.php?page=applicants_details&id=<?php echo $get_inquiry_details[0]->quote_id; ?>" class="btn btn-success">Applicants Details</a> 
      </nav>
      <nav class="my-2 my-md-0 mr-md-3"> 
        <a href="admin.php?page=create_message&id=<?php echo $get_inquiry_details[0]->id; ?>" class="btn btn-danger">Send Message</a> 
      </nav>
      <nav class="my-2 my-md-0 mr-md-3">
        <a href="admin.php?page=notes_list&id=<?php echo $get_inquiry_details[0]->id; ?>" class="btn btn-secondary">Notes</a> 
      </nav>
      <nav class="my-2 my-md-0 mr-md-3"> 
        <div class="btn-group">
            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Action
            </button>
            <div class="dropdown-menu"> 
            <a class="dropdown-item" href="#" id="initiated" style="color:orange;">Initiated 
            <?php if($get_inquiry_details[0]->status == 'initiated'){ ?><span class="dashicons dashicons-yes"> <?php } ?></span></a>
            <a class="dropdown-item" href="#" id="communicated" style="color:purple;">Communicated <?php if($get_inquiry_details[0]->status == 'communicated'){ ?><span class="dashicons dashicons-yes"> <?php } ?></span></a>
            <a class="dropdown-item" href="#" id="closed" style="color:green;">Closed <?php if($get_inquiry_details[0]->status == 'closed'){ ?><span class="dashicons dashicons-yes"> <?php } ?></span></a>
        </div>  
        <input type="hidden" value="<?php echo $get_inquiry_details[0]->id; ?>" class="inq_id" />
        <input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" class="admin-ajax" />
      </nav>  
        <a class="btn btn-outline-primary" onclick="window.history.go(-1); return false;" href="#">Back</a>
    </div> 

    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="alert alert-success reg-succ" style="display:none;" role="alert"> 
              Status updated successfully.
          </div>
        </div>
        <div class="col-sm-4">
          <?php if($get_inquiry_details[0]->status == 'initiated'){ ?>
            <div class="alert alert-warning text-center" role="alert">
                Status : Initiated
            </div>
          <?php }elseif($get_inquiry_details[0]->status == 'communicated'){ ?>
            <div class="alert alert-info text-center" role="alert">
                Status : Communicated
            </div> 
          <?php }else{ ?>
            <div class="alert alert-success text-center" role="alert">
                Status : Closed
            </div>  
          <?php } ?>
        </div> 
      </div>
      
      <div class="card-deck mb-3">      
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal text-center">Customer Details</h4>
          </div>
          <div class="card-body text-left"> 
            <ul class="list-group mt-3 mb-4">
                <li class="list-group-item">First name : <span><?php echo $get_inquiry_details[0]->first_name; ?></span> </li>
                <li class="list-group-item">Last name : <span><?php echo $get_inquiry_details[0]->last_name; ?></span> </li>
                <li class="list-group-item">Email : <span><?php echo $get_inquiry_details[0]->email; ?></span> </li> 
                <li class="list-group-item">Phone Number : <span><?php echo $get_inquiry_details[0]->phone; ?></span> </li> 
                <li class="list-group-item">Status : <span style="color:green;">Active</span></li>
            </ul> 
          </div>
        </div> 
 
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal text-center">Quote Details</h4>
          </div>
          <div class="card-body text-left"> 
            <ul class="list-group mt-3 mb-4">
                <li class="list-group-item">Business Type : <span>Travel Visa Insurance</span> </li>
                <li class="list-group-item">Insured Customers Number : <span> <?php if(!empty($get_inquiry_details[0]->dob_2)){ ?>2<?php }else{ ?>1<?php } ?></span></li> 
                <li class="list-group-item">Departure Date : <span><?php 
                $dateDept = new DateTime($get_inquiry_details[0]->departure_date);
                echo $dateDept->format('Y-m-d'); ?></span> </li> 
                <li class="list-group-item">Return Date : <span><?php 
                $dateReturn = new DateTime($get_inquiry_details[0]->return_date);
                echo $dateReturn->format('Y-m-d'); ?></span></li>
                <?php if(!empty($get_inquiry_details[0]->destination)){ ?>
                <li class="list-group-item">Destination : <span><?php echo $get_inquiry_details[0]->destination; ?></span></li>
                <?php } ?> 
                <li class="list-group-item">Inquired Date : <span><?php 
                  $createDate = new DateTime($get_inquiry_details[0]->created_at);
                  echo $createDate->format('Y-m-d'); ?></span></li>
                <li class="list-group-item">Status : <span style="color:green;"><?php echo $get_inquiry_details[0]->status; ?></span></li>
            </ul> 
          </div>
        </div> 

      </div> 

    </div>
  
<style>
.page-text-style {font-size: 17px;}    
</style>
<script>
    $(document).ready(function() {  
      $(".dropdown-item").click(function(e) {
        e.preventDefault();
        var status = this.id; 
        var inq_id = $('.inq_id').val(); 
        var ajax_url = $('.admin-ajax').val();
 
        $.ajax({
            url: ajax_url, // or example_ajax_obj.ajaxurl if using on frontend
            data: {
                'action': 'update_inquiry_status',
				        'inq_id' : inq_id, 
			        	'status' : status 
            },
            type: 'POST',
            success:function(data) {   
                if(data == 1){   
                    $('.reg-succ').show();
                        setTimeout(function(){
                            $('.reg-succ').hide(); 
                            location.reload();
                        },3000); 
                }else if(data == 0){
                    alert('Server is not responding');
                    //location.reload();  
                }  
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        });   


      });

    }); 
</script>