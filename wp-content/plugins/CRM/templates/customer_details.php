<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">Customer Details</h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="btn btn-warning" href="admin.php?page=edit_contact&id=<?= $get_customer_details[0]->id; ?>">Edit</a>  
      </nav>
      <a class="btn btn-outline-primary" onclick="window.history.go(-1); return false;" href="#">Back</a>
    </div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h3 class="display-5 font-weight-bold"><?php echo $get_customer_details[0]->first_name; ?>  <?php echo $get_customer_details[0]->last_name; ?> </h3> 
    </div> 
    <div class="container">
      <div class="card-deck mb-12">
        <div class="card mb-12 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal text-center">Personal Details</h4>
          </div>
          <div class="card-body text-left"> 
            <ul class="list-group mt-3 mb-4">
                <li class="list-group-item">Date of Birth : <span><?php echo $get_customer_details[0]->dob; ?></span> </li>
                <li class="list-group-item">Email id : <span><?php echo $get_customer_details[0]->email; ?></span> </li>
                <li class="list-group-item">Phone Number : <span><?php echo $get_customer_details[0]->phone; ?></span> </li>
                <li class="list-group-item">Account Created on : <span><?php echo $get_customer_details[0]->created_at; ?></span> </li>
                <li class="list-group-item">Status : <span style="color:green;">Active</span></li>
            </ul> 
          </div>
        </div>
    
      </div>       
    </div>
  
<style>
.page-text-style {font-size: 17px;}    
</style>