<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
<div class="wrap">
    <h1 class="wp-heading-inline">Order Details</h1> 
     <a href="#" onclick="window.history.go(-1); return false;" class="page-title-action">Back</a>
    <table id ="dtable" class="wp-list-table widefat fixed striped posts">
        <thead style="background: #00a0d2;">
            <tr>
                <th width="10%" style="font-size: 17px;">No.</th>
                <th width="60%" align="left" scope="col" id="name" class="manage-column column-title column-primary page-text-style" style="font-size: 17px;"><span>Product</span></th>
                <th width="60%" align="left" scope="col" id="name" class="manage-column column-title column-primary page-text-style" style="font-size: 17px;"><span>Price</span></th> 
            </tr>
        </thead>
        <?php
        $sl = 0; 
        $grand_total = 0;  
        //echo '<pre>';print_r($get_order_details);
        foreach ($get_order_details as $row) {
            $get_price = get_field('price',$row->prod_id);
            //echo 
            $sl++;
            $grand_total += $get_price;
            ?>
            <tr id="post-20" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized">
                <td style="font-size: 17px;"><?= $sl; ?></td>
                <td style="font-size: 17px;"><?= $row->post_title; ?></td>
                <td style="font-size: 17px;"><?= $get_price; ?> AED</td>  
            </tr> 
            <?php
            
            }
            ?>
            <tr id="post-20" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized">
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;</td> 
                <td style="font-size: 17px;"><strong>Grand Total : <?php echo $grand_total; ?> AED</strong></td>
            </tr>
            <tr id="post-20" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized">
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;</td>
            </tr>
            <tr id="post-20" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized"> 
                <td>&nbsp;&nbsp;&nbsp;</td> 
                <td style="font-size: 17px;">Customer Details</td>
                <td style="font-size: 17px;">Name of the customer: <span style="background-color:#999;"></span><?php echo $get_user_details[0]->user_login; ?><br><br>
                    Address : <?php echo $get_user_details[0]->address; ?> 
                    <br><br>  
                    Email ID : <?php echo $get_user_details[0]->user_email; ?> 
                    <br><br>  
                    Phone : <?php echo $get_user_details[0]->user_phone; ?> 
                    </td> 
            </tr>  
            
           <!-- <tr id="post-20" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized">
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td style="font-size: 17px;">Payment Details</td>
                <td style="font-size: 17px;">Payment Status : <span style="background-color:#999;"></span>Pending <br><br>
                    Total : 200 <br><br>
                    Discounts: 0.00 <br><br>
                    Grand Total : <?php //echo $grand_total; ?> </td>  
            </tr>  
            <tr id="post-20" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized">
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td><a href="admin.php?page=order_approve&id=<?= $order_id; ?>" class="btn btn-success">Approve</a> &nbsp;&nbsp; <a href="admin.php?page=order_disapprove&id=<?= $order_id; ?>" class="btn btn-danger">Disapprove</a> </td> 
            </tr>  -->
    </table> 
</div> 
<style>
.page-text-style {font-size: 17px;}    
</style>