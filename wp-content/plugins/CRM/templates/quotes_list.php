<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.dataTables.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/dataTables.bootstrap4.min.js', dirname(__FILE__) ) ) ?>"></script>
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/bootstrap.min.css', dirname(__FILE__) ) ) ?>">
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/dataTables.bootstrap4.min.css', dirname(__FILE__) ) ) ?>">
<div class="wrap"> 
    <h1 class="wp-heading-inline">Customer Quotes</h1> 
    <div class="container" style="margin-top:30px;">
        <table id ="dtable" class="wp-list-table widefat fixed striped posts">
        <thead>
            <tr>
                <th width="10%">No.</th>
                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Quote ID</span></th>
                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Customer Name</span></th>
                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Status</span></th> 
                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Created Date</span></th> 
                <th width="30%" align="left" scope="col" id="author" class="manage-column">Actions</th>
            </tr>
        </thead>
        <?php
            global $wpdb;
            $sl = 0;
            foreach ($get_customer_quotes as $row) {
                $date = new DateTime($row->created_at);
                $created_at = $date->format('Y-m-d'); 
                $getuser =  $wpdb->get_row("SELECT * FROM crm_customers WHERE id=$row->customer_id");
	            $customer_name = $getuser->first_name.' '.$getuser->last_name;
                $sl++;
                ?>
                <tr id="post-20" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized">
                    <td><?= $sl; ?></td>
                    <td><?= $row->quote_id; ?></td> 
                    <td><?= $customer_name; ?></td>
                    <td><?= ucfirst($row->status); ?></td>
                    <td><?= $created_at;  ?></td>
                    <td><a href="admin.php?page=quote_details&id=<?= $row->id; ?>" class="btn btn-success delete-rate-card">View</a></td> 
                </tr> 
                <?php
            }
        ?>
        </table>
    </div>
</div>
<script>
    $(document).ready(function () {
        jQuery('#dtable').DataTable({
            columnDefs: [
                {orderable: false, targets: -1}
            ]
        });
    });
</script>