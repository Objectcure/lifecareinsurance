<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/popper.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/bootstrap.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/jquery.dataTables.min.js', dirname(__FILE__) ) ) ?>"></script>
<script type="text/javascript" src="<?php echo esc_url( plugins_url( 'resources/js/dataTables.bootstrap4.min.js', dirname(__FILE__) ) ) ?>"></script>
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/bootstrap.min.css', dirname(__FILE__) ) ) ?>">
<link rel="stylesheet" href="<?php echo esc_url( plugins_url( 'resources/css/dataTables.bootstrap4.min.css', dirname(__FILE__) ) ) ?>">

<div class="wrap">

<div class="container">
    <div class="row">
        <div class="col-sm-8"> 
        </div>
        <div class="col-sm-4"> 
            <nav class="my-2 my-md-0 mr-md-3">  
                <a href="#" class="btn btn-info" data-toggle="modal" data-target="#exampleModal">Add Note</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-success" onclick="window.history.go(-1); return false;" href="#">Back</a>  
                <input type="hidden" value=" <?php echo $_REQUEST['id']; ?>" class="inq_id" />
                <input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" class="admin-ajax" />
            </nav> 
        </div>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add new note</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <div class="form-group"> 
                            <textarea class="form-control note" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div> 
                        <span class="error-note">This is a mandatory field</span>
                        <div class="alert alert-success reg-succ text-center" role="alert">
                            Note has been saved.
                        </div> 
                    </div>
                <div class="modal-footer"> 
                    <a onclick="return false;" class="btn btn-primary save-note">Save</a>
                </div>
            </div>
        </div>
        </div> 
    </div>
</div>

    
    <?php if(!empty($_REQUEST['success']) && $_REQUEST['success'] == 1){?> 
    <div class="alert alert-success">
        <strong>Success!</strong> Order has been approved.
    </div>
    <?php }?>
     <?php if(!empty($_REQUEST['success']) && $_REQUEST['success'] == 2){?> 
    <div class="alert alert-success">
        <strong>Success!</strong> Order has been disapproved.
    </div>
    <?php }?>
    <h1 class="wp-heading-inline">Notes</h1> 
    <table id ="dtable" class="wp-list-table widefat fixed striped posts">
        <thead>
            <tr>
                <th width="10%">No.</th> 
                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Note</span></th> 
                <th width="30%" align="left" scope="col" id="name" class="manage-column column-title column-primary"><span>Created Date</span></th> 
            </tr>
        </thead>
        <?php
        $sl = 0;
        foreach ($get_notes as $row) {
            $sl++;
            ?>
            <tr id="post-20" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized">
                <td><?= $sl; ?></td>
                <td><?= $row->note; ?></td> 
                <td><?= $row->created_at; ?></td>  
            </tr> 
            <?php
        }
        ?>
    </table>
</div>
<style>
.reg-succ {display : none;}   
.error-note {display : none; color:red;} 
</style>

<script>
    $(document).ready(function () { 

        jQuery('#dtable').DataTable({
            columnDefs: [
                {orderable: false, targets: -1}
            ]
        });

        $('.save-note').click(function () {  
            var inq_id = $('.inq_id').val();
            var ajax_url = $('.admin-ajax').val();
            var note = $('.note').val();

            if(note == ''){$('.error-note').show();return false;}else{$('.error-note').hide();} 

            $.ajax({
                url: ajax_url,
                data: {
                    'action': 'add_note',
                    'inq_id' : inq_id, 
                    'note' : note 
                },
                type: 'POST',
                success:function(data) {   
                    if(data == 1){   
                        $('.note').val('');
                        $('.reg-succ').show();
                            setTimeout(function(){
                                $('.reg-succ').hide(); 
                                location.reload();
                            },3000); 
                    }else if(data == 0){
                        alert('Server is not responding');
                        //location.reload();  
                    }  
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            });   
            
        });

    });
</script>