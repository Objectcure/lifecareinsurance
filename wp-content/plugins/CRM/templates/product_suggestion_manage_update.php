<script type="text/javascript" src="../wp-content/plugins/hw-product-suggestion/resources/chosen_v1.8.5/chosen.jquery.min.js"></script>
<link rel="stylesheet" href="../wp-content/plugins/hw-product-suggestion/resources/chosen_v1.8.5/chosen.min.css">
<div class="wrap">
    <h1 class="wp-heading-inline">Manage Product suggestion</h1>
    <div id="wp-content-editor-tools" class="wp-editor-tools hide-if-no-js" style="position: absolute; top: 0px; width: 847px;">

    </div>
    <form name="post" method="post" id="post">
        <table class="wp-list-table widefat fixed striped posts">
            <thead>
                <tr>
                    <th width="40%" align="left" scope="col" id="title" class="manage-column column-title column-primary"><span>Post title</span></th>
                    <th width="40%" align="left" scope="col" id="postname" class="manage-column">Post name</th>
                    <th width="20%" align="left" scope="col" id="options" class="manage-column"></th>
                </tr>
            </thead>
            <?php
            foreach ($posts as $row) {
                $sl++;
                ?>
                <tr id="td-row-<?= $row->ID ?>" class="iedit author-self level-0 post-20 type-post status-publish format-standard hentry category-uncategorized">
                    <td><input type="checkbox" name="post_ids[]" value="<?= $row->ID ?>" checked="checked"><a href="post.php?post=<?= $row->ID ?>&action=edit" target="blank"><?= $row->post_title; ?></a></td>
                    <td><?= $row->post_name; ?></td>
                    <td><a href="#"><span class="dashicons dashicons-trash"></span></a></td>
                </tr>

                <?php
            }
            ?>

        </table>
        <div style="padding-top: 17px; ">
            <select name="suggestions[]" multiple class="chosen-select" data-placeholder="Select Suggestion" >
                <?php
                foreach ($product_suggestions as $sug) {
                    echo '<option value="' . $sug->id . '">' . $sug->name . '</option>';
                }
                ?>
            </select>
            <span style="padding-left: 10px">
                <input name="bulk_update" type="submit" id="doaction2" class="button button-primary" value="Update">
                <a href="admin.php?page=hw_product_suggestion_manage" class="button button-error">Cancel</a>
            </span>
        </div>
    </form>
</div>


<script>
    jQuery(function () {
        jQuery(".chosen-select").chosen({max_selected_options: 1});
        jQuery(".chosen-select").chosen();
    });

    jQuery(".dashicons-trash").click(function () {
        jQuery(this).closest("tr").remove();
    });
</script>

<style>
    .chosen-container{width: 50% !important;}
</style>
