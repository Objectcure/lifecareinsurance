<script type="text/javascript" src="../wp-content/plugins/hw-product-suggestion/resources/chosen_v1.8.5/chosen.jquery.min.js"></script>
<link rel="stylesheet" href="../wp-content/plugins/hw-product-suggestion/resources/chosen_v1.8.5/chosen.min.css">

<style>
    .chosen-container{width: 100% !important;}
</style>

<div>
    <p class="label">
        <label for="meta-box-dropdown">Product Suggestion</label>
    </p>

    <select name="product_suggestion_select[]" multiple class="chosen-select" id="product_suggestion_select" style="width:40%" data-placeholder="Start Typing to Search Product Suggestion" >
        <?= $option_values ?>
    </select>
</div>
<script>
    $(document).ready(function () {
        jQuery(".chosen-select").chosen({max_selected_options: 1});
        jQuery(".chosen-select").chosen();
        jQuery(".dashicons-trash").click(function () {
            jQuery(this).closest("tr").remove();
        });
        $("#product_suggestion_select_chosen .chosen-search-input").keyup(function () {
            var input = this.value;
            jQuery.ajax({
                type: "POST",
                url: ajaxurl,
                data: {action: 'get_product_suggestion_ajax', input_data: input}
            }).done(function (msg) {

                $("#product_suggestion_select").find('option').remove();
                $.each(msg.suggestion_options, function (index, item)
                {
                    $("#product_suggestion_select").append("<option value='" + item.id + "'>" + item.name + "</option>").trigger("chosen:updated");
                });
                $("#product_suggestion_select").trigger("chosen:updated");

                $("#product_suggestion_select_chosen .chosen-search-input").val(input);
            });
            return true;
        });
    });
</script>

