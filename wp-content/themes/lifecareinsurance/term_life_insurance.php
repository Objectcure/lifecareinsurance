<?php
 
 /* Template Name: Term Life Insurance */ 
 
 get_header();
 ?>
<!-- BANNER -->
<div class="common-banner-section banner-page services">
	<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page"><?php echo get_the_title(); ?></div> 
				</div>
			</div>
		</div>
	</div>
	<!-- Main Header Content Part --> 
	<div class="section pages section-border">
		<ul class="breadcrumb" style="margin: 0px 0px 20px 0px !important;padding: 8px 60px;">
			<li><a href="<?php echo get_home_url(); ?>">Home</a></li>
			<li class="active"><?php echo get_the_title(); ?></li>
		</ul>
		<div class="container"> 
			<div class="row">
				<div class="col-sm-8 col-md-8">
					<h2 class="section-heading">
						About Travel Insurance
					</h2>
					<div class="section-subheading">Super Visa issuing came with a set of mandatory requirements such as medical insurance for a minimum of 1 year for each visa applicant.The insurance must include:</div> 
					<ul class="checklist">
						<li>Coverage for emergency medical attention, hospitalization and repatriation.</li>
						<li>Minimum insurance coverage of $ 100,000.</li>
						<li>Availability for review by a port of entry officer with each entry.</li>
						<li>Validity throughout the stay of the visa holder.</li>
					</ul>
					<div>Seeing the demand for Super Visa Insurance many insurance companies in Canada have come forward to provide Super Visa Insurance services.		</div>
				</div>  

				<div class="col-sm-4 col-md-4">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/travel-insurance.jpg" class="Travel Insurance" />

				</div>
			</div> 
			</div>
		</div>
	</div> 
</div>

	<!-- Page -->
	<div class="section services section-border visa-ins-secondary-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-md-4 col-md-push-8">
				<div class="widget categories">
					<ul class="category-nav">
						<li class="active"><a href="javascript:void(0)" data-href="#overview">Overview</a></li>
						<li><a href="javascript:void(0)" data-href="#eligibility">Eligibility</a></li>
						<li><a href="javascript:void(0)" data-href="#features">Features</a></li>
						<li><a href="javascript:void(0)" data-href="#benefits">Benefits</a></li>
						<li><a href="javascript:void(0)" data-href="#policy_exclusions">Policy Exclusions</a></li> 
					</ul>
				</div>  
			</div>
			<div class="col-sm-8 col-md-8 col-md-pull-4">
				<div id="overview" class="category-content show">
					<div class="single-page"> 
						<?php  
							$overview = get_field("overview");
							echo htmlspecialchars_decode($overview); 
						?> 
					</div>
				</div>
				<div id="eligibility" class="category-content">
					<div class="single-page"> 
						<?php  
							$eligibility = get_field("eligibility");
							echo htmlspecialchars_decode($eligibility); 
						?>
					</div>
				</div>
				<div id="features" class="category-content">
					<div class="single-page"> 
						<?php  
							$features = get_field("features");
							echo htmlspecialchars_decode($features); 
						?>
					</div>
				</div>
				<div id="benefits" class="category-content">
					<div class="single-page"> 
						<?php  
							$benefits = get_field("benefits");
							echo htmlspecialchars_decode($benefits); 
						?>
					</div>
				</div>
				<div id="policy_exclusions" class="category-content">
					<div class="single-page"> 
						<?php  
							$policy_exclusions = get_field("policy_exclusions");
							echo htmlspecialchars_decode($policy_exclusions); 
						?>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>  
	
<div class="section news section-border">
	<div class="container"> 
			<h3 style="color: #2ba7de;">
				Read more about Super Visa Insurance
			</h3>
		<div class="row grid-services"> 
			<div class="col-sm-6 col-md-4">
				<div class="box-news-1">
					<div class="image">
						<a href="news-detail.html" title="House Insurance">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive">
						</a>
					</div>
					<div class="meta-date">March 20, 2016</div>
					<h3 class="blok-title"><a href="#" title="How to protect your kid for their future dream.">How to protect your kid for their future dream.</a></h3>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="box-news-1">
					<div class="image">
						<a href="news-detail.html" title="House Insurance">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive">
						</a>
					</div>
					<div class="meta-date">March 20, 2016</div>
					<h3 class="blok-title"><a href="#" title="How to protect your kid for their future dream.">Traveling more confident with our insurance.</a></h3>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="box-news-1">
					<div class="image">
						<a href="news-detail.html" title="House Insurance">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive">
						</a>
					</div>
					<div class="meta-date">March 20, 2016</div>
					<h3 class="blok-title"><a href="#" title="How to protect your kid for their future dream.">Your vacation more confident and safety with us.</a></h3>
				</div>
			</div>  
		</div>  
	</div>
</div> 
<?php
get_footer(); 