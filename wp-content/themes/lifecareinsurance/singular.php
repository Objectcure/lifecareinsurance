<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<main id="site-content" role="main">

<?php  

$fields = get_fields(get_the_ID());


if( $fields ): ?>  
	<?php foreach( $fields as $name => $value ): ?> 
		<?php $objects = get_field_object($name); ?>
		<?php echo $objects['label']; ?>
		<?php echo $objects['type']; ?>
		<?php print_r($objects['choices']); echo '<br>'?>				
	<?php endforeach; ?> 
<?php endif; ?>

</main><!-- #site-content -->

<?php //get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
