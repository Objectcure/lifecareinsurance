<?php
 
 /* Template Name: Visa Insurance */ 
 
 get_header();
 ?>
<!-- BANNER -->
<div class="common-banner-section banner-page services">
	<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page"><?php echo get_the_title(); ?></div> 
				</div>
			</div>
		</div>
	</div>
	<!-- Main Header Content Part --> 
	<div class="section pages section-border">
		<ul class="breadcrumb" style="margin: 0px 0px 20px 0px !important;padding: 8px 60px;">
			<li><a href="<?php echo get_home_url(); ?>">Home</a></li>
			<li class="active"><?php echo get_the_title(); ?></li>
		</ul>
		<div class="container"> 
			<div class="row">
				<div class="col-sm-8 col-md-8">
					<h2 class="section-heading">
						About Super Visa Insurance
					</h2>
					<div class="section-subheading">Super Visa issuing came with a set of mandatory requirements such as medical insurance for a minimum of 1 year for each visa applicant.The insurance must include:</div> 
					<ul class="checklist">
						<li>Coverage for emergency medical attention, hospitalization and repatriation.</li>
						<li>Minimum insurance coverage of $ 100,000.</li>
						<li>Availability for review by a port of entry officer with each entry.</li>
						<li>Validity throughout the stay of the visa holder.</li>
					</ul>
					<div>Seeing the demand for Super Visa Insurance many insurance companies in Canada have come forward to provide Super Visa Insurance services.		</div>
				</div>  

				<div class="col-sm-4 col-md-4">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/600X600.jpg" class="image-responsive" />

				</div>
			</div> 
			</div>
		</div>
	</div>

	<div class="section pages section-border section-get-quote">
		<div class="container qoute-container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading get-a-quote">
						GET A FREE QUOTE
					</h2>  	
					 
					<div class="qoute-sub-heading"><h4>General Details</h4></div>
						<input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" class="admin-ajax" />	
						<div class="row">
							<div class="col-md-3" style="">
								<div class="form-group date-container1" style="">
									<label for="input-id-1">Date of Birth</label>
									<input type="text" class="form-control datepicker1 dob" id="input-id-1" data-date-end-date="0d" placeholder="Select Date of Birth">
									<span class="error-message-field-dob">This is a mandatory field</span>
								</div>
							</div>
							<div class="col-md-3" style="">
								<div class="form-group" style="">
									<label for="input-id-3">Number of Insured</label>
									<input type="text" class="form-control" id="input-id-3" readonly placeholder="1">
								</div>
							</div>
							<div class="col-md-3" style="">
								<div class="form-group date-container2" style="position: static;">
									<label for="input-id-5">Effective Date</label>
									<input type="text" class="form-control datepicker2 effective-date" id="input-id-5" placeholder="Select Effective Date">
									<span class="error-message-field-effective">This is a mandatory field</span>
								</div>
							</div> 
							<div class="col-md-3" style="">
								<div class="form-group date-container3" style="position: static;">
									<label for="input-id-4">Expiry Date</label>
									<input type="text" class="form-control datepicker3 expiry-date" id="input-id-4" placeholder="Select Expiry Date">
									<span class="error-message-field-expiry">This is a mandatory field</span>
								</div> 
							</div>
						</div>
						<div class="qoute-sub-heading"><h4>Trip Details</h4></div>
						<div class="row">
							<!-- <div class="col-md-3" style="">
								<div class="form-group" style="position: static;">
									<label for="input-id-2">Departure City</label>
									<input type="text" class="form-control departure-city" id="input-id-2" placeholder="Departure City">
									<span class="error-message-field-departure">This is a mandatory field</span>
								</div>
							</div> -->
							<div class="col-md-3" style="">
								<div class="form-group" style="position: static;">
									<label for="select-2">Province of Residence</label>
									<select class="form-control province" id="select-2">
										<option value="AB">Alberta</option>
										<option value="BC">British Columbia</option>
										<option value="MB">Manitoba</option>
										<option value="NB">New Brunswick</option>
										<option value="NL">Newfoundland and Labrador</option>
										<option value="NS">Nova Scotia</option>
										<option value="ON">Ontario</option>
										<option value="PE">Prince Edward Island</option>
										<option value="QC">Quebec</option>
										<option value="SK">Saskatchewan</option>
										<option value="NT">Northwest Territories</option>
										<option value="NU">Nunavut</option>
										<option value="YT">Yukon</option>
									</select>
									<span class="error-message-field-province">This is a mandatory field</span>
								</div>
							</div>
							<div class="col-md-3" style="">
								<div class="form-group" style="position: static;">
									<label for="select-1">Benefits</label>
									<select class="form-control benefits" id="select-1">
										<option value="100000">$1,00,000</option>
									</select>
									<span class="error-message-field-benefits">This is a mandatory field</span>
								</div>
							</div>
							<div class="col-md-3" style="">
								<div class="form-group" style="">
									<label for="select-3">Deductible Price</label>
									<select class="form-control deductable" id="select-3">
										<option value="0">$0</option>
										<option value="100">$100</option>
										<option value="250">$250</option>
										<option value="500">$500</option>
										<option value="1000">$1000</option>
										<option value="3000">$3000</option>
										<option value="5000">$5000</option>
										<option value="10000">$10000</option>
									</select>
									<span class="error-message-field-deductable">This is a mandatory field</span>
								</div> 
							</div>
							<div class="col-md-4" style="">
								<div class="form-group" style="">
									<label for="select-3">Pre Existing Conditions:</label>
										<div class="custom-control custom-checkbox custom-control-inline">
											<input name="pre_existing" id="checkbox_0" type="radio" class="custom-control-input pre-existing" value="yes"> 
											<label for="checkbox_0" class="custom-control-label">Yes</label>
										</div>
										<div class="custom-control custom-checkbox custom-control-inline">
											<input name="pre_existing" id="checkbox_1" type="radio" class="custom-control-input pre-existing" value="no"> 
											<label for="checkbox_1" class="custom-control-label">No</label>
										</div> 
								</div> 
								<span class="error-message-field-pre-existing">This is a mandatory field</span>
							</div>
						</div>
						<div class="qoute-sub-heading"><h4>Contact Information</h4></div>
						<div class="row">
							<div class="col-md-3" style="">
								<div class="form-group" style="">
									<label for="input-text-1">First Name</label>
									<input type="firstname" class="form-control first-name" id="input-id-1" placeholder="First name">
									<span class="error-message-field-firstname">This is a mandatory field</span>
								</div>
							</div>
							<div class="col-md-3" style="">
								<div class="form-group" style="">
									<label for="input-id-3">Last Name</label>
									<input type="lastname" class="form-control last-name" id="input-id-3" placeholder="Last name">
									<span class="error-message-field-lastname">This is a mandatory field</span>
								</div>
							</div>
							<div class="col-md-3" style="">
								<div class="form-group" style="position: static;">
									<label for="input-id-5">Email</label>
									<input type="email" class="form-control email" id="input-id-5" placeholder="Email id">
									<span class="error-message-field-email">This is a mandatory field</span>
									<span class="error-message-valid-email">Please enter a valid email</span>
								</div>
							</div> 
							<div class="col-md-3" style="">
								<div class="form-group" style="position: static;">
									<label for="input-id-4">Phone</label>
									<input type="number" class="form-control phone" id="input-id-4" placeholder="Phone number">
									<span class="error-message-field-phone">This is a mandatory field</span>
								</div> 
							</div>
						</div> 
						<div class="row"> 
							<div class="col-sm-12 col-md-12">
								<div class="alert alert-success reg-succ" style="display:none;" role="alert"> 
									Your inquiry submitted successfully.
								</div>
								<div class="form-group text-right"> 
									<button name="submit" type="submit" class="btn btn-primary get-quote-supervisa">Submit</button> 
								</div>
								<br>
							</div>
						</div> 
				</div>  
			</div> 
			</div>
		</div>
	</div>

	<!-- Page -->
	<div class="section services section-border visa-ins-secondary-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-md-4 col-md-push-8">
					<div class="widget categories">
						<ul class="category-nav">
							<li class="active"><a href="#">Super Visa Pre-Existing Condtions</a></li>
							<li><a href="#">Super Visa Insurance FAQ's</a></li>
							<li><a href="#">Super Visa Insurance Monthly Plan</a></li>
							<li><a href="#">Super Visa Insurance Refund</a></li>
							<li><a href="#">Super Visa Insurance Coverage - Heart Patients</a></li>
							<li><a href="#">Super Visa Insurance Coverage - Alzheimer</a></li>
						</ul>
					</div>  
				</div>
				<div class="col-sm-8 col-md-8 col-md-pull-4">
					<div class="single-page"> 
						<div class="margin-bottom-30"></div>
						<h2 class="section-heading">
						Cost and Eligibility for Super Visa Insurance
						</h2>
						<p class="p-line-height">
							As per online insurance quoting websites, a single Super Life Insurance will cost you $ 138 on an average every month or $ 1660 on an average per annum. 
							This insurance is only valid for parents or grandparents of a Canadian citizen or resident who passes the minimum monthly income necessary to sponsor the applicant. Apart from the financial stability, even the purpose of the visit and a clear background check on the applicant will also decide the further proceedings of the application.
						</p>
						<p class="p-line-height">
							In case, the application is rejected by the Canadian High Commission due to any unfortunate situations, the complete amount will be settled as a refund prior to the commencement of the insurance coverage period. In all other cases, a refund will be provided after a deduction of a minimum of $100 to a maximum of $250 as administration fee charges.
						</p>  
					 </div>
				</div>

			</div>
			
		</div>
	</div> 
	
	<div class="section news section-border">
		<div class="container"> 
				<h3 style="color: #2ba7de;">
					Read more about Super Visa Insurance
				</h3>
			<div class="row grid-services"> 
				<div class="col-sm-6 col-md-4">
					<div class="box-news-1">
						<div class="image">
							<a href="news-detail.html" title="House Insurance">
								<img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive">
							</a>
						</div>
						<div class="meta-date">March 20, 2016</div>
						<h3 class="blok-title"><a href="#" title="How to protect your kid for their future dream.">How to protect your kid for their future dream.</a></h3>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box-news-1">
						<div class="image">
							<a href="news-detail.html" title="House Insurance">
								<img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive">
							</a>
						</div>
						<div class="meta-date">March 20, 2016</div>
						<h3 class="blok-title"><a href="#" title="How to protect your kid for their future dream.">Traveling more confident with our insurance.</a></h3>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box-news-1">
						<div class="image">
							<a href="news-detail.html" title="House Insurance">
								<img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive">
							</a>
						</div>
						<div class="meta-date">March 20, 2016</div>
						<h3 class="blok-title"><a href="#" title="How to protect your kid for their future dream.">Your vacation more confident and safety with us.</a></h3>
					</div>
				</div>  
			</div>  
		</div>
	</div> 
<?php
get_footer(); 