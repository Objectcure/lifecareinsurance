<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */
ob_start();
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>
		<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<link rel="profile" href="https://gmpg.org/xfn/11">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/vendor/bootstrap.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/vendor/bootstrap-datepicker3.min.css" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/vendor/animate.min.css" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/vendor/font-awesome.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/vendor/owl.carousel.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/vendor/owl.theme.default.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/vendor/magnific-popup.css" type="text/css" media="screen" />
		<!-- ==============================================
		Google Fonts
		=============================================== -->
		<!-- <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"> -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:100,300,400,500,600,700" rel="stylesheet">

		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/style.css" type="text/css" media="screen" />
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/modernizr.min.js" ></script>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/vendor/sweetalert2.min.css" type="text/css" media="screen" />
		</head>
	<body>
	 <!-- Load page -->
	<div class="animationload">
		<div class="loader"></div>
	</div>
 	<!-- TOPBAR -->
	<div class="topbar">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-6">
					<div class="topbar-left">
						<div class="welcome-text">
						TOLL FREE : <a href="tel:+1 877 495 2525">1-877-495-2525</a>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<div class="topbar-right">
						<ul class="topbar-menu">
							<li><a href="#" title="Find An Agents">Blog</a></li>
							<li><a href="#" title="FAQ">FAQ's</a></li>
							<li><a href="<?php echo site_url();?>/careers" title="Careers">Careers</a></li>
						</ul>
						<ul class="topbar-sosmed">
							<li>
								<a href="#"><i class="fa fa-facebook"></i></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-twitter"></i></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-instagram"></i></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-pinterest"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="top-nav-main">
		<!-- TOPBAR LOGO SECTION -->
		<div class="topbar-logo">
			<div class="container">
				<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
					<img width="198" src="<?php bloginfo('template_url'); ?>/assets/images/logo-new.svg" alt="Logo" />
				</a>
				 <div class="contact-info">

					<div class="box-icon-1">
						<div class="icon">
							<div class="fa fa-phone"></div>
						</div>
						<div class="body-content">
							<div class="heading">905-791-7781</div>
							info@lifecareinsurance.com
						</div>
					</div>
					<div class="box-icon-1">
						<div class="icon">
							<div class="fa fa-map-marker"></div>
						</div>
						<div class="body-content">
							<div class="heading">8500 Torbram Road, Unit 44</div>
							Brampton, ON, Canada, L6T 5C6
						</div>
					</div>

				</div>
			</div>
		</div>
		<!-- NAVBAR SECTION -->
		<div class="navbar navbar-main">
			<div class="container container-nav">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- <a href="contact.html" title="" class="btn btn-orange-cta pull-right btn-view-all">GET A QUOTE</a> -->
				<nav class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-left main-nav">
						<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ABOUT US <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="about-company.html">Who We Are ?</a></li>
							<li><a href="about-history.html">Contact Us</a></li>
							<li><a href="about-team.html">Work With Us</a></li>
						</ul>
						</li>
						<li class="dropdown insurance-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">INSURANCE <span class="caret"></span></a>
						<div class="dropdown-menu">
							<div class="row">
								<div class="col-md-4">
									<p>For Yourself</p>
									<ul>
										<li><a href="#">Health & Dental</a></li>
										<li><a href="#">Non-medical Health insurance</a></li>
										<li><a href="<?php echo site_url();?>/student-insurance">Student insurance</a></li> 
										<li><a href="<?php echo site_url();?>/disability-insurance">Disability insurance</a></li>
										<li><a href="<?php echo site_url();?>/critical-illness-insurance">Critical illness insurance</a></li>
									</ul>
								</div>
								<div class="col-md-4">
									<p>For Your Family</p>
								<ul>
									<li><a href="<?php echo site_url();?>/mortgage-insurance">Mortgage insurance</a></li>
									<li><a href="<?php echo site_url();?>/term-life-insurance">Term Life insurance</a></li>
									<li><a href="<?php echo site_url();?>/universal-life-insurance">Universal Life insurance</a></li>
									<li><a href="<?php echo site_url();?>/whole-life-insurance">Whole Life insurance</a></li>
									<li><a href="#">Youth/Junior insurance</a></li>
									<li><a href="#">Funeral insurance</a></li>
									</ul>
								</div>
								<div class="col-md-4">
									<p>For Travellers</p>
								<ul>
									<li><a href="<?php echo site_url();?>/travel-insurance">Travel insurance</a></li>
									<li><a href="<?php echo site_url();?>/super-visa-insurance">Super Visa insurance</a></li>
									<li><a href="#">Visitor insurance</a></li>
									<li><a href="#">Snowbird insurance</a></li>
									</ul>
								</div>
							</div>
						</div>
						</li>
						<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">INVESTMENTS <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Financial Planning</a></li>
							<li><a href="#">Estate Planning</a></li>
							<li><a href="#">RESP</a></li>
							<li><a href="#">RRSP</a></li>
							<li><a href="#">TSFA</a></li>
							<li><a href="#">Registered Investment Fund (RIF)</a></li>
							<li><a href="#">Non-registered Investment Fund</a></li>
							<li><a href="#">GIC (term)</a></li>
							<li><a href="#">EHSP</a></li>
						</ul>
						</li>
						<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">TOOLS <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Calculators</a></li>
						</ul>
						</li>
						<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">RESOURCES <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Links</a></li>
							<li><a href="#">Claims</a></li>
						</ul>
						</li>
						<li class="">
						<a href="#" class=""  role="button" aria-haspopup="true" aria-expanded="false">BLOG<span class=""></span></a>
						</li>

					</ul>
				</nav>
			</div>
		</div>
	</div>
