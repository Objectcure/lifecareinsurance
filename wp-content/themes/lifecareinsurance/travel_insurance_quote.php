<?php
 
 /* Template Name: Travel Insurance Quote */ 
 
 get_header();
 ?>

<!-- BANNER -->
<!-- <div class="common-banner-section banner-page services">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="title-page"><?php echo get_the_title(); ?></div> 
			</div>
		</div>
	</div>
</div> -->
	<!-- Main Header Content Part --> 
<div class="section pages section-border">
	<ul class="breadcrumb" style="margin: 0px 0px 0px 0px !important;padding: 8px 60px;">
		<li><a href="<?php echo get_home_url(); ?>">Home</a></li>
		<li class="active"><?php echo get_the_title(); ?></li>
	</ul>
	<!-- <div class="container"> 
		<div class="row">
			<div class="col-sm-8 col-md-8">
				<h2 class="section-heading">
					About Travel Insurance
				</h2>
				<div class="section-subheading">Super Visa issuing came with a set of mandatory requirements such as medical insurance for a minimum of 1 year for each visa applicant.The insurance must include:</div> 
				<ul class="checklist">
					<li>Coverage for emergency medical attention, hospitalization and repatriation.</li>
					<li>Minimum insurance coverage of $ 100,000.</li>
					<li>Availability for review by a port of entry officer with each entry.</li>
					<li>Validity throughout the stay of the visa holder.</li>
				</ul>
				<div>Seeing the demand for Super Visa Insurance many insurance companies in Canada have come forward to provide Super Visa Insurance services.		</div>
			</div>  

			<div class="col-sm-4 col-md-4">
				<img src="<?php bloginfo('template_url'); ?>/assets/images/travel-insurance.jpg" class="Travel Insurance" />

			</div>
		</div> 
		</div>
	</div> -->
</div>

<div class="section pages section-border section-get-quote" id="get-quote-section">
	<div class="container qoute-container">
		<div class="travel-quote-form">
			<h2 class="section-heading get-a-quote">
				GET A FREE QUOTE <?php //echo get_template_directory(); ?>
			</h2>  		 
			<div class="qoute-sub-heading">
				<h4>Customer Details</h4>
			</div>
			<div class="row">
				<div class="col-md-6" style="">
					<div class="form-group" style="">
						<label for="input-text-1">First Name</label>
						<input type="firstname" class="form-control first-name" id="input-id-1" placeholder="E.g: John">
						<small class="error-message-field-firstname">This is a mandatory field</small>
						<small class="error-message-string-firstname">Invalid characters</small>
					</div>
				</div>
				<div class="col-md-6" style="">
					<div class="form-group" style="">
						<label for="input-id-3">Last Name</label>
						<input type="lastname" class="form-control last-name" id="input-id-3" placeholder="E.g: Resig">
						<small class="error-message-field-lastname">This is a mandatory field</small>
						<small class="error-message-string-lastname">Invalid characters</small>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6" style="">
					<div class="form-group" style="position: static;">
						<label for="input-id-5">Email</label>
						<input type="email" class="form-control email" id="input-id-5" placeholder="E.g: mymail@mail.com">
						<small class="error-message-field-email">This is a mandatory field</small>
						<small class="error-message-valid-email">Please enter a valid email</small>
					</div>
				</div> 
				<div class="col-md-6" style="">
					<div class="form-group" style="position: static;">
						<label for="input-id-4">Phone</label>
						<input type="tel" minlength="10" maxlength="16" id="phoneNumber" class="form-control phone" id="input-id-4" placeholder="E.g:(222-333-4444)">
						<small class="error-message-field-phone">This is a mandatory field</small>
						<small class="error-message-field-phone-format">Please enter a valid Canadian phone number</small>
					</div> 
				</div>
			</div>
			<div class="qoute-sub-heading">
				<h4>Travellers Details</h4>
			</div>
			<input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" class="admin-ajax" />
			<input type="hidden" value="0" class="benefits" />
			<div class="row">
				<div class="col-md-6" style="">
					<div class="form-group date-container1" style="">
						<label for="input-id-1">Primary Applicant Date of Birth</label>
						<input type="text" class="form-control datepicker1 dob" id="input-id-1" data-date-end-date="0d" placeholder="Select Date of Birth">
						<small class="error-message-field-dob">This is a mandatory field</small>
					</div>
				</div> 
				<div class="col-md-6" style="">
					<div class="form-group" style="">
						<label for="select-3">Pre Existing Conditions:</label>
						<div>
							<div class="custom-control custom-checkbox custom-control-inline">
								<input name="pre_existing_1" id="checkbox_0" type="radio" class="custom-control-input pre-existing" value="yes"> 
								<label for="checkbox_0" class="custom-control-label">Yes</label>
							</div>
							<div class="custom-control custom-checkbox custom-control-inline">
								<input name="pre_existing_1" id="checkbox_1" type="radio" class="custom-control-input pre-existing" value="no"> 
								<label for="checkbox_1" class="custom-control-label">No</label>
							</div> 
						</div>
					</div> 
					<small class="error-message-field-pre-existing-1">This is a mandatory field</small>  
				</div> 
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="add-applicant">
						<button class="btn btn-primary" id="add_applicant"><i class="fa fa-plus" aria-hidden="true"></i> Add Applicant</button>
						<button class="btn btn-primary" id="add_child"><i class="fa fa-plus" aria-hidden="true"></i> Add Child</button>
					</div>
					<br>
				</div>
			</div>
			<div class="row applicant-elem hide">
				<div class="col-md-6">
					<div class="form-group date-container4">
						<label for="input-id-1">Applicant Date of Birth</label>
						<input type="text" class="form-control datepicker4 appl2-dob" id="input-id-10" data-date-end-date="0d" placeholder="Select Date of Birth">
						<small class="error-message-field-appl2-dob">This is a mandatory field</small>
					</div>
				</div>
				<div class="col-md-6 applicant-2-pre">
					<div class="form-group" style="">
						<label for="select-3">Pre Existing Conditions</label>
						<div>
							<div class="custom-control custom-checkbox custom-control-inline">
								<input name="pre_existing_2" id="checkbox_0" type="radio" class="custom-control-input pre-existing" value="yes"> 
								<label for="checkbox_0" class="custom-control-label">Yes</label>
							</div>
							<div class="custom-control custom-checkbox custom-control-inline">
								<input name="pre_existing_2" id="checkbox_1" type="radio" class="custom-control-input pre-existing" value="no"> 
								<label for="checkbox_1" class="custom-control-label">No</label>
							</div> 
						</div>
					</div> 
					<small class="error-message-field-pre-existing-2">This is a mandatory field</small> 
				</div> 
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group applicant-2-relationship">
								<label for="input-id-1">Relationship with Primary Applicant</label> 
								<div>
									<div class="custom-control custom-checkbox custom-control-inline">
										<input name="relationship" type="radio" class="custom-control-input relationship" value="spouse"> 
										<label for="" class="custom-control-label">Spouse</label>
									</div>
									<div class="custom-control custom-checkbox custom-control-inline">
										<input name="relationship" type="radio" class="custom-control-input relationship" value="other"> 
										<label for="" class="custom-control-label">Other</label>
									</div> 
								</div>
								<small class="error-message-field-relationship">This is a mandatory field</small>
							</div>
						</div>
						<div class="col-md-6 remove-applicant">
							<button class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Remove</button>
						</div>
					</div>	
				</div>
			</div>	
			<div class="row child1 child-elem hide">
				<div class="col-md-4">
					<div class="form-group child1-dob-container">
						<label for="input-id-1">First Child Date of Birth</label>
						<input type="text" class="form-control child1-dob" id="input-id-10" data-date-end-date="0d" placeholder="Select Date of Birth">
						<small class="error-message-field-child1-dob">This is a mandatory field</small>
					</div>
				</div>
				<div class="col-md-4 child1-pre">
					<div class="form-group">
						<label>Pre Existing Conditions</label>
						<div>
							<div class="custom-control custom-checkbox custom-control-inline">
								<input name="child1_pre_existing" id="child1_checkbox_1" type="radio" class="custom-control-input pre-existing" value="yes"> 
								<label for="child1_checkbox_1" class="custom-control-label">Yes</label>
							</div>
							<div class="custom-control custom-checkbox custom-control-inline">
								<input name="child1_pre_existing" id="child1_checkbox_2" type="radio" class="custom-control-input pre-existing" value="no"> 
								<label for="child1_checkbox_2" class="custom-control-label">No</label>
							</div> 
						</div>
					</div> 
					<small class="error-message-field-child1-pre-existing">This is a mandatory field</small> 
				</div> 
				<div class="col-md-4">
					<button class="btn btn-danger remove-child"><i class="fa fa-times" aria-hidden="true"></i> Remove</button>
				</div>
			</div>

			<div class="row child2 child-elem hide">
				<div class="col-md-4">
					<div class="form-group child2-dob-container">
						<label for="input-id-1">Second Child Date of Birth</label>
						<input type="text" class="form-control child2-dob" id="input-id-10" data-date-end-date="0d" placeholder="Select Date of Birth">
						<small class="error-message-field-child2-dob">This is a mandatory field</small>
					</div>
				</div>
				<div class="col-md-4 child2-pre">
					<div class="form-group">
						<label>Pre Existing Conditions</label>
						<div>
							<div class="custom-control custom-checkbox custom-control-inline">
								<input name="child2_pre_existing" id="child2_checkbox_1" type="radio" class="custom-control-input pre-existing" value="yes"> 
								<label for="child2_checkbox_1" class="custom-control-label">Yes</label>
							</div>
							<div class="custom-control custom-checkbox custom-control-inline">
								<input name="child2_pre_existing" id="child2_checkbox_2" type="radio" class="custom-control-input pre-existing" value="no"> 
								<label for="child2_checkbox_2" class="custom-control-label">No</label>
							</div> 
						</div>
					</div> 
					<small class="error-message-field-child2-pre-existing">This is a mandatory field</small> 
				</div> 
				<div class="col-md-4">
					<button class="btn btn-danger remove-child"><i class="fa fa-times" aria-hidden="true"></i> Remove</button>
				</div>
			</div>
							
			<div class="qoute-sub-heading">
				<h4>Trip Details</h4>
			</div>
			<div class="row">
				<div class="col-md-6" style="">
					<div class="form-group" style="position: static;">
						<label for="select-2">Trip Type</label>
						<select class="form-control trip_type" id="select-2">
							<option value="single">Single</option>
							<option value="multiple">Multi</option> 
						</select> 
					</div>
					<input type="hidden" value="ON" class="province" />
				</div>
				<div class="col-md-6" style="">
					<div class="form-group" style="position: static;">
						<label for="select-1">Destination</label>
						<select class="form-control destination" id="select-1">
							<option value="">Select Destination</option>
							<option value="canada">Canada</option>
							<option value="usa">USA</option>
							<option value="international">International</option>
						</select>
						<small class="error-message-field-destination">This is a mandatory field</small>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6" style="">
					<div class="form-group date-container2" style="position: static;">
						<label for="input-id-5">Departure Date</label>
						<input type="text" class="form-control datepicker2 effective-date" id="input-id-11" placeholder="Select Departure Date">
						<small class="error-message-field-effective">This is a mandatory field</small>
					</div> 
				</div>	 
				<div class="col-md-6" style=""> 
					<div class="form-group date-container3" style="position: static;">
						<label for="input-id-4">Return Date</label>
						<input type="text" class="form-control datepicker3 expiry-date" id="input-id-4" placeholder="Select Return Date">
						<small class="error-message-field-expiry">This is a mandatory field</small>
					</div>  
				</div> 
			</div>
			<!-- <div class="col-md-3" style="">
				<div class="form-group" style="position: static;">
					<label for="input-id-2">Departure City</label>
					<input type="text" class="form-control departure-city" id="input-id-2" placeholder="Departure City">
					<small class="error-message-field-departure">This is a mandatory field</small>
				</div>
			</div> -->
			<div class="row">
				<div class="col-md-12">
					<div class="quote-form-checkboxes">
						<div class="checkbox">
							<label><input type="checkbox" value="">Yes, I would like to receive Life Care Insurance electronic news letters and get access to relevant articles, news, tips and more. I am able to unsubscribe at any given time.</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" value="">Yes, I would like to receive a copy of Life Care Insurance Travel Checklist. I can unsubscribe at any time.</label>
						</div>
						<p>We vale your privacy. Life Care Insurance will not trade or sell your name to third parties except when required to fulfill services you request.</p>
						<p><small>For details see our <a href="#">Privacy Policy</a></small></p>
						<br>
						<a href="#">Lifecareinsurance.ca</a>
						<p>8500 Torbram Road, Unit 44 Brampton, ON, Canada, L6T 5C6</p>
					</div>
				</div>
			</div>
			<div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="alert alert-success reg-succ" style="display:none;" role="alert"> 
						Your inquiry submitted successfully.
					</div>
					<div class="form-group text-center" style="margin-top:15px;"> 
						<span class="form-loader" style="display:none;"><img width="180" src="<?php bloginfo('template_url'); ?>/assets/images/loader.gif" alt="Loader" /></span>
						<a href="javascript:void(0);" class="btn btn-info page-reset">Reset</a> 
						<button name="submit" type="submit" class="btn btn-primary get-quote-travel-insurance">Get Quotes</button> 
					</div>
					<br>
				</div>
			</div>
		</div>
	</div>  
</div> 

	<!-- Page -->
<!-- <div class="section services section-border visa-ins-secondary-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-md-4 col-md-push-8">
				<div class="widget categories">
					<ul class="category-nav">
						<li class="active"><a href="javascript:void(0)" data-href="#pre-conditions">Super Visa Pre-Existing Condtions</a></li>
						<li><a href="javascript:void(0)" data-href="#faqs">Super Visa Insurance FAQ's</a></li>
						<li><a href="javascript:void(0)" data-href="#monthly-plan">Super Visa Insurance Monthly Plan</a></li>
						<li><a href="javascript:void(0)" data-href="#refund">Super Visa Insurance Refund</a></li>
						<li><a href="javascript:void(0)" data-href="#heart-patients">Super Visa Insurance Coverage - Heart Patients</a></li>
						<li><a href="javascript:void(0)" data-href="#alzheimer">Super Visa Insurance Coverage - Alzheimer</a></li>
					</ul>
				</div>  
			</div>
			<div class="col-sm-8 col-md-8 col-md-pull-4">
				<div id="pre-conditions" class="category-content show">
					<div class="single-page"> 
						<h2 class="section-heading">
						Cost and Eligibility for Super Visa Insurance
						</h2>
						<p class="p-line-height">
							As per online insurance quoting websites, a single Super Life Insurance will cost you $ 138 on an average every month or $ 1660 on an average per annum. 
							This insurance is only valid for parents or grandparents of a Canadian citizen or resident who passes the minimum monthly income necessary to sponsor the applicant. Apart from the financial stability, even the purpose of the visit and a clear background check on the applicant will also decide the further proceedings of the application.
						</p>
						<p class="p-line-height">
							In case, the application is rejected by the Canadian High Commission due to any unfortunate situations, the complete amount will be settled as a refund prior to the commencement of the insurance coverage period. In all other cases, a refund will be provided after a deduction of a minimum of $100 to a maximum of $250 as administration fee charges.
						</p> 
					</div>
				</div>
				<div id="faqs" class="category-content">
					<div class="single-page"> 
						<h2 class="section-heading">
						Cost and Eligibility for Super Visa Insurance
						</h2>
						<p class="p-line-height">
							As per online insurance quoting websites, a single Super Life Insurance will cost you $ 138 on an average every month or $ 1660 on an average per annum. 
							This insurance is only valid for parents or grandparents of a Canadian citizen or resident who passes the minimum monthly income necessary to sponsor the applicant. Apart from the financial stability, even the purpose of the visit and a clear background check on the applicant will also decide the further proceedings of the application.
						</p>
						<p class="p-line-height">
							In case, the application is rejected by the Canadian High Commission due to any unfortunate situations, the complete amount will be settled as a refund prior to the commencement of the insurance coverage period. In all other cases, a refund will be provided after a deduction of a minimum of $100 to a maximum of $250 as administration fee charges.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>  -->
	
<!-- <div class="section news section-border">
	<div class="container"> 
			<h3 style="color: #2ba7de;">
				Read more about Super Visa Insurance
			</h3>
		<div class="row grid-services"> 
			<div class="col-sm-6 col-md-4">
				<div class="box-news-1">
					<div class="image">
						<a href="news-detail.html" title="House Insurance">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive">
						</a>
					</div>
					<div class="meta-date">March 20, 2016</div>
					<h3 class="blok-title"><a href="#" title="How to protect your kid for their future dream.">How to protect your kid for their future dream.</a></h3>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="box-news-1">
					<div class="image">
						<a href="news-detail.html" title="House Insurance">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive">
						</a>
					</div>
					<div class="meta-date">March 20, 2016</div>
					<h3 class="blok-title"><a href="#" title="How to protect your kid for their future dream.">Traveling more confident with our insurance.</a></h3>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="box-news-1">
					<div class="image">
						<a href="news-detail.html" title="House Insurance">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive">
						</a>
					</div>
					<div class="meta-date">March 20, 2016</div>
					<h3 class="blok-title"><a href="#" title="How to protect your kid for their future dream.">Your vacation more confident and safety with us.</a></h3>
				</div>
			</div>  
		</div>  
	</div>
</div>  -->
<?php
get_footer(); 