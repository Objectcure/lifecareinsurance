<?php
 /* Template Name: Quote Success */ 
get_header(); 
ob_start(); 
?> 
	<!-- Main Header Content Part --> 
    <div class="section pages section-border">
        <ul class="breadcrumb" style="margin: 0px 0px 0px 0px !important;padding: 8px 60px;">
            <li><a href="<?php echo get_home_url(); ?>">Home</a></li>
            <li class="active">Quote Success</li>
        </ul>  
        <div class="container qoute-container"> 
            <div class="row">
                <div class="col-sm-12 col-md-12">   
                    <!-- Result product grid display started -->  
                    <div class="alert alert-get-quote" style="margin-top: 20px" role="alert">
                        <div class="row">
                            <div class="col-xs-3 col-md-12">
                            <br>
                                <div class="alert alert-success" role="alert">
                                    Thank you for the inquiry. Your quote successfully sent to your email. We will get back you very soon for the updates. 
                                </div>
                            </div> 
                        </div>
                    </div> 
                    
            </div>
        </div>
    </div>  
</div> 

<?php
get_footer(); 