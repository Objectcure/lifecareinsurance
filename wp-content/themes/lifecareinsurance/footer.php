<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
	<!-- Scroll to top -->
	<div class="to-top"><i class="fa fa-arrow-up" aria-hidden="true"></i></div>
	<!-- FOOTER SECTION -->
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<div class="footer-item">
						<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
						<img width="198" src="<?php bloginfo('template_url'); ?>/assets/images/logo-new.svg" alt="" />
						</a>
						<p>Successfully running a premier agency in the Brampton area, and leading a team of successful and progressive brokers in the insurance and investment industry.</p>
						<div class="footer-sosmed">
							<a href="https://www.facebook.com/lifecareinsurancebrampton/" target="_blank" title="Lifecareinsurance">
								<div class="item">
									<i class="fa fa-facebook"></i>
								</div>
							</a>
							<a href="https://www.linkedin.com/company/27222208/" title="Lifecareinsurance" target="_blank">
								<div class="item">
									<i class="fa fa-linkedin"></i>
								</div>
							</a>
							<a href="https://www.instagram.com/life.care.insurance/" title="Lifecareinsurance" target="_blank">
								<div class="item">
									<i class="fa fa-instagram"></i>
								</div>
							</a>
							<a href="https://www.youtube.com/channel/UCFddQnsb544mCjCQ1aqCF_g" target="_blank" title="Lifecareinsurance">
								<div class="item">
									<i class="fa fa-youtube"></i>
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="footer-item">
						<div class="footer-title">
							Contact Info
						</div>
						<ul class="list-info">
							<li>
								<div class="info-icon">
									<span class="fa fa-map-marker"></span>
								</div>
								<div class="info-text">8500 Torbram Road, Unit 44 Brampton, ON, Canada, L6T 5C6</div> </li>
							<li>
								<div class="info-icon">
									<span class="fa fa-phone"></span>
								</div>
								<div class="info-text">905-791-7781</div>
							</li>
							<li>
								<div class="info-icon">
									<span class="fa fa-fax"></span>
								</div>
								<div class="info-text">905-791-7735</div>
							</li>
							<li>
								<div class="info-icon">
									<span class="fa fa-envelope"></span>
								</div>
								<div class="info-text">info@lifecareinsurance.ca</div>
							</li>
							<li>
								<div class="info-icon">
									<span class="fa fa-user"></span>
								</div>
								<div class="info-text">Saurabh Rattan : 416-833 8533</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="footer-item">
						<div class="footer-title">
							Our Services
						</div>
						<ul class="list">
							<li><a href="service-life-insurance.html" title="">Life Insurance</a></li>
							<li><a href="service-car-insurance.html" title="">Car Insurance</a></li>
							<li><a href="service-travel-insurance" title="">Travel Insurance</a></li>
							<li><a href="service-house-insurance.html" title="">House Insurance</a></li>
							<li><a href="service-vehicle-insurance.html" title="">Vehicle Insurance</a></li>
							<li><a href="service-boat-insurance.html" title="">Boat Insurance</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="footer-item">
						<div class="footer-title">
							Additional Links
						</div>
						<ul class="list">
							<li><a href="our-company.html" title="">Our Company</a></li>
							<li><a href="our-team.html" title="">Our Team</a></li>
							<li><a href="our-partners.html" title="">Our Partners</a></li>
							<li><a href="careers.html" title="">Careers</a></li>
							<li><a href="contact-us.html" title="">Contact Us</a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
		<div class="fcopy">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<p class="ftex">&copy; 2020 Lifecareinsurance - All Rights Reserved</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" value="<?php echo get_home_url(); ?>" class="site_url" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/jquery.min.js" ></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/bootstrap.min.js" ></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/sweetalert2.min.js"> </script>
	<Script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/bootstrap-datepicker.min.js"> </Script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/jquery.superslides.js" ></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/owl.carousel.js" ></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/bootstrap-hover-dropdown.min.js" ></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/jquery.magnific-popup.min.js" ></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/script.js" ></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/custom.js"> </script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/iconify.min.js"> </script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/jquery.validate.min.js"> </script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/vendor/additional-methods.js"> </script> 
	</body>
</html>
