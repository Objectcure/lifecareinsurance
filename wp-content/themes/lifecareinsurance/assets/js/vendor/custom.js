/**
 * Custom JS functions
 * Author : Jp <ideas.to.jp@gmail.com>
 * Version : 1.0
 */
$(document).ready(function() {
    window.addEventListener("scroll", function(){
        $(".top-nav-main").addClass("sticky-header animate__animated animate__fadeInDown");
        if(document.documentElement.scrollTop === 0) {
            $(".top-nav-main").removeClass("sticky-header animate__animated animate__fadeInDown");
        }
    });

    $(".to-top").click(function () {
        $('html,body').animate({ scrollTop: 0 }, 1000);
        return false;
    });

    $(".category-nav li > a").click(function(event) {
        var targetId = $(event.currentTarget).data('href');
        $(".category-nav li").removeClass("active");
        $(event.currentTarget).closest('li').addClass('active');
        $(".category-content").removeClass('show').hide();
        $(targetId).fadeIn(1000);
    });

    var site_url = $('.site_url').val();
    if (window.location.href.indexOf("generate-quote") > -1) {
        //window.onbeforeunload = function() { return "Your data will be lost."; };
    }
    if (window.location.href.indexOf("quote-success") > -1) {
        setTimeout(function(){
            window.location.replace(site_url+'/');
        },4000);
        window.history.pushState(null, "", window.location.href);
        window.onpopstate = function () {
        window.history.pushState(null, "", window.location.href);
        };
        //window.onbeforeunload = function() { window.history.forward(1); return false; };
    }

    $('.datepicker1').datepicker({
        format: 'yyyy/mm/dd',
        container: '.date-container1',
        autoclose: true,
        orientation: 'left',
        startView: 2,
        todayHighlight: true,
        defaultViewDate: {
            year: 1977,
            month: 01,
            day: 01
        }
    });
    $('.datepicker2').datepicker({
        format: 'yyyy/mm/dd',
        container: '.date-container2',
        autoclose: true,
        orientation: 'left',
        startDate: '-0d'
    });
    $('.datepicker3').datepicker({
        format: 'yyyy/mm/dd',
        container: '.date-container3',
        autoclose: true,
        orientation: 'left',
        startDate: '-0d'
    });
    $('.datepicker4').datepicker({
        format: 'yyyy/mm/dd',
        container: '.date-container4',
        autoclose: true,
        orientation: 'left',
        startView: 2,
        todayHighlight: true,
        defaultViewDate: {
            year: 1977,
            month: 01,
            day: 01
        }
    });

    $('.datepicker5').datepicker({
        format: 'yyyy/mm/dd',
        autoclose: true,
        orientation: 'left',
        startDate: '+0d',
        endDate: ''
    });
    $('.datepicker6').datepicker({
        format: 'yyyy/mm/dd',
        autoclose: true,
        orientation: 'left',
        startDate: '+0d',
        endDate: ''
    });

    // Add applicant UI event

    var clickCount = 0;

    $('.child1-dob').datepicker({
        format: 'yyyy/mm/dd',
        container: '.child1-dob-container',
        autoclose: true,
        orientation: 'left',
        startView: 2,
        todayHighlight: true,
        defaultViewDate: {
            year: 2010,
            month: 01,
            day: 01
        }
    });

    $('.child2-dob').datepicker({
        format: 'yyyy/mm/dd',
        container: '.child2-dob-container',
        autoclose: true,
        orientation: 'left',
        startView: 2,
        todayHighlight: true,
        defaultViewDate: {
            year: 2010,
            month: 01,
            day: 01
        }
    });

    /** Page reset action */
    $('.page-reset').click(function () {
        if (window.location.hash === '') {
            window.location.hash = '#get-quote-section';
            window.location.reload();
        } else {
            window.location.hash = '';
        }
    });
   /* function phoneMask() {
        var num = $(this).val().replace(/\D/g,'');
        $(this).val(num.substring(0,0) + '-' + num.substring(1,4) + '-' + num.substring(4,7) + '-' + num.substring(7,11));
    }
    $('[type="tel"]').keyup(phoneMask);*/


        // When ready.
        /*$(function() {

            var $form = $( ".qoute-container" );
            var $input = $form.find( '[type="tel"]');

            $input.on( "keyup", function( event ) {


                // When user select text in the document, also abort.
                var selection = window.getSelection().toString();
                if ( selection !== '' ) {
                    return;
                }

                // When the arrow keys are pressed, abort.
                if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                    return;
                }

                var $this = $(this);
                var input = $this.val();
                        input = input.replace(/[\W\s\._\-]+/g, '');

                    var split = 3;
                    var chunk = [];

                    for (var i = 0, len = input.length; i < len; i += split) {
                        split = ( i >= 3 && i <= 4 ) ? 4 : 3;
                        chunk.push( input.substr( i, split ) );
                    }

                    $this.val(function() {
                        return chunk.join("-").toUpperCase();
                    });
            });

        } );
        */

        const isNumericInput = (event) => {
            const key = event.keyCode;
            return ((key >= 48 && key <= 57) || // Allow number line
                (key >= 96 && key <= 105) // Allow number pad
            );
        };

        const isModifierKey = (event) => {
            const key = event.keyCode;
            return (event.shiftKey === true || key === 35 || key === 36) || // Allow Shift, Home, End
                (key === 8 || key === 9 || key === 13 || key === 46) || // Allow Backspace, Tab, Enter, Delete
                (key > 36 && key < 41) || // Allow left, up, right, down
                (
                    // Allow Ctrl/Command + A,C,V,X,Z
                    (event.ctrlKey === true || event.metaKey === true) &&
                    (key === 65 || key === 67 || key === 86 || key === 88 || key === 90)
                )
        };

        const enforceFormat = (event) => {
            // Input must be of a valid number format or a modifier key, and not longer than ten digits
            if(!isNumericInput(event) && !isModifierKey(event)){
                event.preventDefault();
            }
        };

        const formatToPhone = (event) => {
            if(isModifierKey(event)) {return;}

            // I am lazy and don't like to type things more than once
            const target = event.target;
            const input = event.target.value.replace(/\D/g,'').substring(0,10); // First ten digits of input only
            const zip = input.substring(0,3);
            const middle = input.substring(3,6);
            const last = input.substring(6,10);

            if(input.length > 6){target.value = `${zip}-${middle}-${last}`;}
            else if(input.length > 3){target.value = `${zip}-${middle}`;}
            else if(input.length > 0){target.value = `${zip}`;}
        };

        const inputElement = document.getElementById('phoneNumber');
        if(inputElement !== null){
            inputElement.addEventListener('keydown',enforceFormat);
            inputElement.addEventListener('keyup',formatToPhone);
        }

    $('#add_applicant').click(function (event){
        $('#add_applicant').addClass('disabled');
        $('.applicant-elem').removeClass('hide');
    });

    $('#add_child').click(function (event) {
        clickCount = $('.child-elem:visible').length + 1;
        console.log(clickCount);
        if(clickCount <= 2 ) {
            $('.child' + clickCount).removeClass('hide');
        } else {
            Swal.fire({
                icon: "warning",
                text: 'Please contact us for getting Quote for more than 2 Childrens',
                footer: '<a href="#" class="btn btn-primary">Call On : 1 877 495 2525</a>',
                showConfirmButton: false
              })
        }
        clickCount = $('.child-elem').length + 1;
    });

    $(".remove-child").click(function(e) {
        var targetParent = $(e.target).closest('.child-elem');
        targetParent.find('input[type="text"]').val('');
        targetParent.find('input[type="radio"]:checked').prop('checked', false);
        targetParent.find('small[class^="error-message-"]').hide();
        if(targetParent.hasClass('child1')) {
            clickCount = 1;
        } else if(targetParent.hasClass('child2')) {
            clickCount = 2;
        }
        targetParent.addClass('hide');
    });

    $(".remove-applicant .btn").click(function(e) {
        var targetParent = $(e.currentTarget).closest('.applicant-elem');
        targetParent.find('input[type="text"]').val('');
        targetParent.find('input[type="radio"]:checked').prop('checked', false);
        targetParent.find('small[class^="error-message-"]').hide();
        targetParent.addClass('hide');
        $("#add_applicant").removeClass('disabled');
    });

    // Super vis insurance Action handler
    $('.get-quote-supervisa').click(function () {
        //var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
        var ajax_url = $('.admin-ajax').val();
        // data validation
        var dob = $('.dob').val(); if(dob == ''){$('.error-message-field-dob').show();return false;}else{$('.error-message-field-dob').hide();}
        var eff_date = $('.effective-date').val();if(eff_date == ''){$('.error-message-field-effective').show();return false;}else{$('.error-message-field-effective').hide();}
        var exp_date = $('.expiry-date').val(); if(exp_date == ''){$('.error-message-field-expiry').show();return false;}else{$('.error-message-field-expiry').hide();}
        var dep_city = $('.departure-city').val(); //if(dep_city == ''){$('.error-message-field-departure').show();return false;}else{$('.error-message-field-departure').hide();}
        var province = $('.province').val(); if(province == ''){$('.error-message-field-province').show();return false;}else{$('.error-message-field-province').hide();}
        var benefits = $('.benefits').val(); if(benefits == ''){$('.error-message-field-benefits').show();return false;}else{$('.error-message-field-benefits').hide();}
        var deductable = $('.deductable').val(); if(deductable == ''){$('.error-message-field-deductable').show();return false;}else{$('.error-message-field-deductable').hide();}
        var pre_exist = $('input[name=pre_existing]:checked').val();if((typeof pre_exist === "undefined")){$('.error-message-field-pre-existing').show();return false;}else{$('.error-message-field-pre-existing').hide();}
        var first_name = $('.first-name').val(); if(first_name == ''){$('.error-message-field-firstname').show();return false;}else{$('.error-message-field-firstname').hide();}
        var last_name = $('.last-name').val(); if(last_name == ''){$('.error-message-field-lastname').show();return false;}else{$('.error-message-field-lastname').hide();}
        var email = $('.email').val(); if(email == ''){$('.error-message-field-email').show();return false;}else{$('.error-message-field-email').hide();}
        var emailvalidate = validateEmail(email);
        if(emailvalidate === false){$('.error-message-valid-email').show();return false;}else{$('.error-message-valid-email').hide();}
        var phone = $('.phone').val(); if(phone == ''){$('.error-message-field-phone').show();return false;}else{$('.error-message-field-phone').hide();}

        $.ajax({
            url: ajax_url, // or example_ajax_obj.ajaxurl if using on frontend
            data: {
                'action': 'get_a_qoute',
				'dob' : dob,
				'effective_date' : eff_date,
				'expiry_date' : exp_date,
				//'departure_city' : dep_city,
				'province' : province,
				'benefits' : benefits,
                'deductable' : deductable,
                'pre_exist' : pre_exist,
				'first_name' : first_name,
				'last_name' : last_name,
				'email' : email,
				'phone' : phone
            },
            type: 'POST',
            success:function(data) {
                if(data == 1){
                    $('.reg-succ').show();
                        setTimeout(function(){
                            $('.reg-succ').hide();
                            location.reload();
                        },4000);
                }else if(data == 0){
                    alert('Server is not responding');
                    //location.reload();
                }
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        });
    });

    function getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
        {
            age--;
        }
        return age;
    }

    // Travel insurance functionality

    $('.get-quote-travel-insurance').click(function () {
        var getdob2 = $('.applicant-elem').hasClass('hide');
        var child1 = $('.child1').hasClass('hide');
        var child2 = $('.child2').hasClass('hide');
        //var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
        var ajax_url = $('.admin-ajax').val();
        var sum_assured = $('.benefits').val();
        // data validation
        var first_name = $('.first-name').val();
        if(first_name == '') {
            $('.error-message-field-firstname').show();
            return false;
        } else if(!/^[a-zA-Z]*$/g.test(first_name)) {
            $('.error-message-string-firstname').show();
            return false;
        }
        else{$('.error-message-field-firstname').hide();$('.error-message-string-firstname').hide();}
        var last_name = $('.last-name').val();
        if(last_name == ''){
            $('.error-message-field-lastname').show();
            return false;
        } else if(!/^[a-zA-Z]*$/g.test(last_name)) {
            $('.error-message-string-lastname').show();
            return false;
        }
        else{
            $('.error-message-field-lastname').hide();
            $('.error-message-string-lastname').hide();
        }
        var email = $('.email').val(); if(email == ''){$('.error-message-field-email').show();return false;}else{$('.error-message-field-email').hide();}
        var emailvalidate = validateEmail(email);
        if(emailvalidate === false){$('.error-message-valid-email').show();return false;}else{$('.error-message-valid-email').hide();}
        var phone = $('.phone').val(); if(phone == ''){$('.error-message-field-phone').show();return false;}else{$('.error-message-field-phone').hide();}
        /*var isphone = /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/.test(phone);
        if(isphone == false){$('.error-message-field-phone-format').show();return false;}else{$('.error-message-field-phone-format').hide();} */

        var dob = $('.dob').val(); if(dob == ''){$('.error-message-field-dob').show();return false;}else{$('.error-message-field-dob').hide();}
        var pre_exist_1 = $('input[name=pre_existing_1]:checked').val();if((typeof pre_exist_1 === "undefined")){$('.error-message-field-pre-existing-1').show();return false;}else{$('.error-message-field-pre-existing-1').hide();}

        var getAge1 = getAge(dob);

        if(!getdob2){
            var dob_app_2 = $('.appl2-dob').val(); if(dob_app_2 == ''){$('.error-message-field-appl2-dob').show();return false;}else{$('.error-message-field-appl2-dob').hide();}
            var pre_exist_2 = $('input[name=pre_existing_2]:checked').val();if((typeof pre_exist_2 === "undefined")){$('.error-message-field-pre-existing-2').show();return false;}else{$('.error-message-field-pre-existing-2').hide();}

            var relationship = $('.relationship:checked').val(); //console.log(relationship);
            if(relationship == undefined){$('.error-message-field-relationship').show();return false;}else{$('.error-message-field-relationship').hide();}



        }

        if(!child1){
            var dob_child_1 = $('.child1-dob').val(); if(dob_child_1 == ''){$('.error-message-field-child1-dob').show();return false;}else{$('.error-message-field-child1-dob').hide();}
            var pre_exist_child1 = $('input[name=child1_pre_existing]:checked').val();if((typeof pre_exist_child1 === "undefined")){$('.error-message-field-child1-pre-existing').show();return false;}else{$('.error-message-field-child1-pre-existing').hide();}
            if(pre_exist_child1 === 'yes'){
                Swal.fire({
                    icon: "warning",
                    text: 'Please contact us for getting Quote for Childrens with pre existing conditions.',
                    footer: '<a href="#" class="btn btn-primary">Call On : 1 877 495 2525</a>',
                    showConfirmButton: false
                  })
                return false;
            }
        }

        if(!child2){
            var dob_child_2 = $('.child2-dob').val(); if(dob_child_2 == ''){$('.error-message-field-child2-dob').show();return false;}else{$('.error-message-field-child2-dob').hide();}
            var pre_exist_child2 = $('input[name=child2_pre_existing]:checked').val();if((typeof pre_exist_child2 === "undefined")){$('.error-message-field-child2-pre-existing').show();return false;}else{$('.error-message-field-child2-pre-existing').hide();}
            if(pre_exist_child2 === 'yes'){
                Swal.fire({
                    icon: "warning",
                    text: 'Please contact us for getting Quote for Childrens with pre existing conditions.',
                    footer: '<a href="tel:1-562-867-5309" class="btn btn-primary">Call us now</a>',
                    showConfirmButton: false
                  })
                return false;
            }
        }

        var destination = $('.destination').val();if(destination == ''){$('.error-message-field-destination').show();return false;}else{$('.error-message-field-destination').hide();}
        var departure_date = $('.effective-date').val();if(departure_date == ''){$('.error-message-field-effective').show();return false;}else{$('.error-message-field-effective').hide();}
        var return_date = $('.expiry-date').val(); if(return_date == ''){$('.error-message-field-expiry').show();return false;}else{$('.error-message-field-expiry').hide();}
        var province = $('.province').val(); if(province == ''){$('.error-message-field-province').show();return false;}else{$('.error-message-field-province').hide();}

        var trip_type = $('.trip_type').val();
        var destination = $('.destination').val();

        // Redirect to create quote page
        var webUrl = document.location;
        //console.log(webUrl);

        if(!getdob2){
            var getAge2 = getAge(dob_app_2);

            if(pre_exist_1 == 'no' && pre_exist_2 == 'yes' || pre_exist_1 == 'yes' && pre_exist_2 == 'no'){
                if(relationship == 'spouse'){
                    Swal.fire({
                        icon: "warning",
                        text: 'It is better to apply individuallly. Otherwise, the premium calculation will be processed based on the spousal age',
                        showConfirmButton: true
                        }).then((result) => {
                            if(result) {
                                $.ajax({
                                    url: ajax_url, // or example_ajax_obj.ajaxurl if using on frontend
                                    data: {
                                        'action': 'generate_qoute',
                                        'dob_1' : dob,
                                        'pre_exist_1' : pre_exist_1,
                                        'dob_2' : dob_app_2,
                                        'pre_exist_2' : pre_exist_2,
                                        'departure_date' : departure_date,
                                        'relationship' : relationship,
                                        'dob_child_1' : dob_child_1,
                                        'pre_exist_child1' : pre_exist_child1,
                                        'dob_child_2' : dob_child_2,
                                        'pre_exist_child2' : pre_exist_child2,
                                        'return_date' : return_date,
                                        'trip_type' : trip_type,
                                        'destination' : destination,
                                        'first_name' : first_name,
                                        'last_name' : last_name,
                                        'email' : email,
                                        'phone' : phone,
                                        'dob' : dob
                                    },
                                    type: 'POST',
                                    crossDomain: true,
                                    success:function(data) {
                                        $('.form-loader').show();
                                        setTimeout(function(){
                                            $('.form-loader').hide();
                                            window.location.replace(webUrl+"/generate-quote?dob1="+dob+'&ext1='+pre_exist_1+'&dob2='+dob_app_2+'&ext2='+pre_exist_2+'&relationship='+relationship+'&dob_child_1='+dob_child_1+'&pre_exist_child1='+pre_exist_child1+'&dob_child_2='+dob_child_2+'&=pre_exist_child2'+pre_exist_child2+'&sum='+sum_assured+'&dep='+departure_date+'&arv='+return_date+'&method=travel_insurance&tr_ty='+trip_type+'&des='+destination+'&qnos='+data);
                                        },500);
                                    },
                                    error: function(errorThrown){
                                        console.log(errorThrown);
                                    }
                                });
                            }
                        });

                        return false;

                }else if(getAge2 > getAge1){

                    Swal.fire({
                        icon: "warning",
                        text: 'It is suggested that the primary applicant can be the eldest person. Otherwise, the premium calculation will be processed based on the eldest persons age',
                        footer: '<button class="btn btn-primary">Continue</button>',
                        showConfirmButton: false
                        }).then((result) => {
                            if(result) {
                                $.ajax({
                                    url: ajax_url, // or example_ajax_obj.ajaxurl if using on frontend
                                    data: {
                                        'action': 'generate_qoute',
                                        'dob_1' : dob,
                                        'pre_exist_1' : pre_exist_1,
                                        'dob_2' : dob_app_2,
                                        'pre_exist_2' : pre_exist_2,
                                        'departure_date' : departure_date,
                                        'relationship' : relationship,
                                        'dob_child_1' : dob_child_1,
                                        'pre_exist_child1' : pre_exist_child1,
                                        'dob_child_2' : dob_child_2,
                                        'pre_exist_child2' : pre_exist_child2,
                                        'return_date' : return_date,
                                        'trip_type' : trip_type,
                                        'destination' : destination,
                                        'first_name' : first_name,
                                        'last_name' : last_name,
                                        'email' : email,
                                        'phone' : phone,
                                        'dob' : dob
                                    },
                                    type: 'POST',
                                    crossDomain: true,
                                    success:function(data) {
                                        $('.form-loader').show();
                                        setTimeout(function(){
                                            $('.form-loader').hide();
                                            window.location.replace(webUrl+"/generate-quote?dob1="+dob+'&ext1='+pre_exist_1+'&dob2='+dob_app_2+'&ext2='+pre_exist_2+'&relationship='+relationship+'&dob_child_1='+dob_child_1+'&pre_exist_child1='+pre_exist_child1+'&dob_child_2='+dob_child_2+'&=pre_exist_child2'+pre_exist_child2+'&sum='+sum_assured+'&dep='+departure_date+'&arv='+return_date+'&method=travel_insurance&tr_ty='+trip_type+'&des='+destination+'&qnos='+data);
                                        },500);
                                    },
                                    error: function(errorThrown){
                                        console.log(errorThrown);
                                    }
                                });
                            }
                        });

                        return false;

                }else{

                    Swal.fire({
                        icon: "warning",
                        text: 'It is suggested that the primary applicant can be the eldest person. Otherwise, the premium calculation will be processed based on the eldest persons age',
                        showConfirmButton: true
                        }).then((result) => {
                            if(result) {
                                $.ajax({
                                    url: ajax_url, // or example_ajax_obj.ajaxurl if using on frontend
                                    data: {
                                        'action': 'generate_qoute',
                                        'dob_1' : dob,
                                        'pre_exist_1' : pre_exist_1,
                                        'dob_2' : dob_app_2,
                                        'pre_exist_2' : pre_exist_2,
                                        'departure_date' : departure_date,
                                        'relationship' : relationship,
                                        'dob_child_1' : dob_child_1,
                                        'pre_exist_child1' : pre_exist_child1,
                                        'dob_child_2' : dob_child_2,
                                        'pre_exist_child2' : pre_exist_child2,
                                        'return_date' : return_date,
                                        'trip_type' : trip_type,
                                        'destination' : destination,
                                        'first_name' : first_name,
                                        'last_name' : last_name,
                                        'email' : email,
                                        'phone' : phone,
                                        'dob' : dob
                                    },
                                    type: 'POST',
                                    crossDomain: true,
                                    success:function(data) {
                                        $('.form-loader').show();
                                        setTimeout(function(){
                                            $('.form-loader').hide();
                                            window.location.replace(webUrl+"/generate-quote?dob1="+dob+'&ext1='+pre_exist_1+'&dob2='+dob_app_2+'&ext2='+pre_exist_2+'&relationship='+relationship+'&dob_child_1='+dob_child_1+'&pre_exist_child1='+pre_exist_child1+'&dob_child_2='+dob_child_2+'&=pre_exist_child2'+pre_exist_child2+'&sum='+sum_assured+'&dep='+departure_date+'&arv='+return_date+'&method=travel_insurance&tr_ty='+trip_type+'&des='+destination+'&qnos='+data);
                                        },500);
                                    },
                                    error: function(errorThrown){
                                        console.log(errorThrown);
                                    }
                                });
                            }
                        });

                        return false;

                }
            }


        }else{

            $.ajax({
                url: ajax_url, // or example_ajax_obj.ajaxurl if using on frontend
                data: {
                    'action': 'generate_qoute',
                    'dob_1' : dob,
                    'pre_exist_1' : pre_exist_1,
                    'dob_2' : dob_app_2,
                    'pre_exist_2' : pre_exist_2,
                    'departure_date' : departure_date,
                    'relationship' : relationship,
                    'dob_child_1' : dob_child_1,
                    'pre_exist_child1' : pre_exist_child1,
                    'dob_child_2' : dob_child_2,
                    'pre_exist_child2' : pre_exist_child2,
                    'return_date' : return_date,
                    'trip_type' : trip_type,
                    'destination' : destination,
                    'first_name' : first_name,
                    'last_name' : last_name,
                    'email' : email,
                    'phone' : phone,
                    'dob' : dob
                },
                type: 'POST',
                crossDomain: true,
                success:function(data) {
                    $('.form-loader').show();
                    setTimeout(function(){
                        $('.form-loader').hide();
                        window.location.replace(webUrl+"/generate-quote?dob1="+dob+'&ext1='+pre_exist_1+'&dob2='+dob_app_2+'&ext2='+pre_exist_2+'&relationship='+relationship+'&dob_child_1='+dob_child_1+'&pre_exist_child1='+pre_exist_child1+'&dob_child_2='+dob_child_2+'&=pre_exist_child2'+pre_exist_child2+'&sum='+sum_assured+'&dep='+departure_date+'&arv='+return_date+'&method=travel_insurance&tr_ty='+trip_type+'&des='+destination+'&qnos='+data);
                    },500);
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            });

        }



    });

    /**
     *
     * Questionnaire modal rendering function
     */

    var parent_amounts = [];
    localStorage.removeItem('parentAmt');
    $('.add-questionnaire').click(function (e) {
        $('.add-questionnaire').removeClass('current');
        var currentBtnElem = $(e.currentTarget);
        currentBtnElem.addClass('current');

        var parent_amounts = [];
        $(e.target).parent('div').find('.form-loader-1').show();
        var ajax_url = $('.admin-ajax').val();
        var product_id = $(e.target).attr('data-pid');
        var trip_type = $('.trip_type').val();
        var dob_2 = $('.dob_2').val();

        var parentEl = $(e.target).closest(".panel");
        var amountElems = parentEl.find("span.quote-rate");
        amountElems.each(function(i, e) {
            parent_amounts.push({'pid' : product_id, 'amt' : $(e).data("amount"), 'amount_id' : $(e).data("amtid"), 'classification' : $(e).data("classification")});
        });
        localStorage.setItem('parentAmt', JSON.stringify(parent_amounts));
        // ajax call
        $.ajax({
            url: ajax_url,
            data: {
                'action': 'get_questionnaire',
                'product_id' : product_id,
                'trip_type' : trip_type,
                'dob_2' : dob_2
            },
            type: 'POST',
            success:function(data) {
                setTimeout(function(){
                    $('.form-loader-1').hide();
                    $('.updated-contents').html(data);
                    $('#gridSystemModal').modal('show');
                },750);
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        });
    });

    // Travelence Questionnaire

    $('#gridSystemModal').on('shown.bs.modal', function () {
        var product_id = $('.productid').val();
        if(product_id == 46) {
            var questElems = $(".travelence-46 tbody tr");
            var noFlag = true;
            var allPriSelected = false;
            var hospitalNoFlag = false;

            $.each(questElems, function(index, value){
                if(index < 8) {
                    $(value).addClass('pri-questions');
                }
                if(index == 8) {
                    $(value).addClass('hide one-yes-question');
                } else if(index > 8) {
                    $(value).addClass('hide sec-questions');
                }
            });

            var questElemsPri = $(".travelence-46 tr.pri-questions");
            var questElemsSec = $(".travelence-46 tr.sec-questions");

            $(".travelence-46 tr input[type='radio']").change(function(e){
                var priYesCount = 0;
                if(isMultiple) {
                    var priYesCount1 = 0;
                    var priYesCount2 = 0;
                    var secYesFlag = false;
                    var hospitalFlag = false;
                    $.each(questElemsPri, function(index, value){
                        var currVal1 = $(value).find("input.pre-existing-1[type='radio']:checked").val();
                        if(currVal1 === 'yes') {
                            noFlag = false;
                            priYesCount1++;
                        }
                    });
                    $.each(questElemsPri, function(index, value){
                        var currVal2 = $(value).find("input.pre-existing-2[type='radio']:checked").val();
                        if(currVal2 === 'yes') {
                            noFlag = false;
                            priYesCount2++;
                        }
                    });
                    $.each(questElemsSec, function(index, value){
                        var currVal1 = $(value).find("input.pre-existing-1[type='radio']:checked").val();
                        var currVal2 = $(value).find("input.pre-existing-2[type='radio']:checked").val();
                        if(currVal1 === 'yes' || currVal2 === 'yes') {
                            secYesFlag = true;
                        }
                    });
                    if($('.travelence-46 tr.one-yes-question input.pre-existing-1[type="radio"]:checked').val() === 'yes' || $('.travelence-46 tr.one-yes-question input.pre-existing-2[type="radio"]:checked').val() === 'yes') {
                        hospitalFlag = true;
                    }
                    if(!$('.travelence-46 tr.pri-questions:not(:has(input.pre-existing-1:radio:checked))').length && !$('.travelence-46 tr.pri-questions:not(:has(input.pre-existing-2:radio:checked))').length) {
                        allPriSelected = true;
                    }

                    if(!$('.travelence-46 tr.one-yes-question:not(:has(input.pre-existing-1:radio:checked))').length && !$('.travelence-46 tr.one-yes-question:not(:has(input.pre-existing-2:radio:checked))').length) {
                        if(!hospitalFlag){
                            hospitalNoFlag = true;
                        }
                    }

                    var priYesCount = priYesCount1 > priYesCount2 ? priYesCount1 : priYesCount2;
                } else {

                // console.log('priYesCount', priYesCount);
                    var secYesFlag = false;
                    var hospitalFlag = false;
                    $.each(questElemsPri, function(index, value){
                        var currVal = $(value).find("input[type='radio']:checked").val();
                        if(currVal === 'yes') {
                            noFlag = false;
                            priYesCount++;
                        }
                    });

                    $.each(questElemsSec, function(index, value){
                        var currVal = $(value).find("input[type='radio']:checked").val();
                        if(currVal === 'yes') {
                            secYesFlag = true;
                        }
                    });

                    if($('.travelence-46 tr.one-yes-question input[type="radio"]:checked').val() === 'yes') {
                        hospitalFlag = true;
                    }

                    if(!$('.travelence-46 tr.pri-questions:not(:has(:radio:checked))').length) {
                        allPriSelected = true;
                    }

                    if($('.travelence-46 tr.one-yes-question input[type="radio"]:checked').val() === 'no') {
                        hospitalNoFlag = true;
                    }
                }
                if(priYesCount >= 2 || secYesFlag || hospitalFlag) {
                    //Display error message
                    $(".apply-addon").addClass('disabled');
                    $('.error-message-cant-buy').css("text-align", "left");
                    $('.error-message-cant-buy').show();
                    setTimeout(function(){
                        $('.error-message-cant-buy').hide();
                    },3000);
                } else {
                    $(".apply-addon").removeClass('disabled');
                }

                if(hospitalFlag){
                    $('.travelence-46 tr.sec-questions').addClass('hide');
                }

                if(priYesCount >= 2) {
                    $('.travelence-46 tr.sec-questions').addClass('hide');
                    $('.travelence-46 tr.one-yes-question').addClass('hide');
                }

                if (allPriSelected) {
                    if(priYesCount === 0) {
                        if($('.travelence-46 tr.sec-questions').hasClass('hide')){
                            $('.travelence-46 tr.sec-questions').removeClass('hide');
                            $('.travelence-46 tr.sec-questions input[type="radio"]').prop('checked', false);
                        }
                        $('.travelence-46 tr.one-yes-question').addClass('hide');
                    } else if(priYesCount === 1) {
                        if($('.travelence-46 tr.one-yes-question').hasClass('hide')){
                            $('.travelence-46 tr.one-yes-question').removeClass('hide');
                            $('.travelence-46 tr.one-yes-question input[type="radio"]').prop('checked', false);
                        }
                        if(hospitalNoFlag) {
                            if($('.travelence-46 tr.sec-questions').hasClass('hide')) {
                                $('.travelence-46 tr.sec-questions').removeClass('hide');
                                $('.travelence-46 tr.sec-questions input[type="radio"]').prop('checked', false);
                            }
                        }
                    }
                }
            });
        } else if(product_id == 92) {
            var questElems = $(".travelence-92 tbody tr");
            var allPriSelected = false;
            var allSecSelected = false;

            $.each(questElems, function(index, value){
                if(index <= 7) {
                    $(value).addClass('pri-questions');
                } else if(index <= 12) {
                    $(value).addClass('hide sec-questions');
                } else {
                    $(value).addClass('hide ter-questions');
                }
            });

            var questElemsPri = $(".travelence-92 tr.pri-questions");
            var questElemsSec = $(".travelence-92 tr.sec-questions");
            var questElemsTer = $(".travelence-92 tr.ter-questions");

            $(".travelence-92 tr input[type='radio']").change(function(e){
                var priYesCount = 0;
                var secYesCount = 0;
                var terYesCount = 0;
                // var secYesFlag = false;
                $.each(questElemsPri, function(index, value){
                    var currVal = $(value).find("input[type='radio']:checked").val();
                    if(currVal === 'yes') {
                        noFlag = false;
                        priYesCount++;
                    }
                });

                $.each(questElemsSec, function(index, value){
                    var currVal = $(value).find("input[type='radio']:checked").val();
                    if(currVal === 'yes') {
                        secYesCount++;
                    }
                });

                $.each(questElemsTer, function(index, value){
                    var currVal = $(value).find("input[type='radio']:checked").val();
                    if(currVal === 'yes') {
                        terYesCount++;
                    }
                });

                if(!$('.travelence-92 tr.pri-questions:not(:has(:radio:checked))').length) {
                    allPriSelected = true;
                }

                if(!$('.travelence-92 tr.sec-questions:not(:has(:radio:checked))').length) {
                    allSecSelected = true;
                }

                if(priYesCount >= 1 || secYesCount >= 1 || terYesCount >= 1) {
                    //Display error message
                    $(".apply-addon").addClass('disabled');
                    $('.error-message-cant-buy').css("text-align", "left");
                    $('.error-message-cant-buy').show();
                    setTimeout(function(){
                        $('.error-message-cant-buy').hide();
                    },3000);
                } else {
                    $(".apply-addon").removeClass('disabled');
                }

                if(priYesCount >= 1) {
                    $('.travelence-46 tr.sec-questions').addClass('hide');
                    $('.travelence-46 tr.ter-questions').addClass('hide');
                }
                console.log('allPriSelected', allPriSelected);
                console.log('priYesCount', priYesCount);
                if (allPriSelected) {
                    if(priYesCount === 0) {
                        if($('.travelence-92 tr.sec-questions').hasClass('hide')){
                            $('.travelence-92 tr.sec-questions').removeClass('hide');
                            $('.travelence-92 tr.sec-questions input[type="radio"]').prop('checked', false);
                        }
                    } else {
                        $('.travelence-92 tr.sec-questions').addClass('hide');
                        $('.travelence-92 tr.sec-questions input[type="radio"]').prop('checked', false);
                    }
                }
                if(allSecSelected) {
                    if(secYesCount === 0) {
                        if($('.travelence-92 tr.ter-questions').hasClass('hide')){
                            $('.travelence-92 tr.ter-questions').removeClass('hide');
                            $('.travelence-92 tr.ter-questions input[type="radio"]').prop('checked', false);
                        }
                    } else {
                        $('.travelence-92 tr.ter-questions').addClass('hide');
                        $('.travelence-92 tr.ter-questions input[type="radio"]').prop('checked', false);
                    }
                }
            });
        } else if(product_id == 96) {
            if(isMultiple) {
                $(".tugo-questions .tugo-quest-row:eq(0) input[type='radio']").change(function(e){
                    if(($("input[name='medication1']:checked").val() == "1" || $("input[name='medication1']:checked").val() == "0") && ($("input[name='medication2']:checked").val() == "1" || $("input[name='medication2']:checked").val() == "0")) {
                        $(".tugo-questions .tugo-quest-row:eq(1)").removeClass('hide');
                        $(".tugo-questions .alert").addClass('hide');
                    } else {
                        $(".tugo-questions .alert").addClass('hide');
                        $(".tugo-questions .tugo-quest-row").addClass('hide');
                        $(".tugo-questions .tugo-quest-row:eq(0)").removeClass('hide');
                        $(".tugo-questions .tugo-quest-row.hide input[type='radio']").prop('checked', false);
                    }
                    if($("input[name='medication1']:checked").val() == "3" || $("input[name='medication2']:checked").val() == "3") {
                        $(".tugo-questions .alert .category-type").text('6');
                        $(".tugo-questions .alert").removeClass('hide');

                    } else if($("input[name='medication1']:checked").val() == "2" || $("input[name='medication2']:checked").val() == "2") {
                        $(".tugo-questions .alert .category-type").text('5');
                        $(".tugo-questions .alert").removeClass('hide');

                    }
                });

                $(".tugo-questions .tugo-quest-row:eq(1) input[type='radio']").change(function(e){
                    if($("input[name='heart-attack1']:checked").val() == "yes" || $("input[name='heart-attack2']:checked").val() == "yes") {
                        $(".tugo-questions .tugo-quest-row").addClass('hide');
                        $(".tugo-questions .tugo-quest-row:eq(0)").removeClass('hide');
                        $(".tugo-questions .tugo-quest-row:eq(1)").removeClass('hide');
                        $(".tugo-questions .tugo-quest-row.hide input[type='radio']").prop('checked', false);
                        $(".tugo-questions .alert .category-type").text('5');
                        $(".tugo-questions .alert").removeClass('hide');
                    } else if ($("input[name='heart-attack1']:checked").val() == "no" && $("input[name='heart-attack2']:checked").val() == "no") {
                        $(".tugo-questions .tugo-quest-row:eq(2)").removeClass('hide');
                        $(".tugo-questions .alert").addClass('hide');
                    }
                });

                $(".tugo-questions .tugo-quest-row:eq(2) input[type='radio']").change(function(e){
                    if($("input[name='medical-condition1']:checked").val() == "0" && $("input[name='medical-condition2']:checked").val() == "0") {
                        $(".tugo-questions .tugo-quest-row:eq(3)").removeClass('hide');
                        $(".tugo-questions .alert").addClass('hide');
                    } else {
                        $(".tugo-questions .tugo-quest-row").addClass('hide');
                        $(".tugo-questions .tugo-quest-row:eq(0)").removeClass('hide');
                        $(".tugo-questions .tugo-quest-row:eq(1)").removeClass('hide');
                        $(".tugo-questions .tugo-quest-row:eq(2)").removeClass('hide');
                        $(".tugo-questions .tugo-quest-row.hide input[type='radio']").prop('checked', false);
                        $(".tugo-questions .alert").addClass('hide');
                    }
                    if($("input[name='medical-condition1']:checked").val() == "2" || $("input[name='medical-condition2']:checked").val() == "2") {
                        $(".tugo-questions .alert .category-type").text('4');
                        $(".tugo-questions .alert").removeClass('hide');
                    } else if($("input[name='medical-condition1']:checked").val() == "1" || $("input[name='medical-condition2']:checked").val() == "1") {
                        $(".tugo-questions .alert .category-type").text('3');
                        $(".tugo-questions .alert").removeClass('hide');
                    }
                });

                $(".tugo-questions .tugo-quest-row:eq(3) input[type='radio']").change(function(e){
                    if($("input[name='other-medical-condition1']:checked").val() == "yes" || $("input[name='other-medical-condition2']:checked").val() == "yes") {
                        $(".tugo-questions .tugo-quest-row:eq(4)").addClass('hide');
                        $(".tugo-questions .alert .category-type").text('2');
                        $(".tugo-questions .alert").removeClass('hide');
                    } else if($("input[name='other-medical-condition1']:checked").val() == "no" && $("input[name='other-medical-condition2']:checked").val() == "no") {
                        $(".tugo-questions .tugo-quest-row:eq(4)").removeClass('hide');
                        $(".tugo-questions .alert").addClass('hide');
                    }
                });

                $(".tugo-questions .tugo-quest-row:eq(4) input[type='radio']").change(function(e){
                    if($("input[name='tobacco1']:checked").val() == "yes" || $("input[name='tobacco2']:checked").val() == "yes") {
                        $(".tugo-questions .alert .category-type").text('2');
                        $(".tugo-questions .alert").removeClass('hide');
                    } else if($("input[name='tobacco1']:checked").val() == "no" && $("input[name='tobacco2']:checked").val() == "no") {
                        $(".tugo-questions .alert .category-type").text('1');
                        $(".tugo-questions .alert").removeClass('hide');
                    }
                });
            } else {
                var questElems = $(".tugo-questions .tugo-quest-row");
                $(".tugo-questions .tugo-quest-row:eq(0) input[type='radio']").change(function(e){
                    if(e.currentTarget.value == "1" || e.currentTarget.value == 0) {
                        $(".tugo-questions .tugo-quest-row:eq(1)").removeClass('hide');
                        $(".tugo-questions .alert").addClass('hide');
                    } else {
                        $(".tugo-questions .alert").addClass('hide');
                        $(".tugo-questions .tugo-quest-row").addClass('hide');
                        $(".tugo-questions .tugo-quest-row:eq(0)").removeClass('hide');
                        $(".tugo-questions .tugo-quest-row.hide input[type='radio']").prop('checked', false);
                    }
                    if(e.currentTarget.value == "3") {
                        $(".tugo-questions .alert .category-type").text('6');
                        $(".tugo-questions .alert").removeClass('hide');
                    } else if(e.currentTarget.value == "2") {
                        $(".tugo-questions .alert .category-type").text('5');
                        $(".tugo-questions .alert").removeClass('hide');
                    }
                });

                $(".tugo-questions .tugo-quest-row:eq(1) input[type='radio']").change(function(e){
                    if(e.currentTarget.value == "no") {
                        $(".tugo-questions .tugo-quest-row:eq(2)").removeClass('hide');
                        $(".tugo-questions .alert").addClass('hide');
                    } else {
                        $(".tugo-questions .tugo-quest-row").addClass('hide');
                        $(".tugo-questions .tugo-quest-row:eq(0)").removeClass('hide');
                        $(".tugo-questions .tugo-quest-row:eq(1)").removeClass('hide');
                        $(".tugo-questions .tugo-quest-row.hide input[type='radio']").prop('checked', false);
                        $(".tugo-questions .alert .category-type").text('5');
                        $(".tugo-questions .alert").removeClass('hide');
                    }
                });

                $(".tugo-questions .tugo-quest-row:eq(2) input[type='radio']").change(function(e){
                    if(e.currentTarget.value == "0") {
                        $(".tugo-questions .tugo-quest-row:eq(3)").removeClass('hide');
                        $(".tugo-questions .alert").addClass('hide');
                    } else {
                        $(".tugo-questions .tugo-quest-row").addClass('hide');
                        $(".tugo-questions .tugo-quest-row:eq(0)").removeClass('hide');
                        $(".tugo-questions .tugo-quest-row:eq(1)").removeClass('hide');
                        $(".tugo-questions .tugo-quest-row:eq(2)").removeClass('hide');
                        $(".tugo-questions .tugo-quest-row.hide input[type='radio']").prop('checked', false);
                        $(".tugo-questions .alert").addClass('hide');
                    }
                    if(e.currentTarget.value == "2") {
                        $(".tugo-questions .alert .category-type").text('4');
                        $(".tugo-questions .alert").removeClass('hide');
                    } else if(e.currentTarget.value == "1") {
                        $(".tugo-questions .alert .category-type").text('3');
                        $(".tugo-questions .alert").removeClass('hide');
                    }
                });

                $(".tugo-questions .tugo-quest-row:eq(3) input[type='radio']").change(function(e){
                    if(e.currentTarget.value == "no") {
                        $(".tugo-questions .tugo-quest-row:eq(4)").removeClass('hide');
                        $(".tugo-questions .alert").addClass('hide');
                    } else {
                        $(".tugo-questions .tugo-quest-row:eq(4)").addClass('hide');
                        $(".tugo-questions .alert .category-type").text('2');
                        $(".tugo-questions .alert").removeClass('hide');
                    }
                });
                $(".tugo-questions .tugo-quest-row:eq(4) input[type='radio']").change(function(e){
                    if(e.currentTarget.value == "no") {
                        $(".tugo-questions .alert .category-type").text('1');
                        $(".tugo-questions .alert").removeClass('hide');
                    } else {
                        $(".tugo-questions .alert .category-type").text('2');
                        $(".tugo-questions .alert").removeClass('hide');
                    }
                });
            }

        }
    });

    /**
     * Update generate quote
     */
    $('.update_trip').click(function (e) {
        var url = new URL(window.location.href);
        var search_params = url.searchParams;
        var updated_dep_date = $('.updated-dep-date').val();
        var updated_ret_date = $('.updated-ret-date').val();
        var trip_type = $('.updated-trip-type').val();
        var sum_assured = $('.updated-sum-assured').val();
        var ajax_url = $('.admin-ajax').val();

        if(updated_dep_date == ''){$('.error-message-field-up-depdate').show();return false;}else{$('.error-message-field-up-depdate').hide();}
        if(updated_ret_date == ''){$('.error-message-field-up-retdate').show();return false;}else{$('.error-message-field-up-retdate').hide();}
        search_params.set('dep', updated_dep_date);
        search_params.set('arv', updated_ret_date);
        search_params.set('tr_ty', trip_type);
        if(sum_assured != ''){
            search_params.set('sum', sum_assured);
        }
        url.search = search_params.toString();
        var new_url = url.toString();

         // ajax call
         $.ajax({
            url: ajax_url,
            data: {
                'action': 'update_redirect'
            },
            type: 'POST',
            success:function(data) {
                setTimeout(function(){
                    window.location.replace(new_url);
                },500);
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        });
    });


    /**
     *
     * Questionnaire apply action
     */
    var dataarray = [];
    var strarray = [];
    var amount_output = [];
    var isMultiple = false;

    if($('.dob2').val() !== "undefined"){
        isMultiple = true;
    }


    $('.apply-addon').click(function (e) {
        var prod_id = $('.productid').val();
        var quote_id = $('.quote_id').val();
        var trip_type = $('.trip_type').val();
        var dob2 = $('.dob2').val(); if(dob2 == 'undefined'){isMultiple = false;}else{isMultiple = true;}
        var total_points = 0, total_points_1 = 0, total_points_2 = 0;

        var travel_method = $('.travel-method').val();
        var output = [];
        var outputamounts = [];
        var classification = '';

        if($('.questionnaire-validation table.table_radio tbody tr:visible:not(:has(:radio:checked))').length || $('.questionnaire-validation .tugo-questions .tugo-quest-options:visible:not(:has(:radio:checked))').length) {
            $('.error-message-quest-validation').css("text-align", "left");
            $('.error-message-quest-validation').show();
            setTimeout(function(){
                $('.error-message-quest-validation').hide();
            },3000);
        }
        else {
            var parent_amt = localStorage.getItem("parentAmt");
            var all_amounts = JSON.parse(parent_amt);
            //console.log(all_amounts);

            if(isMultiple) {
                var valArrayOne = $.map($(".table_radio input:radio:checked[name^='1_']"), function(elem, idx) {
                    if(travel_method == 'travel'){
                        if($(elem).val() == 'yes'){
                            total_points_1 += $(elem).data("vals");
                        }
                    }
                    return $(elem).attr("name")+"="+ $(elem).val();
                });
                var applicantOneData = [];
                $.each(valArrayOne, function(index, value){
                    applicantOneData = applicantOneData + "&" + value;
                });

                var valArrayTwo = $.map($(".table_radio input:radio:checked[name^='2_']"), function(elem, idx) {
                    if(travel_method == 'travel'){
                        if($(elem).val() == 'yes'){
                            total_points_2 += $(elem).data("vals");
                        }
                    }
                    return $(elem).attr("name")+"="+ $(elem).val();
                });
                var applicantTwoData = [];
                $.each(valArrayTwo, function(index, value){
                    applicantTwoData = applicantTwoData + "&" + value;
                });

                dataarray.push({'pid' : prod_id, 'data1' : applicantOneData, 'data2' : applicantTwoData});
                localStorage.setItem('questdata', JSON.stringify(dataarray));
                // console.log('questdata', dataarray);

            }
            else {
                var valArray = $.map($("input:radio:checked"), function(elem, idx) {
                    if(travel_method == 'travel'){
                        if($(elem).val() == 'yes'){
                            total_points += $(elem).data("vals");
                        }
                    }else{

                    }
                    return $(elem).attr("name")+"="+ $(elem).val();
                });
                var data = [];
                $.each(valArray, function(index, value){
                    data = data + "&" + value;
                });

                dataarray.push({'pid' : prod_id, 'data' : data});
            }

            if(prod_id == 59){

                if(isMultiple) {
                    var consolidated_valueOne = 0;
                    var consolidated_valueTwo = 0;
                    $.each(valArrayOne, function(index, value){
                        var fields = value.split('=');
                        // processed_data.push({'item' : fields[0], 'value' : fields[1]});
                        switch (fields[0]) {
                            case '1_heart_condition':
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 6;
                            }
                            break;
                            case "1_lung_condition_excluding_asthma":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 6;
                            }
                            break;
                            case "1_diabetes_excluding_diet_controlled":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 6;
                            }
                            break;
                            case "1_stroke_cva_or_transient_ischemic_attack_tia":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 6;
                            }
                            break;
                            case "1_bowel__stomach_disorder_excluding_diverticulitis":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 6;
                            }
                            break;
                            case "1_cancer_excluding_basal_or_squamous_cell_skin_cancer_and_breast_cancer_treated_only_with_hormone_therapy":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 6;
                            }
                            break;
                            case "1_artery_or_vein_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 5;
                            }
                            break;
                            case "1_diverticulitis":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 4;
                            }
                            break;
                            case "1_neurological_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 3;
                            }
                            break;
                            case "1_pancreas_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 3;
                            }
                            break;
                            case "1_liver_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 3;
                            }
                            break;
                            case "1_kidney_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 3;
                            }
                            break;
                            case "1_high_blood_pressure_-_treated_at_any_one_time_with_3_or_more_medications":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 6;
                            }
                            break;
                            case "1_high_blood_pressure_-_treated_at_any_one_time_with_1_or_2_medications_only":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 1;
                            }
                            break;
                            case "1_asthma":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 2;
                            }
                            break;
                            case "1_diet_controlled_diabetes":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 1;
                            }
                            break;
                            case "1_prior_to_your_application_date_has_it_been_more_than_24_months_since_you_have_undergone_a_medical_check-up":
                            if(fields[1] === 'yes') {
                                consolidated_valueOne += 2;
                            }
                            break;
                        }

                    });
                    $.each(valArrayTwo, function(index, value){
                        var fields = value.split('=');
                        // processed_data.push({'item' : fields[0], 'value' : fields[1]});
                        switch (fields[0]) {
                            case '2_heart_condition':
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 6;
                            }
                            break;
                            case "2_lung_condition_excluding_asthma":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 6;
                            }
                            break;
                            case "2_diabetes_excluding_diet_controlled":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 6;
                            }
                            break;
                            case "2_stroke_cva_or_transient_ischemic_attack_tia":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 6;
                            }
                            break;
                            case "2_bowel__stomach_disorder_excluding_diverticulitis":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 6;
                            }
                            break;
                            case "2_cancer_excluding_basal_or_squamous_cell_skin_cancer_and_breast_cancer_treated_only_with_hormone_therapy":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 6;
                            }
                            break;
                            case "2_artery_or_vein_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 5;
                            }
                            break;
                            case "2_diverticulitis":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 4;
                            }
                            break;
                            case "2_neurological_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 3;
                            }
                            break;
                            case "2_pancreas_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 3;
                            }
                            break;
                            case "2_liver_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 3;
                            }
                            break;
                            case "2_kidney_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 3;
                            }
                            break;
                            case "2_high_blood_pressure_-_treated_at_any_one_time_with_3_or_more_medications":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 6;
                            }
                            break;
                            case "2_high_blood_pressure_-_treated_at_any_one_time_with_1_or_2_medications_only":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 1;
                            }
                            break;
                            case "2_asthma":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 2;
                            }
                            break;
                            case "2_diet_controlled_diabetes":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 1;
                            }
                            break;
                            case "2_prior_to_your_application_date_has_it_been_more_than_24_months_since_you_have_undergone_a_medical_check-up":
                            if(fields[1] === 'yes') {
                                consolidated_valueTwo += 2;
                            }
                            break;
                        }
                    });
                    // console.log(consolidated_valueOne);
                    // console.log(consolidated_valueTwo);
                    var setEligibilityFlag = '';
                    if(consolidated_valueOne > 17 || consolidated_valueTwo > 17){
                        setEligibilityFlag = 'failed';
                        $('.error-message-no-eligibility').show();
                        $(".error-message-no-eligibility").css("text-align","left");
                        return false;
                    }else{
                        var setEligibilityFlag = 'passed';
                        $('.error-message-no-eligibility').hide();
                    }
                }else{
                    var processed_data = [];
                    var consolidated_value = 0;
                    $.each(valArray, function(index, value){
                        var fields = value.split('=');
                        processed_data.push({'item' : fields[0], 'value' : fields[1]});
                        switch (fields[0]) {
                            case 'heart_condition':
                            if(fields[1] === 'yes') {
                                consolidated_value += 6;
                            }
                            break;
                            case "lung_condition_excluding_asthma":
                            if(fields[1] === 'yes') {
                                consolidated_value += 6;
                            }
                            break;
                            case "diabetes_excluding_diet_controlled":
                            if(fields[1] === 'yes') {
                                consolidated_value += 6;
                            }
                            break;
                            case "stroke_cva_or_transient_ischemic_attack_tia":
                            if(fields[1] === 'yes') {
                                consolidated_value += 6;
                            }
                            break;
                            case "bowel__stomach_disorder_excluding_diverticulitis":
                            if(fields[1] === 'yes') {
                                consolidated_value += 6;
                            }
                            break;
                            case "cancer_excluding_basal_or_squamous_cell_skin_cancer_and_breast_cancer_treated_only_with_hormone_therapy":
                            if(fields[1] === 'yes') {
                                consolidated_value += 6;
                            }
                            break;
                            case "artery_or_vein_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_value += 5;
                            }
                            break;
                            case "diverticulitis":
                            if(fields[1] === 'yes') {
                                consolidated_value += 4;
                            }
                            break;
                            case "neurological_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_value += 3;
                            }
                            break;
                            case "pancreas_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_value += 3;
                            }
                            break;
                            case "liver_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_value += 3;
                            }
                            break;
                            case "kidney_disorder":
                            if(fields[1] === 'yes') {
                                consolidated_value += 3;
                            }
                            break;
                            case "high_blood_pressure_-_treated_at_any_one_time_with_3_or_more_medications":
                            if(fields[1] === 'yes') {
                                consolidated_value += 6;
                            }
                            break;
                            case "high_blood_pressure_-_treated_at_any_one_time_with_1_or_2_medications_only":
                            if(fields[1] === 'yes') {
                                consolidated_value += 1;
                            }
                            break;
                            case "asthma":
                            if(fields[1] === 'yes') {
                                consolidated_value += 2;
                            }
                            break;
                            case "diet_controlled_diabetes":
                            if(fields[1] === 'yes') {
                                consolidated_value += 1;
                            }
                            break;
                            case "prior_to_your_application_date_has_it_been_more_than_24_months_since_you_have_undergone_a_medical_check-up":
                            if(fields[1] === 'yes') {
                                consolidated_value += 2;
                            }
                            break;
                        }

                    });
                    var setEligibilityFlag = '';
                    if(consolidated_value > 17){
                        setEligibilityFlag = 'failed';
                        $('.error-message-no-eligibility').show();
                        $(".error-message-no-eligibility").css("text-align","left");
                        return false;
                    }else{
                        var setEligibilityFlag = 'passed';
                        $('.error-message-no-eligibility').hide();
                    }
                //console.log(dataarray);
                }
            }
            localStorage.setItem('questdata', JSON.stringify(dataarray));

            //return false;

            //console.log(total_points_1);
            //console.log(total_points_2);

            if(prod_id == 59){
                if(setEligibilityFlag == 'passed'){
                    if(isMultiple) {
                        if((consolidated_valueOne == 0 || consolidated_valueOne == 1) || (consolidated_valueTwo == 0 || consolidated_valueTwo == 1)){
                            classification = ['premier single', 'premier annual multiple'];
                            $.each(all_amounts, function(index, value){
                                if(value.classification == classification[0] || value.classification == classification[1]){
                                    output.push(value.amt);
                                    outputamounts.push({'rate_id' : value.amount_id, 'amount' : value.amt});
                                }
                            });
                        }
                        if((consolidated_valueOne >= 2 && consolidated_valueOne <= 5) || consolidated_valueTwo >= 2 && consolidated_valueTwo <= 5){
                            classification = ['ultra single', 'ultra annual multiple'];
                            $.each(all_amounts, function(index, value){
                                if(value.classification == classification[0] || value.classification == classification[1]){
                                    output.push(value.amt);
                                    outputamounts.push({'rate_id' : value.amount_id, 'amount' : value.amt});
                                }
                            });
                        }
                        if((consolidated_valueOne >= 6 && consolidated_valueOne <= 11) || consolidated_valueTwo >= 6 && consolidated_valueTwo <= 11){
                            classification = ['super single', 'super annual multiple'];
                            $.each(all_amounts, function(index, value){
                                if(value.classification == classification[0] || value.classification == classification[1]){
                                    output.push(value.amt);
                                    outputamounts.push({'rate_id' : value.amount_id, 'amount' : value.amt});
                                }
                            });
                        }
                        if((consolidated_valueOne >= 12 && consolidated_valueOne <= 16) || (consolidated_valueTwo >= 12 && consolidated_valueTwo <= 16)){
                            classification = ['elite single multiple', 'elite annual multiple'];
                            $.each(all_amounts, function(index, value){
                                if(value.classification == classification[0] || value.classification == classification[1]){
                                    output.push(value.amt);
                                    outputamounts.push({'rate_id' : value.amount_id, 'amount' : value.amt});
                                }
                            });
                        }

                    }else{
                        if(consolidated_value == 0 || consolidated_value == 1){
                            classification = ['premier single', 'premier annual multiple'];
                            $.each(all_amounts, function(index, value){
                                if(value.classification == classification[0] || value.classification == classification[1]){
                                    output.push(value.amt);
                                    outputamounts.push({'rate_id' : value.amount_id, 'amount' : value.amt});
                                }
                            });
                        }else if(consolidated_value >= 2 && consolidated_value <= 5){
                            classification = ['ultra single', 'ultra annual multiple'];
                            $.each(all_amounts, function(index, value){
                                if(value.classification == classification[0] || value.classification == classification[1]){
                                    output.push(value.amt);
                                    outputamounts.push({'rate_id' : value.amount_id, 'amount' : value.amt});
                                }
                            });
                        }else if(consolidated_value >= 6 && consolidated_value <= 11){
                            classification = ['super single', 'super annual multiple'];
                            $.each(all_amounts, function(index, value){
                                if(value.classification == classification[0] || value.classification == classification[1]){
                                    output.push(value.amt);
                                    outputamounts.push({'rate_id' : value.amount_id, 'amount' : value.amt});
                                }
                            });
                        }else if(consolidated_value >= 12 && consolidated_value <= 16){
                            classification = ['elite single', 'elite annual multiple'];
                            $.each(all_amounts, function(index, value){
                                if(value.classification == classification[0] || value.classification == classification[1]){
                                    output.push(value.amt);
                                    outputamounts.push({'rate_id' : value.amount_id, 'amount' : value.amt});
                                }
                            });
                        }

                    }
                    strarray.push({'qid' : quote_id, 'prid' : prod_id, 'data' : output});
                }
            }
            else if(prod_id == 46){
                var selectedInclusive = $('#inclusive-amounts option:selected').attr('data-amount');
                var selectedAmountId = $('#inclusive-amounts option:selected').attr('data-amtid');
                outputamounts.push({'rate_id' : selectedAmountId, 'amount' : selectedInclusive});

                var emergencyMedicalElems = $('.add-questionnaire.current').closest('.product-item').find("span.quote-rate");
                $.each(emergencyMedicalElems, function(index, value){
                    outputamounts.push({'rate_id' : $(value).attr('data-amtid'), 'amount' : $(value).attr('data-amount')});
                });

                var selectedTripCancellation = $('.trip-cancellation-list option:selected').attr('data-amount');
                var selectedTripId = $('.trip-cancellation-list option:selected').attr('data-amtid');
                if($('.trip-cancellation-list').val() !== '-1') {
                    outputamounts.push({'rate_id' : selectedTripId, 'amount' : selectedTripCancellation});
                }

            }
            else if(prod_id == 92){
                $.each(all_amounts, function(index, value){
                    if(value.classification == 'single'){
                        output.push(value.amt);
                        outputamounts.push({'rate_id' : value.amount_id, 'amount' : value.amt});
                    }
                });
                strarray.push({'qid' : quote_id, 'prid' : prod_id, 'data' : output});

            }
            else if(prod_id == 96){
                var selectedInclusive = $('#tugo-inclusive-amounts option:selected').attr('data-amount');
                var selectedAmountId = $('#tugo-inclusive-amounts option:selected').attr('data-amtid');
                outputamounts.push({'rate_id' : selectedAmountId, 'amount' : selectedInclusive});

                var emergencyMedicalElems = $('.add-questionnaire.current').closest('.product-item').find("span.quote-rate");
                $.each(emergencyMedicalElems, function(index, value){
                    var actualAmount = $(value).attr('data-amount');
                    if($(value).find('.deductable-amount').length) {
                        actualAmount = $(value).find('.deductable-amount').text();
                    }
                    outputamounts.push({'rate_id' : $(value).attr('data-amtid'), 'amount' : actualAmount});
                });

                // var selectedTripCancellation = $('.trip-cancellation-list option:selected').attr('data-amount');
                // var selectedTripId = $('#inclusive-amounts option:selected').attr('data-amtid');
                // if($('.trip-cancellation-list').val() !== '-1') {
                //     outputamounts.push({'rate_id' : selectedTripId, 'amount' : selectedTripCancellation});
                // }
            }

            amount_output.push({'p_id' : prod_id, 'pr_amount' : outputamounts});
            localStorage.setItem('quoteamounts', JSON.stringify(amount_output));
            console.log('quoteamounts', amount_output);

            $('.error-message-quest-validation').hide();
            $('#gridSystemModal').modal('hide');
            $('.get-quote-option').show();
            $('.select-item-txt-'+prod_id).show();
            $('.selected-items-'+prod_id).show();
            $(".selected-items-"+prod_id ).prop( "checked", true );
        }


        // console.log('all_amounts', all_amounts);
        // console.log('outputamounts', outputamounts);
        // console.log('amount_output', amount_output);
        // console.log('strarray', strarray);
        $('.add-questionnaire.current').closest('.product-item').find("span.quote-rate").addClass('inactive');
        $.each(strarray, function(i, el){
            $.each(el.data, function(index, value){
                $("[data-amount='" + value + "']").removeClass('inactive').addClass("add-active");
            });
        });

        if(prod_id == 46 || prod_id == 96) {
            $('.add-questionnaire.current').closest('.product-item').find("span.quote-rate").removeClass('inactive').addClass('add-active');
        }

        if(!$('.questionnaire-validation table.table_radio tbody tr:visible:not(:has(:radio:checked))').length) {
            $('.add-questionnaire.current').addClass('disabled');
            // $('.add-questionnaire.current').closest('.product-item').find('select:not(.style-btn)').attr('disabled', 'disabled');
            if(prod_id == 46 || prod_id == 96) {
                $('.add-questionnaire.current').closest('.product-item').find("span.quote-rate, select.quote-rate").addClass('add-active');
            }
        }
    });

    // Sum Insured list filtering
    function selectInclusiveAmount() {
        var selectedAmount = $("#inclusive-amounts").val();
        var numOfTravellers = $('#inclusive-amounts option:selected').data('travellers');
        var numOfDays = $('#inclusive-amounts option:selected').data('days');
        var amountPerDay = $('#inclusive-amounts option:selected').data('amount');
        $('.travellers-number').text(numOfTravellers);
        $('.days-number').text(numOfDays);
        $('.amount-per-day').text(amountPerDay);
        // $('.total-amount').text(selectedAmount);
    }

    if($("#inclusive-amounts").length) {
        selectInclusiveAmount();
    }

    $('#inclusive-amounts').change(function(e) {
        selectInclusiveAmount();
    });

    $('.sum-assured-list').change(function(e) {
        // $('.update_sum_assured').removeClass('disabled');
        var selectedValue = $('.sum-assured-list').val();
        $('#inclusive-amounts option[data-sum-assured=' + selectedValue + ']').attr('selected', 'selected');
        selectInclusiveAmount();
    });
    // $('.update_sum_assured').click(function(e) {
    //     var selectedValue = $('.sum-assured-list').val();
    //     $('#inclusive-amounts option[data-sum-assured=' + selectedValue + ']').attr('selected', 'selected');
    //     selectInclusiveAmount();
    // });

    // $('.reset_sum_assured').click(function(e){
    //     $('.update_sum_assured').addClass('disabled');
    //     $('.sum-assured-list').val('-1');
    // });

    //Travelance Trip Cancellation
    $('.trip-sum-assured-list').change(function() {
        // $('.update_trip_sum_assured').removeClass('disabled');
        var selectedValue = $('.trip-sum-assured-list').val();
        $('.trip-cancellation-list option[data-sum-assured=' + selectedValue + ']').attr('selected', 'selected');
    });

    // $('.reset_trip_sum_assured').click(function(e){
    //     $('.update_trip_sum_assured').addClass('disabled');
    //     $('.trip-sum-assured-list').val('-1');
    //     $('.trip-cancellation-list option:first-child').attr('selected', 'selected');
    // });

    // $('.update_trip_sum_assured').click(function(e) {
    //     var selectedValue = $('.trip-sum-assured-list').val();
    //     $('.trip-cancellation-list option[data-sum-assured=' + selectedValue + ']').attr('selected', 'selected');
    // });

    //Tugo filter sum insured

    function selectTugoInclusiveAmount() {
        var selectedAmount = $("#tugo-inclusive-amounts").val();
        var numOfTravellers = $('#tugo-inclusive-amounts option:selected').data('travellers');
        var numOfDays = $('#tugo-inclusive-amounts option:selected').data('days');
        var amountPerDay = $('#tugo-inclusive-amounts option:selected').data('amount');
        $('.tugo-travellers-number').text(numOfTravellers);
        $('.tugo-days-number').text(numOfDays);
        $('.tugo-amount-per-day').text(amountPerDay);
        // $('.tugo-total-amount').text(selectedAmount);
    }
    if($("#tugo-inclusive-amounts").length) {
        selectTugoInclusiveAmount();
    }
    $('#tugo-inclusive-amounts').change(function(e) {
        selectTugoInclusiveAmount();
    });
    $('.tugo-sum-assured-list').change(function(e) {
        // $('.update_tugo_sum_assured').removeClass('disabled');
        var selectedValue = $('.tugo-sum-assured-list').val();
        $('#tugo-inclusive-amounts option[data-sum-assured=' + selectedValue + ']').attr('selected', 'selected');
        selectTugoInclusiveAmount();
    });
    // $('.update_tugo_sum_assured').click(function(e) {
    //     var selectedValue = $('.tugo-sum-assured-list').val();
    //     $('#tugo-inclusive-amounts option[data-sum-assured=' + selectedValue + ']').attr('selected', 'selected');
    //     selectTugoInclusiveAmount();
    // });
    // $('.reset_tugo_sum_assured').click(function(e){
    //     $('.update_tugo_sum_assured').addClass('disabled');
    //     $('.tugo-sum-assured-list').val('-1');
    //     $('#tugo-inclusive-amounts option:first-child').attr('selected', 'selected');
    //     selectTugoInclusiveAmount();
    // });


    // $('.sum-deductable-value').change(function() {
    //     $('.apply_deductables').removeClass('disabled');
    // });
    $('.sum-deductable-value').change(function(e) {
        var selectedDeductableValue = $('.sum-deductable-value').val();
        var parentEl = $(e.currentTarget).closest(".product-item").find('.sub_category');
        if(selectedDeductableValue != ''){
            $('.deductable-amount').remove();
            var final_amount = 0;
            $.each(parentEl, function(i, e){
                var disount;
                var deductableFlag = true;
                var actual_amount = parseFloat($(e).find('span.quote-rate').data("amount"));
                var total_days = parseFloat($(e).find('span.quote-rate').data("days"));
                //localStorage.setItem('a_amount', actual_amount);
                if($(e).find('span.quote-rate').data("emergency") == 'Emergency Medical Insurance'){
                    if(selectedDeductableValue == 0){
                        disount = (0.15) * actual_amount;
                        final_amount = (actual_amount + disount) * total_days;
                    }else if(selectedDeductableValue == 300){
                        deductableFlag = false;
                        $('small.emergency_medical').removeClass('strike-text');
                    }else if(selectedDeductableValue == 500){
                        var disount = (0.05) * actual_amount;
                        final_amount = (actual_amount - disount) * total_days;
                    }else if(selectedDeductableValue == 1000){
                        disount = (0.1) * actual_amount;
                        final_amount = (actual_amount - disount) * total_days;
                    }else if(selectedDeductableValue == 2000){
                        disount = (0.2) * actual_amount;
                        final_amount = (actual_amount - disount) * total_days;
                    }else if(selectedDeductableValue == 5000){
                        disount = (0.3) * actual_amount;
                        final_amount = (actual_amount - disount) * total_days;
                    }else if(selectedDeductableValue == 10000){
                        disount = (0.4) * actual_amount;
                        final_amount = (actual_amount - disount) * total_days;
                    }else if(selectedDeductableValue == 25000){
                        disount = (0.55) * actual_amount;
                        final_amount = (actual_amount - disount) * total_days;
                    }else if(selectedDeductableValue == 50000){
                        disount = (0.65) * actual_amount;
                        final_amount = (actual_amount - disount) * total_days;
                    }else if(selectedDeductableValue == 100000){
                        disount = (0.75) * actual_amount;
                        final_amount = (actual_amount - disount) * total_days;
                    }
                    if(deductableFlag){
                        var final_amount_elem = document.createElement('small');
                        final_amount_elem.classList = 'deductable-amount';
                        final_amount_elem.textContent = final_amount.toFixed(2);
                        $(e).find('span.quote-rate small.emergency_medical').addClass('strike-text');
                        $(e).find('span.quote-rate small.emergency_medical').after(final_amount_elem);
                        $(e).find('span.quote-rate').attr('data-final-amount', final_amount.toFixed(2));
                    }
                }
                //console.log($(e).data("emergency"));
            });
        }


        //var amountElems = parentEl.find(".sub_category");
        //console.log(amountElems);
        /*amountElems.each(function(i, e) {
            alert($(e).data("emergency"));
            if($(e).data("emergency") == 'Emergency Medical Insurance'){
                //alert($('.'));
            }
        }); */


        //parentElem.find('.emergency_medical').removeClass('disabled');
        //parentElem.find('.quote-rate').addClass('disabled');
        //$('.quote-rate[data-sum-assured=' + selectedValue + ']').removeClass('disabled');
    });

    // $('.reset_deductables').click(function(e) {
    //     $(e.currentTarget).closest(".product-item").find('.sub_category span.quote-rate').removeAttr('data-final-amount');
    //     $('small.emergency_medical').removeClass('strike-text');
    //     $('.deductable-amount').remove();
    //     $('.sum-deductable-value').val('-1');
    //     $('.apply_deductables').addClass('disabled');
    // });

    /**
     *
     * save final data
     */
    $('.get-final-quote').click(function (e) {
        var cur_url = new URL(window.location.href);
        $('.final-quote-btn').prop('disabled', 'false');
        $('.form-loader').show();
        var quote_id = $('.quote_id').val();
        var retrievedData = localStorage.getItem("questdata");
        var formatted_data = JSON.parse(retrievedData);
        var retrievedAmountData = localStorage.getItem("quoteamounts");
        var formatted_amount_data = JSON.parse(retrievedAmountData);
        var dob_2 = $('.dob_2').val();
        var no_travelers = $('.no_travelers').val();
        var applicant = 0;
        if(dob_2 == 'undefined'){applicant = 1;}else{applicant = 2;}
        // console.log(formatted_data);
        // console.log(formatted_amount_data);return false;
        var ajax_url = $('.admin-ajax').val();
        // ajax call
        $.ajax({
            url: ajax_url,
            data: {
                'action': 'process_final_quote',
                'quote_id' : quote_id,
                'raw_data' : formatted_data,
                'applicant' : applicant,
                'raw_amount_data' : formatted_amount_data,
                'no_travelers' : no_travelers
            },
            type: 'POST',
            dataType: "json",
            success:function(data) {
                //console.log(data);return false;
                if(data.status == 1){
                    $('.form-loader').hide();
                    //cur_url.substring(0, cur_url.indexOf('?'));
                    //window.location.href =  window.location.href.split("?")[0];
                    setTimeout(function(){
                        window.location.replace(site_url+'/quote-success');
                    },1000);
                }
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        });


    });

 
    $.validator.setDefaults({
      submitHandler: function() {
        $('.form-loader').show();
        setTimeout(function(){
            $('.form-loader').hide();
            window.location.replace(site_url+"/generate-quote-student?dob1=");
        },500);
      }
    });

    $("#signupForm").validate({
			rules: {
				firstname: {
          required:true,
          lettersonly:true
        },
				lastname: {
          required:true,
          lettersonly:true
        },
				phonenumber: {
					required: true
				},
				dateofbirth: {
					required: true
				},
				destination: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				departure_date: {
					required: true
				},
				expiry_date: "required"
			},
			messages: {
				firstname: {
          required: "Please enter your First name",
          lettersonly: "Please enter a valid name"
        },
        lastname: {
          required: "Please enter your Last name",
          lettersonly: "Please enter a valid name"
        },
				phonenumber: {
					required: "Please enter your phone number"
				},
				dateofbirth: {
					required: "Please provide a date of birth"
				},
				destination: {
					required: "Please select a destination"
				},
				email:{
          	required: "Please enter an email address",
            email: "Please enter a valid email address",
        },
				departure_date: "Please provide a departure date",
        expiry_date: "Please provide a return date"
			}
		});



    //var dob_1 = localStorage.getItem('dob_1');console.log(dob_1);

    // Email validate function
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    var bodyEl = $('body'),
    accordionDT = $('.accordion').find('dt'),
    accordionDD = accordionDT.next('dd'),
    parentHeight = accordionDD.height(),
    childHeight = accordionDD.children('.content').outerHeight(true),
    newHeight = parentHeight > 0 ? 0 : childHeight,
    accordionPanel = $('.accordion-panel'),
    buttonsWrapper = accordionPanel.find('.buttons-wrapper'),
    openBtn = accordionPanel.find('.open-btn'),
    closeBtn = accordionPanel.find('.close-btn');

    bodyEl.on('click', function(argument) {
        var totalItems = $('.accordion').children('dt').length;
        var totalItemsOpen = $('.accordion').children('dt.is-open').length;

        if (totalItems == totalItemsOpen) {
        openBtn.addClass('hidden');
        closeBtn.removeClass('hidden');
        buttonsWrapper.addClass('is-open');
        } else {
        openBtn.removeClass('hidden');
        closeBtn.addClass('hidden');
        buttonsWrapper.removeClass('is-open');
        }
    });

  function openAll() {
    openBtn.on('click', function(argument) {
      accordionDD.each(function(argument) {
        var eachNewHeight = $(this).children('.content').outerHeight(true);
        $(this).css({
          height: eachNewHeight
        });
      });
      accordionDT.addClass('is-open');
    });
  }

  function closeAll() {
    closeBtn.on('click', function(argument) {
      accordionDD.css({
        height: 0
      });
      accordionDT.removeClass('is-open');
    });
  }

  function openCloseItem() {
    accordionDT.on('click', function() {
      var el = $(this),
        target = el.next('dd'),
        parentHeight = target.height(),
        childHeight = target.children('.content').outerHeight(true),
        newHeight = parentHeight > 0 ? 0 : childHeight;

      // animate to new height
      target.css({
        height: newHeight
      });

      // remove existing classes & add class to clicked target
      if (!el.hasClass('is-open')) {
        el.addClass('is-open');
      }

      // if we are on clicked target then remove the class
      else {
        el.removeClass('is-open');
      }
    });
  }

  openAll();
  closeAll();
  openCloseItem();

});
