<?php
 
 /* Template Name: Travel Insurance */ 
 
 get_header();
 ?>
 <!-- Travel Insurance landing page -->
 <div class="insurance-landing-section">
     <div class="container">
         <div class="insurance-landing-content">
             <h2>Compare Quotes to Find the Cheapest Travel Insurance</h2>
             <h5>A single search can save you 68%.*</h5>
             <h5>Explore the best rates now.</h5>
             <a href="<?php echo site_url();?>/travel-insurance-quote" class="btn btn-primary main-btn">Get Quotes</a>
         </div>
     </div>
 </div>

 <div class="section pages section-border">
    <ul class="breadcrumb" style="margin: 0px 0px 20px 0px !important;padding: 8px 60px;">
        <li><a href="<?php echo get_home_url(); ?>">Home</a></li>
        <li class="active"><?php echo get_the_title(); ?></li>
    </ul>
    <div class="container"> 
        <div class="row">
            <div class="col-sm-8 col-md-8">
                <h2 class="section-heading">
                    About Travel Insurance
                </h2>
                <div class="section-subheading">Super Visa issuing came with a set of mandatory requirements such as medical insurance for a minimum of 1 year for each visa applicant.The insurance must include:</div> 
                <ul class="checklist">
                    <li>Coverage for emergency medical attention, hospitalization and repatriation.</li>
                    <li>Minimum insurance coverage of $ 100,000.</li>
                    <li>Availability for review by a port of entry officer with each entry.</li>
                    <li>Validity throughout the stay of the visa holder.</li>
                </ul>
                <div>Seeing the demand for Super Visa Insurance many insurance companies in Canada have come forward to provide Super Visa Insurance services.		</div>
            </div>  

            <div class="col-sm-4 col-md-4">
                <img src="<?php bloginfo('template_url'); ?>/assets/images/travel-insurance.jpg" class="Travel Insurance" />

            </div>
        </div> 
        </div>
    </div>
</div>

<div class="section services section-border visa-ins-secondary-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-md-4 col-md-push-8">
				<div class="widget categories">
					<ul class="category-nav">
						<li class="active"><a href="javascript:void(0)" data-href="#pre-conditions">Super Visa Pre-Existing Condtions</a></li>
						<li><a href="javascript:void(0)" data-href="#faqs">Super Visa Insurance FAQ's</a></li>
						<li><a href="javascript:void(0)" data-href="#monthly-plan">Super Visa Insurance Monthly Plan</a></li>
						<li><a href="javascript:void(0)" data-href="#refund">Super Visa Insurance Refund</a></li>
						<li><a href="javascript:void(0)" data-href="#heart-patients">Super Visa Insurance Coverage - Heart Patients</a></li>
						<li><a href="javascript:void(0)" data-href="#alzheimer">Super Visa Insurance Coverage - Alzheimer</a></li>
					</ul>
				</div>  
			</div>
			<div class="col-sm-8 col-md-8 col-md-pull-4">
				<div id="pre-conditions" class="category-content show">
					<div class="single-page"> 
						<h2 class="section-heading">
						Cost and Eligibility for Super Visa Insurance
						</h2>
						<p class="p-line-height">
							As per online insurance quoting websites, a single Super Life Insurance will cost you $ 138 on an average every month or $ 1660 on an average per annum. 
							This insurance is only valid for parents or grandparents of a Canadian citizen or resident who passes the minimum monthly income necessary to sponsor the applicant. Apart from the financial stability, even the purpose of the visit and a clear background check on the applicant will also decide the further proceedings of the application.
						</p>
						<p class="p-line-height">
							In case, the application is rejected by the Canadian High Commission due to any unfortunate situations, the complete amount will be settled as a refund prior to the commencement of the insurance coverage period. In all other cases, a refund will be provided after a deduction of a minimum of $100 to a maximum of $250 as administration fee charges.
						</p> 
					</div>
				</div>
				<div id="faqs" class="category-content">
					<div class="single-page"> 
						<h2 class="section-heading">
						Cost and Eligibility for Super Visa Insurance
						</h2>
						<p class="p-line-height">
							As per online insurance quoting websites, a single Super Life Insurance will cost you $ 138 on an average every month or $ 1660 on an average per annum. 
							This insurance is only valid for parents or grandparents of a Canadian citizen or resident who passes the minimum monthly income necessary to sponsor the applicant. Apart from the financial stability, even the purpose of the visit and a clear background check on the applicant will also decide the further proceedings of the application.
						</p>
						<p class="p-line-height">
							In case, the application is rejected by the Canadian High Commission due to any unfortunate situations, the complete amount will be settled as a refund prior to the commencement of the insurance coverage period. In all other cases, a refund will be provided after a deduction of a minimum of $100 to a maximum of $250 as administration fee charges.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> 


<div class="section news section-border">
	<div class="container"> 
			<h3 style="color: #2ba7de;">
				Read more about Super Visa Insurance
			</h3>
		<div class="row grid-services"> 
			<div class="col-sm-6 col-md-4">
				<div class="box-news-1">
					<div class="image">
						<a href="news-detail.html" title="House Insurance">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive">
						</a>
					</div>
					<div class="meta-date">March 20, 2016</div>
					<h3 class="blok-title"><a href="#" title="How to protect your kid for their future dream.">How to protect your kid for their future dream.</a></h3>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="box-news-1">
					<div class="image">
						<a href="news-detail.html" title="House Insurance">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive">
						</a>
					</div>
					<div class="meta-date">March 20, 2016</div>
					<h3 class="blok-title"><a href="#" title="How to protect your kid for their future dream.">Traveling more confident with our insurance.</a></h3>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="box-news-1">
					<div class="image">
						<a href="news-detail.html" title="House Insurance">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive">
						</a>
					</div>
					<div class="meta-date">March 20, 2016</div>
					<h3 class="blok-title"><a href="#" title="How to protect your kid for their future dream.">Your vacation more confident and safety with us.</a></h3>
				</div>
			</div>  
		</div>  
	</div>
</div> 
 


<?php
get_footer(); 