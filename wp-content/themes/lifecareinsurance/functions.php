<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
// Load Composer's autoloader
require 'vendor/autoload.php';

/**
 * Twenty Twenty functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

/**
 * Table of Contents:
 * Theme Support
 * Required Files
 * Register Styles
 * Register Scripts
 * Register Menus
 * Custom Logo
 * WP Body Open
 * Register Sidebars
 * Enqueue Block Editor Assets
 * Enqueue Classic Editor Styles
 * Block Editor Settings
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function twentytwenty_theme_support() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Custom background color.
	add_theme_support(
		'custom-background',
		array(
			'default-color' => 'f5efe0',
		)
	);

	// Set content-width.
	global $content_width;
	if ( ! isset( $content_width ) ) {
		$content_width = 580;
	}

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// Set post thumbnail size.
	set_post_thumbnail_size( 1200, 9999 );

	// Add custom image size used in Cover Template.
	add_image_size( 'twentytwenty-fullscreen', 1980, 9999 );

	// Custom logo.
	$logo_width  = 120;
	$logo_height = 90;

	// If the retina setting is active, double the recommended width and height.
	if ( get_theme_mod( 'retina_logo', false ) ) {
		$logo_width  = floor( $logo_width * 2 );
		$logo_height = floor( $logo_height * 2 );
	}

	add_theme_support(
		'custom-logo',
		array(
			'height'      => $logo_height,
			'width'       => $logo_width,
			'flex-height' => true,
			'flex-width'  => true,
		)
	);

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'style',
		)
	);

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Twenty Twenty, use a find and replace
	 * to change 'twentytwenty' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'twentytwenty' );

	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	// Add support for responsive embeds.
	add_theme_support( 'responsive-embeds' );

	/*
	 * Adds starter content to highlight the theme on fresh sites.
	 * This is done conditionally to avoid loading the starter content on every
	 * page load, as it is a one-off operation only needed once in the customizer.
	 */
	if ( is_customize_preview() ) {
		require get_template_directory() . '/inc/starter-content.php';
		add_theme_support( 'starter-content', twentytwenty_get_starter_content() );
	}

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * Adds `async` and `defer` support for scripts registered or enqueued
	 * by the theme.
	 */
	$loader = new TwentyTwenty_Script_Loader();
	add_filter( 'script_loader_tag', array( $loader, 'filter_script_loader_tag' ), 10, 2 );

}

add_action( 'after_setup_theme', 'twentytwenty_theme_support' );

/**
 * REQUIRED FILES
 * Include required files.
 */
require get_template_directory() . '/inc/template-tags.php';

// Handle SVG icons.
require get_template_directory() . '/classes/class-twentytwenty-svg-icons.php';
require get_template_directory() . '/inc/svg-icons.php';

// Handle Customizer settings.
require get_template_directory() . '/classes/class-twentytwenty-customize.php';

// Require Separator Control class.
require get_template_directory() . '/classes/class-twentytwenty-separator-control.php';

// Custom comment walker.
require get_template_directory() . '/classes/class-twentytwenty-walker-comment.php';

// Custom page walker.
require get_template_directory() . '/classes/class-twentytwenty-walker-page.php';

// Custom script loader class.
require get_template_directory() . '/classes/class-twentytwenty-script-loader.php';

// Non-latin language handling.
require get_template_directory() . '/classes/class-twentytwenty-non-latin-languages.php';

// Custom CSS.
require get_template_directory() . '/inc/custom-css.php';

/**
 * Register and Enqueue Styles.
 */
function twentytwenty_register_styles() {

	$theme_version = wp_get_theme()->get( 'Version' );

	wp_enqueue_style( 'twentytwenty-style', get_stylesheet_uri(), array(), $theme_version );
	wp_style_add_data( 'twentytwenty-style', 'rtl', 'replace' );

	// Add output of Customizer settings as inline style.
	wp_add_inline_style( 'twentytwenty-style', twentytwenty_get_customizer_css( 'front-end' ) );

	// Add print CSS.
	wp_enqueue_style( 'twentytwenty-print-style', get_template_directory_uri() . '/print.css', null, $theme_version, 'print' );

}

add_action( 'wp_enqueue_scripts', 'twentytwenty_register_styles' );

/**
 * Register and Enqueue Scripts.
 */
function twentytwenty_register_scripts() {

	$theme_version = wp_get_theme()->get( 'Version' );

	if ( ( ! is_admin() ) && is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'twentytwenty-js', get_template_directory_uri() . '/assets/js/index.js', array(), $theme_version, false );
	wp_script_add_data( 'twentytwenty-js', 'async', true );

}

add_action( 'wp_enqueue_scripts', 'twentytwenty_register_scripts' );

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function twentytwenty_skip_link_focus_fix() {
	// The following is minified via `terser --compress --mangle -- assets/js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'twentytwenty_skip_link_focus_fix' );

/** Enqueue non-latin language styles
 *
 * @since Twenty Twenty 1.0
 *
 * @return void
 */
function twentytwenty_non_latin_languages() {
	$custom_css = TwentyTwenty_Non_Latin_Languages::get_non_latin_css( 'front-end' );

	if ( $custom_css ) {
		wp_add_inline_style( 'twentytwenty-style', $custom_css );
	}
}

add_action( 'wp_enqueue_scripts', 'twentytwenty_non_latin_languages' );

/**
 * Register navigation menus uses wp_nav_menu in five places.
 */
function twentytwenty_menus() {

	$locations = array(
		'primary'  => __( 'Desktop Horizontal Menu', 'twentytwenty' ),
		'expanded' => __( 'Desktop Expanded Menu', 'twentytwenty' ),
		'mobile'   => __( 'Mobile Menu', 'twentytwenty' ),
		'footer'   => __( 'Footer Menu', 'twentytwenty' ),
		'social'   => __( 'Social Menu', 'twentytwenty' ),
	);

	register_nav_menus( $locations );
}

add_action( 'init', 'twentytwenty_menus' );

/**
 * Get the information about the logo.
 *
 * @param string $html The HTML output from get_custom_logo (core function).
 *
 * @return string $html
 */
function twentytwenty_get_custom_logo( $html ) {

	$logo_id = get_theme_mod( 'custom_logo' );

	if ( ! $logo_id ) {
		return $html;
	}

	$logo = wp_get_attachment_image_src( $logo_id, 'full' );

	if ( $logo ) {
		// For clarity.
		$logo_width  = esc_attr( $logo[1] );
		$logo_height = esc_attr( $logo[2] );

		// If the retina logo setting is active, reduce the width/height by half.
		if ( get_theme_mod( 'retina_logo', false ) ) {
			$logo_width  = floor( $logo_width / 2 );
			$logo_height = floor( $logo_height / 2 );

			$search = array(
				'/width=\"\d+\"/iU',
				'/height=\"\d+\"/iU',
			);

			$replace = array(
				"width=\"{$logo_width}\"",
				"height=\"{$logo_height}\"",
			);

			// Add a style attribute with the height, or append the height to the style attribute if the style attribute already exists.
			if ( strpos( $html, ' style=' ) === false ) {
				$search[]  = '/(src=)/';
				$replace[] = "style=\"height: {$logo_height}px;\" src=";
			} else {
				$search[]  = '/(style="[^"]*)/';
				$replace[] = "$1 height: {$logo_height}px;";
			}

			$html = preg_replace( $search, $replace, $html );

		}
	}

	return $html;

}

add_filter( 'get_custom_logo', 'twentytwenty_get_custom_logo' );

if ( ! function_exists( 'wp_body_open' ) ) {

	/**
	 * Shim for wp_body_open, ensuring backward compatibility with versions of WordPress older than 5.2.
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
}

/**
 * Include a skip to content link at the top of the page so that users can bypass the menu.
 */
function twentytwenty_skip_link() {
	echo '<a class="skip-link screen-reader-text" href="#site-content">' . __( 'Skip to the content', 'twentytwenty' ) . '</a>';
}

add_action( 'wp_body_open', 'twentytwenty_skip_link', 5 );

/**
 * Register widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twentytwenty_sidebar_registration() {

	// Arguments used in all register_sidebar() calls.
	$shared_args = array(
		'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
		'after_title'   => '</h2>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget'  => '</div></div>',
	);

	// Footer #1.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Footer #1', 'twentytwenty' ),
				'id'          => 'sidebar-1',
				'description' => __( 'Widgets in this area will be displayed in the first column in the footer.', 'twentytwenty' ),
			)
		)
	);

	// Footer #2.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Footer #2', 'twentytwenty' ),
				'id'          => 'sidebar-2',
				'description' => __( 'Widgets in this area will be displayed in the second column in the footer.', 'twentytwenty' ),
			)
		)
	);

}

add_action( 'widgets_init', 'twentytwenty_sidebar_registration' );

/**
 * Enqueue supplemental block editor styles.
 */
function twentytwenty_block_editor_styles() {

	// Enqueue the editor styles.
	wp_enqueue_style( 'twentytwenty-block-editor-styles', get_theme_file_uri( '/assets/css/editor-style-block.css' ), array(), wp_get_theme()->get( 'Version' ), 'all' );
	wp_style_add_data( 'twentytwenty-block-editor-styles', 'rtl', 'replace' );

	// Add inline style from the Customizer.
	wp_add_inline_style( 'twentytwenty-block-editor-styles', twentytwenty_get_customizer_css( 'block-editor' ) );

	// Add inline style for non-latin fonts.
	wp_add_inline_style( 'twentytwenty-block-editor-styles', TwentyTwenty_Non_Latin_Languages::get_non_latin_css( 'block-editor' ) );

	// Enqueue the editor script.
	wp_enqueue_script( 'twentytwenty-block-editor-script', get_theme_file_uri( '/assets/js/editor-script-block.js' ), array( 'wp-blocks', 'wp-dom' ), wp_get_theme()->get( 'Version' ), true );
}

add_action( 'enqueue_block_editor_assets', 'twentytwenty_block_editor_styles', 1, 1 );

/**
 * Enqueue classic editor styles.
 */
function twentytwenty_classic_editor_styles() {

	$classic_editor_styles = array(
		'/assets/css/editor-style-classic.css',
	);

	add_editor_style( $classic_editor_styles );

}

add_action( 'init', 'twentytwenty_classic_editor_styles' );

/**
 * Output Customizer settings in the classic editor.
 * Adds styles to the head of the TinyMCE iframe. Kudos to @Otto42 for the original solution.
 *
 * @param array $mce_init TinyMCE styles.
 *
 * @return array $mce_init TinyMCE styles.
 */
function twentytwenty_add_classic_editor_customizer_styles( $mce_init ) {

	$styles = twentytwenty_get_customizer_css( 'classic-editor' );

	if ( ! isset( $mce_init['content_style'] ) ) {
		$mce_init['content_style'] = $styles . ' ';
	} else {
		$mce_init['content_style'] .= ' ' . $styles . ' ';
	}

	return $mce_init;

}

add_filter( 'tiny_mce_before_init', 'twentytwenty_add_classic_editor_customizer_styles' );

/**
 * Output non-latin font styles in the classic editor.
 * Adds styles to the head of the TinyMCE iframe. Kudos to @Otto42 for the original solution.
 *
 * @param array $mce_init TinyMCE styles.
 *
 * @return array $mce_init TinyMCE styles.
 */
function twentytwenty_add_classic_editor_non_latin_styles( $mce_init ) {

	$styles = TwentyTwenty_Non_Latin_Languages::get_non_latin_css( 'classic-editor' );

	// Return if there are no styles to add.
	if ( ! $styles ) {
		return $mce_init;
	}

	if ( ! isset( $mce_init['content_style'] ) ) {
		$mce_init['content_style'] = $styles . ' ';
	} else {
		$mce_init['content_style'] .= ' ' . $styles . ' ';
	}

	return $mce_init;

}

add_filter( 'tiny_mce_before_init', 'twentytwenty_add_classic_editor_non_latin_styles' );

/**
 * Block Editor Settings.
 * Add custom colors and font sizes to the block editor.
 */
function twentytwenty_block_editor_settings() {

	// Block Editor Palette.
	$editor_color_palette = array(
		array(
			'name'  => __( 'Accent Color', 'twentytwenty' ),
			'slug'  => 'accent',
			'color' => twentytwenty_get_color_for_area( 'content', 'accent' ),
		),
		array(
			'name'  => __( 'Primary', 'twentytwenty' ),
			'slug'  => 'primary',
			'color' => twentytwenty_get_color_for_area( 'content', 'text' ),
		),
		array(
			'name'  => __( 'Secondary', 'twentytwenty' ),
			'slug'  => 'secondary',
			'color' => twentytwenty_get_color_for_area( 'content', 'secondary' ),
		),
		array(
			'name'  => __( 'Subtle Background', 'twentytwenty' ),
			'slug'  => 'subtle-background',
			'color' => twentytwenty_get_color_for_area( 'content', 'borders' ),
		),
	);

	// Add the background option.
	$background_color = get_theme_mod( 'background_color' );
	if ( ! $background_color ) {
		$background_color_arr = get_theme_support( 'custom-background' );
		$background_color     = $background_color_arr[0]['default-color'];
	}
	$editor_color_palette[] = array(
		'name'  => __( 'Background Color', 'twentytwenty' ),
		'slug'  => 'background',
		'color' => '#' . $background_color,
	);

	// If we have accent colors, add them to the block editor palette.
	if ( $editor_color_palette ) {
		add_theme_support( 'editor-color-palette', $editor_color_palette );
	}

	// Block Editor Font Sizes.
	add_theme_support(
		'editor-font-sizes',
		array(
			array(
				'name'      => _x( 'Small', 'Name of the small font size in the block editor', 'twentytwenty' ),
				'shortName' => _x( 'S', 'Short name of the small font size in the block editor.', 'twentytwenty' ),
				'size'      => 18,
				'slug'      => 'small',
			),
			array(
				'name'      => _x( 'Regular', 'Name of the regular font size in the block editor', 'twentytwenty' ),
				'shortName' => _x( 'M', 'Short name of the regular font size in the block editor.', 'twentytwenty' ),
				'size'      => 21,
				'slug'      => 'normal',
			),
			array(
				'name'      => _x( 'Large', 'Name of the large font size in the block editor', 'twentytwenty' ),
				'shortName' => _x( 'L', 'Short name of the large font size in the block editor.', 'twentytwenty' ),
				'size'      => 26.25,
				'slug'      => 'large',
			),
			array(
				'name'      => _x( 'Larger', 'Name of the larger font size in the block editor', 'twentytwenty' ),
				'shortName' => _x( 'XL', 'Short name of the larger font size in the block editor.', 'twentytwenty' ),
				'size'      => 32,
				'slug'      => 'larger',
			),
		)
	);

	add_theme_support( 'editor-styles' );

	// If we have a dark background color then add support for dark editor style.
	// We can determine if the background color is dark by checking if the text-color is white.
	if ( '#ffffff' === strtolower( twentytwenty_get_color_for_area( 'content', 'text' ) ) ) {
		add_theme_support( 'dark-editor-style' );
	}

}

add_action( 'after_setup_theme', 'twentytwenty_block_editor_settings' );

/**
 * Overwrite default more tag with styling and screen reader markup.
 *
 * @param string $html The default output HTML for the more tag.
 *
 * @return string $html
 */
function twentytwenty_read_more_tag( $html ) {
	return preg_replace( '/<a(.*)>(.*)<\/a>/iU', sprintf( '<div class="read-more-button-wrap"><a$1><span class="faux-button">$2</span> <span class="screen-reader-text">"%1$s"</span></a></div>', get_the_title( get_the_ID() ) ), $html );
}

add_filter( 'the_content_more_link', 'twentytwenty_read_more_tag' );

/**
 * Enqueues scripts for customizer controls & settings.
 *
 * @since Twenty Twenty 1.0
 *
 * @return void
 */
function twentytwenty_customize_controls_enqueue_scripts() {
	$theme_version = wp_get_theme()->get( 'Version' );

	// Add main customizer js file.
	wp_enqueue_script( 'twentytwenty-customize', get_template_directory_uri() . '/assets/js/customize.js', array( 'jquery' ), $theme_version, false );

	// Add script for color calculations.
	wp_enqueue_script( 'twentytwenty-color-calculations', get_template_directory_uri() . '/assets/js/color-calculations.js', array( 'wp-color-picker' ), $theme_version, false );

	// Add script for controls.
	wp_enqueue_script( 'twentytwenty-customize-controls', get_template_directory_uri() . '/assets/js/customize-controls.js', array( 'twentytwenty-color-calculations', 'customize-controls', 'underscore', 'jquery' ), $theme_version, false );
	wp_localize_script( 'twentytwenty-customize-controls', 'twentyTwentyBgColors', twentytwenty_get_customizer_color_vars() );
}

add_action( 'customize_controls_enqueue_scripts', 'twentytwenty_customize_controls_enqueue_scripts' );

/**
 * Enqueue scripts for the customizer preview.
 *
 * @since Twenty Twenty 1.0
 *
 * @return void
 */
function twentytwenty_customize_preview_init() {
	$theme_version = wp_get_theme()->get( 'Version' );

	wp_enqueue_script( 'twentytwenty-customize-preview', get_theme_file_uri( '/assets/js/customize-preview.js' ), array( 'customize-preview', 'customize-selective-refresh', 'jquery' ), $theme_version, true );
	wp_localize_script( 'twentytwenty-customize-preview', 'twentyTwentyBgColors', twentytwenty_get_customizer_color_vars() );
	wp_localize_script( 'twentytwenty-customize-preview', 'twentyTwentyPreviewEls', twentytwenty_get_elements_array() );

	wp_add_inline_script(
		'twentytwenty-customize-preview',
		sprintf(
			'wp.customize.selectiveRefresh.partialConstructor[ %1$s ].prototype.attrs = %2$s;',
			wp_json_encode( 'cover_opacity' ),
			wp_json_encode( twentytwenty_customize_opacity_range() )
		)
	);
}

add_action( 'customize_preview_init', 'twentytwenty_customize_preview_init' );

/**
 * Get accessible color for an area.
 *
 * @since Twenty Twenty 1.0
 *
 * @param string $area The area we want to get the colors for.
 * @param string $context Can be 'text' or 'accent'.
 * @return string Returns a HEX color.
 */
function twentytwenty_get_color_for_area( $area = 'content', $context = 'text' ) {

	// Get the value from the theme-mod.
	$settings = get_theme_mod(
		'accent_accessible_colors',
		array(
			'content'       => array(
				'text'      => '#000000',
				'accent'    => '#cd2653',
				'secondary' => '#6d6d6d',
				'borders'   => '#dcd7ca',
			),
			'header-footer' => array(
				'text'      => '#000000',
				'accent'    => '#cd2653',
				'secondary' => '#6d6d6d',
				'borders'   => '#dcd7ca',
			),
		)
	);

	// If we have a value return it.
	if ( isset( $settings[ $area ] ) && isset( $settings[ $area ][ $context ] ) ) {
		return $settings[ $area ][ $context ];
	}

	// Return false if the option doesn't exist.
	return false;
}

/**
 * Returns an array of variables for the customizer preview.
 *
 * @since Twenty Twenty 1.0
 *
 * @return array
 */
function twentytwenty_get_customizer_color_vars() {
	$colors = array(
		'content'       => array(
			'setting' => 'background_color',
		),
		'header-footer' => array(
			'setting' => 'header_footer_background_color',
		),
	);
	return $colors;
}

/**
 * Get an array of elements.
 *
 * @since Twenty Twenty 1.0
 *
 * @return array
 */
function twentytwenty_get_elements_array() {

	// The array is formatted like this:
	// [key-in-saved-setting][sub-key-in-setting][css-property] = [elements].
	$elements = array(
		'content'       => array(
			'accent'     => array(
				'color'            => array( '.color-accent', '.color-accent-hover:hover', '.color-accent-hover:focus', ':root .has-accent-color', '.has-drop-cap:not(:focus):first-letter', '.wp-block-button.is-style-outline', 'a' ),
				'border-color'     => array( 'blockquote', '.border-color-accent', '.border-color-accent-hover:hover', '.border-color-accent-hover:focus' ),
				'background-color' => array( 'button:not(.toggle)', '.button', '.faux-button', '.wp-block-button__link', '.wp-block-file .wp-block-file__button', 'input[type="button"]', 'input[type="reset"]', 'input[type="submit"]', '.bg-accent', '.bg-accent-hover:hover', '.bg-accent-hover:focus', ':root .has-accent-background-color', '.comment-reply-link' ),
				'fill'             => array( '.fill-children-accent', '.fill-children-accent *' ),
			),
			'background' => array(
				'color'            => array( ':root .has-background-color', 'button', '.button', '.faux-button', '.wp-block-button__link', '.wp-block-file__button', 'input[type="button"]', 'input[type="reset"]', 'input[type="submit"]', '.wp-block-button', '.comment-reply-link', '.has-background.has-primary-background-color:not(.has-text-color)', '.has-background.has-primary-background-color *:not(.has-text-color)', '.has-background.has-accent-background-color:not(.has-text-color)', '.has-background.has-accent-background-color *:not(.has-text-color)' ),
				'background-color' => array( ':root .has-background-background-color' ),
			),
			'text'       => array(
				'color'            => array( 'body', '.entry-title a', ':root .has-primary-color' ),
				'background-color' => array( ':root .has-primary-background-color' ),
			),
			'secondary'  => array(
				'color'            => array( 'cite', 'figcaption', '.wp-caption-text', '.post-meta', '.entry-content .wp-block-archives li', '.entry-content .wp-block-categories li', '.entry-content .wp-block-latest-posts li', '.wp-block-latest-comments__comment-date', '.wp-block-latest-posts__post-date', '.wp-block-embed figcaption', '.wp-block-image figcaption', '.wp-block-pullquote cite', '.comment-metadata', '.comment-respond .comment-notes', '.comment-respond .logged-in-as', '.pagination .dots', '.entry-content hr:not(.has-background)', 'hr.styled-separator', ':root .has-secondary-color' ),
				'background-color' => array( ':root .has-secondary-background-color' ),
			),
			'borders'    => array(
				'border-color'        => array( 'pre', 'fieldset', 'input', 'textarea', 'table', 'table *', 'hr' ),
				'background-color'    => array( 'caption', 'code', 'code', 'kbd', 'samp', '.wp-block-table.is-style-stripes tbody tr:nth-child(odd)', ':root .has-subtle-background-background-color' ),
				'border-bottom-color' => array( '.wp-block-table.is-style-stripes' ),
				'border-top-color'    => array( '.wp-block-latest-posts.is-grid li' ),
				'color'               => array( ':root .has-subtle-background-color' ),
			),
		),
		'header-footer' => array(
			'accent'     => array(
				'color'            => array( 'body:not(.overlay-header) .primary-menu > li > a', 'body:not(.overlay-header) .primary-menu > li > .icon', '.modal-menu a', '.footer-menu a, .footer-widgets a', '#site-footer .wp-block-button.is-style-outline', '.wp-block-pullquote:before', '.singular:not(.overlay-header) .entry-header a', '.archive-header a', '.header-footer-group .color-accent', '.header-footer-group .color-accent-hover:hover' ),
				'background-color' => array( '.social-icons a', '#site-footer button:not(.toggle)', '#site-footer .button', '#site-footer .faux-button', '#site-footer .wp-block-button__link', '#site-footer .wp-block-file__button', '#site-footer input[type="button"]', '#site-footer input[type="reset"]', '#site-footer input[type="submit"]' ),
			),
			'background' => array(
				'color'            => array( '.social-icons a', 'body:not(.overlay-header) .primary-menu ul', '.header-footer-group button', '.header-footer-group .button', '.header-footer-group .faux-button', '.header-footer-group .wp-block-button:not(.is-style-outline) .wp-block-button__link', '.header-footer-group .wp-block-file__button', '.header-footer-group input[type="button"]', '.header-footer-group input[type="reset"]', '.header-footer-group input[type="submit"]' ),
				'background-color' => array( '#site-header', '.footer-nav-widgets-wrapper', '#site-footer', '.menu-modal', '.menu-modal-inner', '.search-modal-inner', '.archive-header', '.singular .entry-header', '.singular .featured-media:before', '.wp-block-pullquote:before' ),
			),
			'text'       => array(
				'color'               => array( '.header-footer-group', 'body:not(.overlay-header) #site-header .toggle', '.menu-modal .toggle' ),
				'background-color'    => array( 'body:not(.overlay-header) .primary-menu ul' ),
				'border-bottom-color' => array( 'body:not(.overlay-header) .primary-menu > li > ul:after' ),
				'border-left-color'   => array( 'body:not(.overlay-header) .primary-menu ul ul:after' ),
			),
			'secondary'  => array(
				'color' => array( '.site-description', 'body:not(.overlay-header) .toggle-inner .toggle-text', '.widget .post-date', '.widget .rss-date', '.widget_archive li', '.widget_categories li', '.widget cite', '.widget_pages li', '.widget_meta li', '.widget_nav_menu li', '.powered-by-wordpress', '.to-the-top', '.singular .entry-header .post-meta', '.singular:not(.overlay-header) .entry-header .post-meta a' ),
			),
			'borders'    => array(
				'border-color'     => array( '.header-footer-group pre', '.header-footer-group fieldset', '.header-footer-group input', '.header-footer-group textarea', '.header-footer-group table', '.header-footer-group table *', '.footer-nav-widgets-wrapper', '#site-footer', '.menu-modal nav *', '.footer-widgets-outer-wrapper', '.footer-top' ),
				'background-color' => array( '.header-footer-group table caption', 'body:not(.overlay-header) .header-inner .toggle-wrapper::before' ),
			),
		),
	);

	/**
	* Filters Twenty Twenty theme elements
	*
	* @since Twenty Twenty 1.0
	*
	* @param array Array of elements
	*/
	return apply_filters( 'twentytwenty_get_elements_array', $elements );
}

function wp_custom_attachment() {
 
    wp_nonce_field(plugin_basename(__FILE__), 'wp_custom_attachment_nonce');
     
    $html = '<p class="description">';
        $html .= 'Upload your PDF here.';
    $html .= '</p>';
    $html .= '<input type="file" id="wp_custom_attachment" name="wp_custom_attachment" value="" size="25" />';
     
    echo $html;
 
} // end wp_custom_attachment

function add_custom_meta_boxes() {
 
    // Define the custom attachment for posts
    add_meta_box(
        'wp_custom_attachment',
        'Custom Attachment',
        'wp_custom_attachment',
        'post',
        'side'
    );
     
    // Define the custom attachment for pages
    add_meta_box(
        'wp_custom_attachment',
        'Custom Attachment',
        'wp_custom_attachment',
        'page',
        'side'
    );
 
} // end add_custom_meta_boxes
add_action('add_meta_boxes', 'add_custom_meta_boxes');

function save_custom_meta_data($id) {
 
    /* --- security verification --- */
    if(!wp_verify_nonce($_POST['wp_custom_attachment_nonce'], plugin_basename(__FILE__))) {
      return $id;
    } // end if
       
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
      return $id;
    } // end if
       
    if('page' == $_POST['post_type']) {
      if(!current_user_can('edit_page', $id)) {
        return $id;
      } // end if
    } else {
        if(!current_user_can('edit_page', $id)) {
            return $id;
        } // end if
    } // end if
    /* - end security verification - */
     
    // Make sure the file array isn't empty
    if(!empty($_FILES['wp_custom_attachment']['name'])) {
         
        // Setup the array of supported file types. In this case, it's just PDF.
        $supported_types = array('application/pdf');
         
        // Get the file type of the upload
        $arr_file_type = wp_check_filetype(basename($_FILES['wp_custom_attachment']['name']));
        $uploaded_type = $arr_file_type['type'];
         
        // Check if the type is supported. If not, throw an error.
        if(in_array($uploaded_type, $supported_types)) {
 
            // Use the WordPress API to upload the file
            $upload = wp_upload_bits($_FILES['wp_custom_attachment']['name'], null, file_get_contents($_FILES['wp_custom_attachment']['tmp_name']));
     
            if(isset($upload['error']) && $upload['error'] != 0) {
                wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
            } else {
                add_post_meta($id, 'wp_custom_attachment', $upload);
                update_post_meta($id, 'wp_custom_attachment', $upload);     
            } // end if/else
 
        } else {
            wp_die("The file type that you've uploaded is not a PDF.");
        } // end if/else
         
    } // end if
     
} // end save_custom_meta_data
add_action('save_post', 'save_custom_meta_data');

function update_edit_form() {
    echo ' enctype="multipart/form-data"';
} // end update_edit_form
add_action('post_edit_form_tag', 'update_edit_form');



function wp_custom_attachment_1() {
 
    wp_nonce_field(plugin_basename(__FILE__), 'wp_custom_attachment_1_nonce');
     
    $html = '<p class="description">';
        $html .= 'Upload your Second PDF here.';
    $html .= '</p>';
    $html .= '<input type="file" id="wp_custom_attachment_1" name="wp_custom_attachment_1" value="" size="25" />';
     
    echo $html;
 
} // end wp_custom_attachment_1

function add_custom_meta_boxes_1() {
 
    // Define the custom attachment for posts
    add_meta_box(
        'wp_custom_attachment_1',
        'Custom Attachment',
        'wp_custom_attachment_1',
        'post',
        'side'
    );
     
    // Define the custom attachment for pages
    add_meta_box(
        'wp_custom_attachment_1',
        'Custom Attachment',
        'wp_custom_attachment_1',
        'page',
        'side'
    );
 
} // end add_custom_meta_boxes
add_action('add_meta_boxes', 'add_custom_meta_boxes_1');

function save_custom_meta_data_1($id) {
 
    /* --- security verification --- */
    if(!wp_verify_nonce($_POST['wp_custom_attachment_1_nonce'], plugin_basename(__FILE__))) {
      return $id;
    } // end if
       
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
      return $id;
    } // end if
       
    if('page' == $_POST['post_type']) {
      if(!current_user_can('edit_page', $id)) {
        return $id;
      } // end if
    } else {
        if(!current_user_can('edit_page', $id)) {
            return $id;
        } // end if
    } // end if
    /* - end security verification - */
     
    // Make sure the file array isn't empty
    if(!empty($_FILES['wp_custom_attachment_1']['name'])) {
         
        // Setup the array of supported file types. In this case, it's just PDF.
        $supported_types = array('application/pdf');
         
        // Get the file type of the upload
        $arr_file_type = wp_check_filetype(basename($_FILES['wp_custom_attachment_1']['name']));
        $uploaded_type = $arr_file_type['type'];
         
        // Check if the type is supported. If not, throw an error.
        if(in_array($uploaded_type, $supported_types)) {
 
            // Use the WordPress API to upload the file
            $upload = wp_upload_bits($_FILES['wp_custom_attachment_1']['name'], null, file_get_contents($_FILES['wp_custom_attachment_1']['tmp_name']));
     
            if(isset($upload['error']) && $upload['error'] != 0) {
                wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
            } else {
                add_post_meta($id, 'wp_custom_attachment_1', $upload);
                update_post_meta($id, 'wp_custom_attachment_1', $upload);     
            } // end if/else
 
        } else {
            wp_die("The file type that you've uploaded is not a PDF.");
        } // end if/else
         
    } // end if
     
} // end save_custom_meta_data_1
add_action('save_post', 'save_custom_meta_data_1');

function update_edit_form_1() {
    echo ' enctype="multipart/form-data"';
} // end update_edit_form
add_action('post_edit_form_tag', 'update_edit_form_1');

/**
 * Custom functions : Call AJAX Functions
 */



// Set global quote cookie

function setQuoteCookie(){ 
	setcookie("quote", 'No');  
	setcookie("dob_1", '');
	setcookie("pre_exist_1", '');
	setcookie("dob_2", '');
	setcookie("pre_exist_2", '');
	setcookie("departure_date", '');
	setcookie("return_date", '');
	setcookie("province", ''); 
	setcookie("sum_assured",  '');
	setcookie("first_name",  '');
	setcookie("last_name",  '');
	setcookie("email", ''); 
	setcookie("phone", '');
}
add_action( 'init', 'setQuoteCookie');


function get_a_qoute(){ 
	global $wpdb;
	$dob = $_POST['dob']; 
	$effective_date = $_POST['effective_date'];  
	$expiry_date = $_POST['expiry_date'];  
	$departure_city = $_POST['departure_city'];  
	$province = $_POST['province'];  
	$benefits = $_POST['benefits'];  
	$deductable = $_POST['deductable'];  
	$pre_exist = $_POST['pre_exist'];  
	$first_name = $_POST['first_name'];  
	$last_name = $_POST['last_name'];  
	$email = $_POST['email'];  
	$phone = $_POST['phone'];  

	$result = $wpdb->get_var("SELECT COUNT(*) FROM crm_customers WHERE email='$email'");  
	if($result == 0){
		$save_cus_data = $wpdb->insert('crm_customers', array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,  
			'phone' => $phone,
			'business_type' => 1
		));
		$customer_id = $wpdb->insert_id;
	}else{
		$user = $wpdb->get_row("SELECT * FROM crm_customers WHERE email='$email'");  
		$customer_id = $user->id;		
	}

	// save inquiry data

	$save_inq_data = $wpdb->insert('crm_queries', array(
		'business_type_id' => 1,
		'customer_id' => $customer_id,
		'insured_count' => 1,  
		'effective_date' => $effective_date,
		'expiry_date' => $expiry_date,
		'departure_city' => $departure_city,
		'province' => $province,
		'benefits' => $benefits,
		'deductable_price' => $deductable,
		'pre_existing_medical' => $pre_exist,
		'query_status' => 'Pending',
		'query_notes' => 'Empty'
	));

	if($save_inq_data){ 

		// Sending mail to Admin - Intimation


		// Send mail to Customer - Acknowledgment

		echo 1;
	}else{
		echo 0;	} 

	wp_die();	
}
add_action('wp_ajax_get_a_qoute', 'get_a_qoute');
add_action('wp_ajax_nopriv_get_a_qoute', 'get_a_qoute');
 

function update_contact(){
	global $wpdb;
	$dob = $_POST['dob']; 
	$email = $_POST['email'];  
	$phone = $_POST['phone'];  
	$customer_id = $_POST['customer_id'];  

	$result = $wpdb->get_var("SELECT COUNT(*) FROM crm_customers WHERE email='$email'");  
	if($result == 0){ 
		$query_array['dob'] = $dob; 
		$query_array['email'] = $email; 
		$query_array['phone'] = $phone;  
		$updated_data = $wpdb->update('crm_customers',$query_array,array('id'=>$customer_id));  

		echo 1; 
	}else{
		$result1 = $wpdb->get_var("SELECT COUNT(*) FROM crm_customers WHERE email='$email' AND id=$customer_id ");  

		if($result1 != 0){
			$query_array1['dob'] = $dob; 
			$query_array1['phone'] = $phone;  
			$updated_data = $wpdb->update('crm_customers',$query_array1,array('id'=>$customer_id));  
			echo 1;	 				
		}else{
			echo 2;			
		}
	}
 
	wp_die();	
}
add_action('wp_ajax_update_contact', 'update_contact');
add_action('wp_ajax_nopriv_update_contact', 'update_contact');



function update_inquiry_status(){
	global $wpdb;
	$inq_id = $_POST['inq_id']; 
	$status = $_POST['status']; 

	$query_array['status'] = $status;  
	$updated_data = $wpdb->update('crm_quotes',$query_array,array('id'=>$inq_id));  
   
	if($updated_data){   
		echo 1; 
	}else{ 
		echo 0;	
	}
 
	wp_die();	
}
add_action('wp_ajax_update_inquiry_status', 'update_inquiry_status');
add_action('wp_ajax_nopriv_update_inquiry_status', 'update_inquiry_status');



function add_note(){
	global $wpdb;
	$inq_id = $_POST['inq_id']; 
	$note = $_POST['note']; 

	$save_data = $wpdb->insert('crm_notes', array(
		'inquiry_id' => $inq_id,
		'note' => $note 
	));

	if($save_data){  
		echo 1;
	}else{
		echo 0;	}  

	wp_die();	
}
add_action('wp_ajax_add_note', 'add_note');
add_action('wp_ajax_nopriv_add_note', 'add_note');



function add_referal(){
	global $wpdb; 
	$save_data = $wpdb->insert('crm_referals', array(
		'referal_title' => $_POST['title'],
		'description' => $_POST['description'],
		'status' => 1
	));

	if($save_data){  
		echo 1;
	}else{
		echo 0;	}  

	wp_die();	
}
add_action('wp_ajax_add_referal', 'add_referal');
add_action('wp_ajax_nopriv_add_referal', 'add_referal');


/**
 * Function for deleting a referral
 */
function delete_referal(){
	global $wpdb;
	$id = $_POST['item_id'];
	$table = 'crm_referals';
	$delete_action = $wpdb->delete( $table, array( 'id' => $id ) );
	if($delete_action){
		echo 1;
	}else{
		echo 0;
	}
	wp_die();
}
add_action('wp_ajax_delete_referal', 'delete_referal');
add_action('wp_ajax_nopriv_delete_referal', 'delete_referal');


// Function for generating a quote
function generate_qoute(){ 
	global $wpdb;
	// Set cookie values
	setcookie("quote", '');
	$email = $_POST['email']; 
	// Use customer data - save and return customer id
	$customer_id = 100;
	$result = $wpdb->get_var("SELECT id FROM crm_customers WHERE email='$email'");  
	if(empty($result)){ 
		$query_array['first_name'] = $_POST['first_name']; 
		$query_array['last_name'] = $_POST['last_name']; 
		$query_array['dob'] = $_POST['dob']; 
		$query_array['email'] = $_POST['email']; 
		$query_array['phone'] = $_POST['phone'];  
		$wpdb->insert('crm_customers',$query_array);  
		$customer_id = $wpdb->insert_id;
	}else{
		$customer_id = $result;
	}

	// Generate quote - save quote data
	$date =  date('mdY');
	$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 3; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
	$random_id = 'TRV'.$date.$randomString;
	$save_quote_data = $wpdb->insert('crm_quotes', array(
		'quote_id' => $random_id,
		'customer_id' => $customer_id,
		'status' => 'initiated',
		'dob_1' => $_POST['dob_1'], 
		'pre_existing_1' => $_POST['pre_exist_1'], 
		'dob_2' => $_POST['dob_2'], 
		'pre_existing_2' => $_POST['pre_exist_2'], 
		'child1_dob' => $_POST['dob_child_1'], 
		'child1_pre_existing' => $_POST['pre_exist_child1'], 
		'child2_dob' => $_POST['dob_child_2'], 
		'child2_pre_existing' => $_POST['pre_exist_child2'], 
		'relationship' => $_POST['relationship'], 
		'trip_type' => $_POST['trip_type'], 
		'trip_destination' => $_POST['destination'], 
		'departure_date' => $_POST['departure_date'], 
		'return_date' => $_POST['return_date'], 
	)); 
	if($save_quote_data){
		echo $random_id; 
	}else{
		echo 0;
	}  
	//echo $random_id; 
	//echo json_encode(['result'=> 'Values saved', 'status' => 1]);
	wp_die();	
}
add_action('wp_ajax_generate_qoute', 'generate_qoute');
add_action('wp_ajax_nopriv_generate_qoute', 'generate_qoute');


function reorder_array(&$array, $new_order) {
	$inverted = array_flip($new_order);
	uksort($array, function($a, $b) use ($inverted) {
	  return $inverted[$a] > $inverted[$b];
	});
  }

// Function for get products questionnaire
function get_questionnaire(){ 	
	$prod_id = $_POST['product_id'];
	$fields = get_fields($prod_id); 
	$dob_2 = $_POST['dob_2'];
	//echo '<pre>';print_r($fields);
	 if($prod_id == 46){
		reorder_array($fields, array('coronary_artery_disease_heart_attack_or_angina', 'valvular_heart_disease_abnormal_heartbeat_arrhythmia_or_use_of_a_pacemaker', 'a_lung_or_respiratory_condition_for_which_daily_medication_has_been_prescribed_including_inhalers', 'diabetes_that_requires_insulin',' stroke_or_mini-stroke_tia','aneurysm','blood_clots','gastro-intestinal_bleed','were_you_admitted_to_hospital_for_this_condition_in_the_2_yearsbefore_your_departure_date','do_you_have_a_terminal_illness','has_a_doctor_advised_younot_to_travel','do_you_have_metastatic_cancer_','have_you_lived_in_a_retirement_home_nursing_home_assisted_living_home_convalescent_homehospice_or_rehabilitation_centrethatassists_you_daily_with_your_mobility_or_medications_',' have_you_had_chemotherapy_radiation_therapy_or_any_surgery_for_cancer_excluding_theremoval_of_skin_lesions_other_than_malignant_melanoma','in_the_2_yearsbefore_your_departure_date_were_you_diagnosed_or_treated_forcongestive_heart_failure','in_the_2_yearsbefore_your_departure_date_were_you_diagnosed_or_treated_forcongestive_kidney_failure_requiring_dialysis','in_the_2_yearsbefore_your_departure_date_were_you_diagnosed_or_treated_forcongestive_alung_condition_requiringhome_oxygen'));
	 }elseif($prod_id == 96){ 
		reorder_array($fields, array('tugo_category_1', 'tugo_category_2', ' tugo_category_3', 'tugo_category_4')); 
	}elseif($prod_id == 92){
		reorder_array($fields, array('condition1','condition2','condition3','condition4','condition5','condition6','condition7','condition8','condition9','condition10','condition11','condition12','condition13','condition14','condition15','condition16','condition17','condition18','condition19','condition20')); 

	}
 
	$updated_array = array();
	foreach( $fields as $name => $value ){
		$updated_array[] = $name;
	}

	// echo '<pre>';print_r($updated_array);

	if(!empty($fields)){		 
		if($dob_2 == 'undefined'){
			if($prod_id == 96){


				// New code comes here
				$response = '<div class="tugo-questions">
				<input type="hidden" class="productid" value='.$prod_id.' />
				<div class="tugo-quest-row">
					<p>At the time of application, how many medications in total do you take or have you been ordered to take by a physician, to treat one or more of the following medical conditions:</p>
					<ul>
						<li>Lung conditions/disease (include asthma)</li>
						<li>Diabetes</li>
						<li>Heart conditions/disease (do not include aspirin, hypertension (high blood pressure) or high cholesterol medications)</li>
					</ul>
					<div class="tugo-quest-options">
						<label><input type="radio" name="medication" value="3">3 or more medical conditions</label>
						<label><input type="radio" name="medication" value="2">2 or more medical conditions</label>
						<label><input type="radio" name="medication" value="1">1 medical condition</label>
						<label><input type="radio" name="medication" value="0">None</label>
					</div>
				</div>
				<div class="tugo-quest-row hide">
					<p>Within the 24 months prior to the date of application, have you had a heart attack, stroke and/or transient ischemic attack (mini-stroke, TIA)?</p>
					<div class="tugo-quest-options">
						<label><input type="radio" name="heart-attack" value="yes">Yes</label>
						<label><input type="radio" name="heart-attack" value="no">No</label>
					</div>
				</div>
				<div class="tugo-quest-row hide">
					<p>At the time of application, how many of the following medical
					conditions are you receiving treatment for?: • Lung conditions/disease (include asthma) • Diabetes • Heart conditions/disease (do not include aspirin, hypertension (high blood pressure) or high cholesterol medications)</p>
					<div class="row">
						<div class="col-md-6">
							<ul>
								<li>Heart conditions/disease (include aspirin)</li>
								<li>Lung conditions/disease (include asthma)</li>
								<li>Diabetes (controlled by medication or diet)</li>
								<li>Hypertension</li>
								<li>Diverticulitis</li>
								<li>Bowel obstruction</li>
							</ul>
						</div>
						<div class="col-md-6">
							<ul>
								<li>Peptic ulcer</li>
								<li>GERD (gastro-esophageal reflux disease)</li>
								<li>Kidney infections</li>
								<li>Kidney stones</li>
								<li>Kidney failure</li>
								<li>Cancer</li>
							</ul>
						</div>
					</div>
					<div class="tugo-quest-options">
						<label><input type="radio" name="medical-condition" value="2">2 or more medical conditions</label>
						<label><input type="radio" name="medical-condition" value="1">1 medical condition</label>
						<label><input type="radio" name="medical-condition" value="0">None</label>
					</div>
				</div>
				<div class="tugo-quest-row hide">
					<p>At the time of application, do you have any medical conditions that were not listed in the previous questions for which you are currently receiving treatment? Treatment includes medication that you take or have been ordered to take by a physician, not including a minor ailment.</p>
					<div class="tugo-quest-options">
						<label><input type="radio" name="other-medical-condition" value="yes">Yes</label>
						<label><input type="radio" name="other-medical-condition" value="no">No</label>
					</div>
				</div>
				<div class="tugo-quest-row hide">
					<p>Have you used any tobacco products in the past 12 months?</p>
					<div class="tugo-quest-options">
						<label><input type="radio" name="tobacco" value="yes">Yes</label>
						<label><input type="radio" name="tobacco" value="no">No</label>
					</div>
				</div>
				<div class="alert alert-success hide">You are eligible for Rate Category <span class="category-type"></span></div></div>';



				// New code ends here
				// $response = "<table border='0' width='100%' class='table table_radio single_app travelence-".$prod_id."'>";
				// $response .= "<input type='hidden' class='productid' value=".$prod_id." />";
				// foreach( $updated_array as $name => $value ){
				// 	$objects = get_field_object($value, $prod_id); 
				// 	if(empty($objects['default_value'])){ $def_val = 0;}else{$def_val = $objects['default_value']; }
				// 	$response .= "<tr>";
				// 	$response .= "<td>".$objects['label']."</td>";
				// 	$response .= "<td><input name=".$value." id='chkbx' data-vals=".$def_val." type='radio' class='custom-control-input pre-existing' value='yes'> 
				// 	<label for='chkbx' class='custom-control-label'>Yes</label>";
				// 	$response .= "<input name=".$value." id='chkbx' data-vals='0' type='radio' class='custom-control-input pre-existing' value='no'> 
				// 	<label for='chkbx' class='custom-control-label'>No</label></td>";
				// 	$response .= "</tr>";
				// }
				// $response .= "</table>";
				echo $response;


			}else{ 
				$response = "<table border='0' width='100%' class='table table_radio single_app travelence-".$prod_id."'>";
				$response .= "<input type='hidden' class='productid' value=".$prod_id." />";
				foreach( $updated_array as $name => $value ){
					$objects = get_field_object($value, $prod_id); 
					if(empty($objects['default_value'])){ $def_val = 0;}else{$def_val = $objects['default_value']; }
					$response .= "<tr>";
					$response .= "<td>".$objects['label']."</td>";
					$response .= "<td><input name=".$value." id='chkbx' data-vals=".$def_val." type='radio' class='custom-control-input pre-existing' value='yes'> 
					<label for='chkbx' class='custom-control-label'>Yes</label>";
					$response .= "<input name=".$value." id='chkbx' data-vals='0' type='radio' class='custom-control-input pre-existing' value='no'> 
					<label for='chkbx' class='custom-control-label'>No</label></td>";
					$response .= "</tr>";
				}
				$response .= "</table>";
				echo $response;
			}
		}else{
			if($prod_id == 96){
				$response = '<div class="tugo-questions">
				<input type="hidden" class="productid" value='.$prod_id.' />
				<div class="tugo-quest-row">
					<p>At the time of application, how many medications in total do you take or have you been ordered to take by a physician, to treat one or more of the following medical conditions:</p>
					<ul>
						<li>Lung conditions/disease (include asthma)</li>
						<li>Diabetes</li>
						<li>Heart conditions/disease (do not include aspirin, hypertension (high blood pressure) or high cholesterol medications)</li>
					</ul>
					<div class="tugo-quest-options">
						<p>Applicant 1</p>
						<label><input type="radio" name="medication1" value="3">3 or more medical conditions</label>
						<label><input type="radio" name="medication1" value="2">2 or more medical conditions</label>
						<label><input type="radio" name="medication1" value="1">1 medical condition</label>
						<label><input type="radio" name="medication1" value="0">None</label>
					</div>
					<div class="tugo-quest-options">
						<p>Applicant 2</p>
						<label><input type="radio" name="medication2" value="3">3 or more medical conditions</label>
						<label><input type="radio" name="medication2" value="2">2 or more medical conditions</label>
						<label><input type="radio" name="medication2" value="1">1 medical condition</label>
						<label><input type="radio" name="medication2" value="0">None</label>
					</div>
				</div>
				<div class="tugo-quest-row hide">
					<p>Within the 24 months prior to the date of application, have you had a heart attack, stroke and/or transient ischemic attack (mini-stroke, TIA)?</p>
					<div class="tugo-quest-options">
						<p>Applicant 1</p>
						<label><input type="radio" name="heart-attack1" value="yes">Yes</label>
						<label><input type="radio" name="heart-attack1" value="no">No</label>
						
					</div>
					<div class="tugo-quest-options">
						<p>Applicant 2</p>
						<label><input type="radio" name="heart-attack2" value="yes">Yes</label>
						<label><input type="radio" name="heart-attack2" value="no">No</label>
					</div>
				</div>
				<div class="tugo-quest-row hide">
					<p>At the time of application, how many of the following medical
					conditions are you receiving treatment for?: • Lung conditions/disease (include asthma) • Diabetes • Heart conditions/disease (do not include aspirin, hypertension (high blood pressure) or high cholesterol medications)</p>
					<div class="row">
						<div class="col-md-6">
							<ul>
								<li>Heart conditions/disease (include aspirin)</li>
								<li>Lung conditions/disease (include asthma)</li>
								<li>Diabetes (controlled by medication or diet)</li>
								<li>Hypertension</li>
								<li>Diverticulitis</li>
								<li>Bowel obstruction</li>
							</ul>
						</div>
						<div class="col-md-6">
							<ul>
								<li>Peptic ulcer</li>
								<li>GERD (gastro-esophageal reflux disease)</li>
								<li>Kidney infections</li>
								<li>Kidney stones</li>
								<li>Kidney failure</li>
								<li>Cancer</li>
							</ul>
						</div>
					</div>
					<div class="tugo-quest-options">
						<p>Applicant 1</p>
						<label><input type="radio" name="medical-condition1" value="2">2 or more medical conditions</label>
						<label><input type="radio" name="medical-condition1" value="1">1 medical condition</label>
						<label><input type="radio" name="medical-condition1" value="0">None</label>
					</div>
					<div class="tugo-quest-options">
						<p>Applicant 2</p>
						<label><input type="radio" name="medical-condition2" value="2">2 or more medical conditions</label>
						<label><input type="radio" name="medical-condition2" value="1">1 medical condition</label>
						<label><input type="radio" name="medical-condition2" value="0">None</label>
					</div>
				</div>
				<div class="tugo-quest-row hide">
					<p>At the time of application, do you have any medical conditions that were not listed in the previous questions for which you are currently receiving treatment? Treatment includes medication that you take or have been ordered to take by a physician, not including a minor ailment.</p>
					<div class="tugo-quest-options">
						<p>Applicant 1</p>
						<label><input type="radio" name="other-medical-condition1" value="yes">Yes</label>
						<label><input type="radio" name="other-medical-condition1" value="no">No</label>
					</div>
					<div class="tugo-quest-options">
						<p>Applicant 2</p>
						<label><input type="radio" name="other-medical-condition2" value="yes">Yes</label>
						<label><input type="radio" name="other-medical-condition2" value="no">No</label>
					</div>
				</div>
				<div class="tugo-quest-row hide">
					<p>Have you used any tobacco products in the past 12 months?</p>
					<div class="tugo-quest-options">
						<p>Applicant 1</p>
						<label><input type="radio" name="tobacco1" value="yes">Yes</label>
						<label><input type="radio" name="tobacco1" value="no">No</label>
					</div>
					<div class="tugo-quest-options">
						<p>Applicant 2</p>
						<label><input type="radio" name="tobacco2" value="yes">Yes</label>
						<label><input type="radio" name="tobacco2" value="no">No</label>
					</div>
				</div>
				<div class="alert alert-success hide">You are eligible for Rate Category <span class="category-type"></span></div></div>';
			}
			// $response .= "<table border='0' width='100%' class='table'>";
			// $response .= "<input type='hidden' class='productid' value=".$prod_id." />";
			// $response .= "<tr>";
			// $response .= "<td style='border:0px !important;'></td>";
			// $response .= "<td style='width: 115px !important; border:0px !important;'><label for='chkbx' class='custom-control-label'>Applicant 1</label></td>";
			// $response .= "<td style='width: 125px !important; border:0px !important;'><label for='chkbx' class='custom-control-label'>Applicant 2</label></td>";
			// $response .= "</tr>";
			// $response .= "</table>";
			else {
				$response .= "<input type='hidden' class='productid' value=".$prod_id." />";
				$response .= "<table border='0' width='100%' class='table table_radio travelence-".$prod_id."'>";
				$response .= "<thead><tr>";
				$response .= "<th style='border:0px !important;'></th>";
				$response .= "<th style='width: 115px !important; border:0px !important;'><label for='chkbx' class='custom-control-label'>Applicant 1</label></th>";
				$response .= "<th style='width: 125px !important; border:0px !important;'><label for='chkbx' class='custom-control-label'>Applicant 2</label></th>";
				$response .= "</tr></thead>";
				foreach( $updated_array as $name => $value ){
					$objects = get_field_object($value, $prod_id);
					$response .= "<tr>";
					$response .= "<td>".$objects['label']."</td>";
					$response .= "<td><input name=1_".$value." id='chkbx' type='radio' class='custom-control-input pre-existing-1' value='yes'> 
					<label for='chkbx' class='custom-control-label'>Yes</label>";
					$response .= "<input name=1_".$value." id='chkbx' type='radio' class='custom-control-input pre-existing-1' value='no'> 
					<label for='chkbx' class='custom-control-label'>No</label></td>";
					$response .= "<td><input name=2_".$value." id='chkbx' type='radio' class='custom-control-input pre-existing-2' value='yes'> 
					<label for='chkbx' class='custom-control-label'>Yes</label>";
					$response .= "<input name=2_".$value." id='chkbx' type='radio' class='custom-control-input pre-existing-2' value='no'> 
					<label for='chkbx' class='custom-control-label'>No</label></td>";
					$response .= "</tr>";
				}
				$response .= "</table>";
			}
			echo $response; 
		}
	}else{
		echo 0;
	}

	wp_die();
}
add_action('wp_ajax_get_questionnaire', 'get_questionnaire');
add_action('wp_ajax_nopriv_get_questionnaire', 'get_questionnaire');

/**
 * functiom for questionaire updates
 */
function update_questionaire(){

}
add_action('wp_ajax_update_questionaire', 'update_questionaire');
add_action('wp_ajax_nopriv_update_questionaire', 'update_questionaire');


/**
 * Function for add rate card for travel insurance
 */
function add_rate_travel_insurance(){
	global $wpdb;
	$pieces = explode("-", $_POST['age_range']);
	$tugo_trip_category = $_POST['category_tugo_trip_cat'];
	if($_POST['trip_cancel_sum_assured'] != 0){
		$add_rate_travel_insurance = explode("-", $_POST['trip_cancel_sum_assured']);
		$sum_assured_1 = $add_rate_travel_insurance[0];
		$sum_assured_2 = $add_rate_travel_insurance[1];
	}else{
		$sum_assured_1 = 0;
		$sum_assured_2 = 0;
	}
	
	if(substr_count($_POST['travel_days'], '-') != 0){
		$piece = explode("-", $_POST['travel_days']);
		$trav_in = $piece[0];
		$trav_out = $piece[1];
		$travel_days = 0;
	}else{
		$trav_in = 0;
		$trav_out = 0;
		$travel_days = $_POST['travel_days'];
	}

	if(!empty($_POST['sub_category_tugo'])){
		$sub_category = $_POST['sub_category_tugo'];
		$destination = $_POST['trip_cat'];
	}else{
		$sub_category = $_POST['trip_cat'];
		$destination = '';
	}
	$save_inq_data = $wpdb->insert('crm_rate_card', array(
		'p_id' => $_POST['product'],
		'trip_type' => $_POST['trip_type'],
		'sub_category' => $sub_category,
		'age_in' => $pieces[0],  
		'age_bound' => $pieces[1],
		'travel_days' => $travel_days,
		'travel_days_in' => $trav_in,
		'travel_days_bound' => $trav_out,
		'amount' => $_POST['amount'],
		'sum_assured' => $_POST['sum_assured'],
		'tugo_sum_assured_1' => $sum_assured_1,
		'tugo_sum_assured_2' => $sum_assured_2,
		'tugo_trip_category' => $tugo_trip_category,
		'destination' => $destination,
		'status' => 1 
	));

	if($save_inq_data){  
		echo 1;
	}else{
		echo 0;	} 
 
	wp_die();
}
add_action('wp_ajax_add_rate_travel_insurance', 'add_rate_travel_insurance');
add_action('wp_ajax_nopriv_add_rate_travel_insurance', 'add_rate_travel_insurance');

/**
 * Function for update trip
 */
function update_redirect(){
	echo 1;
	wp_die();
}
add_action('wp_ajax_update_redirect', 'update_redirect');
add_action('wp_ajax_nopriv_update_redirect', 'update_redirect');

/**
 * Function for deleting a travel card
 */
function delete_travel_rate_card(){
	global $wpdb;
	$id = $_POST['item_id'];
	$table = 'crm_rate_card';
	$delete_action = $wpdb->delete( $table, array( 'id' => $id ) );
	if($delete_action){
		echo 1;
	}else{
		echo 0;
	}
	wp_die();
}
add_action('wp_ajax_delete_travel_rate_card', 'delete_travel_rate_card');
add_action('wp_ajax_nopriv_delete_travel_rate_card', 'delete_travel_rate_card');

/**
 * Function for handling final quote generation process
 */
function process_final_quote(){
	global $wpdb;
	$quote_id = $_POST['quote_id'];
	$data = $_POST['raw_data'];
	$applicant = $_POST['applicant'];
	$no_travelers = $_POST['no_travelers'];
	$raw_amount_data = $_POST['raw_amount_data'];
	$json_decoded_data = json_decode($data);
	$formatted = [];
	$second_format = [];
	$user_data =  $wpdb->get_row("SELECT * FROM crm_quotes WHERE quote_id='$quote_id' ");
	$user_id = $user_data->customer_id;
	$departure_date = $user_data->departure_date;
	$return_date = $user_data->return_date;
	$getuser =  $wpdb->get_row("SELECT * FROM crm_customers WHERE id=$user_id");
	$customer_name = $getuser->first_name.' '.$getuser->last_name;
	if($applicant == 1){
	// Save customers questionnaire data
		foreach($data as $key=>$value){
			$formatted[$key]['p_id'] = $value['pid']; 
			$second_format = explode('&', $value['data']);  
			foreach($second_format as $keys=>$val){
				$explode_2 = explode("=", $val);
				$final_array[$key][$keys]['item'] = $explode_2[0];
				$final_array[$key][$keys]['value'] = $explode_2[1]; 
				// insert the questionnaire data to the db_id
				if(!empty($explode_2[0])){
					$wpdb->insert('crm_quotes_items', array(
						'quote_id' => $quote_id,
						'product_id' => $value['pid'],
						'question' => $explode_2[0],  
						'answer' => $explode_2[1],
						'applicant' => 'applicant1',
						'status' => 1 
					)); 
				}
			}
		} 
		// Save cusomters questionnaire amount data
		foreach($raw_amount_data as $amount){
			$prod_id = $amount['p_id'];
			foreach($amount['pr_amount'] as $keys=>$val){
				$wpdb->insert('crm_quotes_amounts', array(
					'quote_id' => $quote_id,
					'rate_id' => $val['rate_id'],
					'amount' => $val['amount'],
					'status' => 1
				));  
			} 
		}

	}else{

		foreach($data as $key=>$value){
			$formatted[$key]['p_id'] = $value['pid']; 
			$second_format = explode('&', $value['data1']);  
			foreach($second_format as $keys=>$val){
				$explode_2 = explode("=", $val);
				$final_array[$key][$keys]['item'] = $explode_2[0];
				$final_array[$key][$keys]['value'] = $explode_2[1]; 
				// insert the questionnaire data to the db_id
				if(!empty($explode_2[0])){
					$wpdb->insert('crm_quotes_items', array(
						'quote_id' => $quote_id,
						'product_id' => $value['pid'],
						'question' => $explode_2[0],  
						'answer' => $explode_2[1],
						'applicant' => 'applicant1',
						'status' => 1 
					)); 
				}
			}
		}

		foreach($data as $key=>$value){
			$formatted[$key]['p_id'] = $value['pid']; 
			$second_format = explode('&', $value['data2']);  
			foreach($second_format as $keys=>$val){
				$explode_2 = explode("=", $val);
				$final_array[$key][$keys]['item'] = $explode_2[0];
				$final_array[$key][$keys]['value'] = $explode_2[1]; 
				// insert the questionnaire data to the db_id
				if(!empty($explode_2[0])){
					$wpdb->insert('crm_quotes_items', array(
						'quote_id' => $quote_id,
						'product_id' => $value['pid'],
						'question' => $explode_2[0],  
						'answer' => $explode_2[1],
						'applicant' => 'applicant2',
						'status' => 1 
					)); 
				}
			}
		}

		// Save cusomters questionnaire amount data
		foreach($raw_amount_data as $amount){
			$prod_id = $amount['p_id'];
			foreach($amount['pr_amount'] as $keys=>$val){
				$wpdb->insert('crm_quotes_amounts', array(
					'quote_id' => $quote_id,
					'rate_id' => $val['rate_id'],
					'amount' => $val['amount'],
					'status' => 1
				));  
			} 
		}

	}
	
	foreach ($raw_amount_data as $amt){
		$amount = [];
		$prod_id = $amt['p_id'];
		$post = $wpdb->get_row("SELECT * FROM wp_lci_posts WHERE ID=$prod_id"); 
		foreach($amt['pr_amount'] as $keys=>$val){
			$amount[] = $val['amount'];
		}  
		$post_array[] = array('title' => $post->post_title, 'content' => $post->post_content, 'amount' => $amount);
	}

	/*foreach($data as $key=>$value){
		$pid = $value['pid'];
		$post = $wpdb->get_row("SELECT * FROM wp_lci_posts WHERE ID=$pid");
		$post_array[] = array('title' => $post->post_title, 'content' => $post->post_content);
	}*/
	// Send email  to the customer and admin

	  // Instantiation and passing `true` enables exceptions
	$mail = new PHPMailer(true);
	try {
		//Server settings
		//$mail->SMTPDebug = SMTP::DEBUG_SERVER;    
		/*$mail->isSMTP();                              
		$mail->Host       = 's132-148-151-204.secureserver.net';                
		$mail->SMTPAuth   = false;                             
		$mail->Username   = '';  // User name
		$mail->Password   = '';  //  Password
		$mail->SMTPSecure = "none";   
		$mail->Port       = 25; */

		
		$mail->isSMTP();                              
		$mail->Host       = 'smtp.gmail.com';                
		$mail->SMTPAuth   = true;                             
		$mail->Username   = 'ideas.to.jp@gmail.com';  // Gmail id
		$mail->Password   = '^Commradejp:gmail1';  // Gmail Password
		$mail->SMTPSecure = "tls";   
		$mail->Port       = 587;  

		//Recipients
		$mail->setFrom('no-reply@example.com', 'Mail from Lifecareinsurance');
		$mail->addAddress('ideas.to.jp@gmail.com', $customer_name);  

		/*
		$mail->isSMTP();
		$mail->Host = 'localhost';
		$mail->SMTPAuth = false;
		$mail->SMTPAutoTLS = false; 
		$mail->Port = 25; */

		//Recipients
		//$mail->setFrom('emaildelivery@lifecareinsurance.ca', 'Mail from Lifecareinsurance');
		//$mail->addAddress($getuser->email, $customer_name);  

		// Content
		$mail->isHTML(true);                                 
		$mail->Subject = 'Lifecareinsurance'; 
		$body = '<!DOCTYPE html>
				<html>
				<head>
				<meta name="viewport" content="width=device-width" />
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<title>Lifecareinsurance</title>
				</head>
				<body>
					<table width="650" style="background-color: #f6f6f6;margin: 0 auto;line-height:24px;border-radius:6px;">
						<thead>
							<tr>
								<th style="padding: 20px 0;"><div class="logo"><img width="300" src="https://lifecareinsurance.ca/wp-content/uploads/2019/07/lifecare-logo-new-site.png" alt="Lifecareinsurance"></div></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="padding: 10px 20px;">
									<table style="background-color: #ffffff;border-radius:6px;">
										<tbody>
											<tr>
												<td style="padding: 10px 20px;"><strong>Hi '.$customer_name.',</strong></td>
											</tr>
											<tr>
												<td style="padding: 10px 20px;">Thank you for your interest on the Lifecareinsurance. Here, you can find out the requested Quote generated according to the given data. Our representative will contact you very soon for further proceedings.</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="text-align: right;padding: 10px 20px;">
									<span style="color: #2c408f;font-weight: bold;font-size: 18px;">Quote #'.$quote_id.'</span> 
								</td>
							</tr>
							<tr>
								<td style="text-align: right;padding: 10px 20px;">
									<span style="color: #999;font-weight: bold;font-size: 13px;">Departure Date :'.$departure_date.'</span><br>
									<span style="color: #999;font-weight: bold;font-size: 13px;">Return Date :'.$return_date.'</span> 
								</td>
							</tr> 
							'; 
								foreach($post_array as $p) {  
								$body .= '<tr>
									<td style="padding: 10px 20px;">
										<table style="background-color: #fff;border-radius:6px;">
											<tbody>
												<tr>
													<td style="padding: 10px 20px; color: #2c408f;font-size: 20px;font-weight: bold;">'.$p["title"].'</td>
												</tr>
												<tr>
													<td style="padding: 10px 20px;">'.$p['content'].'</td>
												</tr>
												<tr>
													<td style="padding: 10px 20px;">Number of Applicants:'.$no_travelers.'</td>
												</tr>
												<tr>
													<td style="padding: 10px 20px;">Applicable Rates:';
													foreach($p['amount'] as $amount){
														$body .= '<span style="display: inline-block;vertical-align: middle;color: #ffffff;background-color: #2c408f;border-radius: 2px;
														padding: 0 6px;margin-right: 5px;">C$'.$amount * $no_travelers.'</span>';
													}
								$body .= '</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>'; 
								}  
				$body .='<tr>
				<td style="padding: 10px 20px;">
					<table style="width:100%;background-color: #fff;">
						<tbody>
							<tr>
								<td style="padding: 10px 20px; text-align: center;">
									<p>Powered by <a href="https://lifecareinsurance.ca/">Lifecareinsurance</a></p>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			</tbody>
			</table>
			</body>
			</html>';
 
		  $mail->Body = $body; 
		  $mail->send();
		  //echo 'Message has been sent';
	  } catch (Exception $e) {
		  echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	  }

	$characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < 5; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	$referral_code = 'TRVREF'.$randomString; 

	// save the data to database
	$wpdb->insert('crm_referal_activities', array(
		'referal_id' => 1,
		'customer_id' => $user_id,
		'quote_id' => $quote_id,
		'referal_code' => $referral_code,
		'status' => 1
	));

	// Send Referral code to the customer via email
	try {
		//Server settings
		//$mail->SMTPDebug = SMTP::DEBUG_SERVER;    
		/*$mail->isSMTP();                              
		$mail->Host       = 's132-148-151-204.secureserver.net';                
		$mail->SMTPAuth   = false;                             
		$mail->Username   = '';  // User name
		$mail->Password   = '';  //  Password
		$mail->SMTPSecure = "none";   
		$mail->Port       = 25;  
		$mail->isSMTP();
		$mail->Host = 'localhost';
		$mail->SMTPAuth = false;
		$mail->SMTPAutoTLS = false; 
		$mail->Port = 25;  */

		//Recipients
		//$mail->setFrom('emaildelivery@lifecareinsurance.ca', 'Mail from Lifecareinsurance');
		//$mail->addAddress($getuser->email, $customer_name);   


		$mail->isSMTP();                              
		$mail->Host       = 'smtp.gmail.com';                
		$mail->SMTPAuth   = true;                             
		$mail->Username   = 'ideas.to.jp@gmail.com';  // Gmail id
		$mail->Password   = '^Commradejp:gmail1';  // Gmail Password
		$mail->SMTPSecure = "tls";   
		$mail->Port       = 587;  

		//Recipients
		$mail->setFrom('no-reply@example.com', 'Mail from Lifecareinsurance');
		$mail->addAddress('ideas.to.jp@gmail.com', $customer_name); 

		// Content
		$mail->isHTML(true);                                 
		$mail->Subject = 'Lifecareinsurance - Referal code'; 
		$body = '<!DOCTYPE html>
				<html>
				<head>
				<meta name="viewport" content="width=device-width" />
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<title>Lifecareinsurance</title>
				</head>
				<body>
					<table width="650" style="background-color: #f6f6f6;margin: 0 auto;line-height:24px;border-radius:6px;">
						<thead>
							<tr>
								<th style="padding: 20px 0;"><div class="logo"><img width="300" src="https://lifecareinsurance.ca/wp-content/uploads/2019/07/lifecare-logo-new-site.png" alt="Lifecareinsurance"></div></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="padding: 10px 20px;">
									<table style="background-color: #ffffff;border-radius:6px;">
										<tbody>
											<tr>
												<td style="padding: 10px 20px;"><strong>Hi '.$customer_name.',</strong></td>
											</tr>
											<tr>
												<td style="padding: 10px 20px;">Thank you for your interest on the Lifecareinsurance. Please find the attached Referral code with this email.</td>
											</tr>
											<tr><td style="padding: 10px 20px;"><b>You can redeem this referral code for getting 10% discount on Tim Hortons outlets.</b></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="text-align: center;padding: 10px 20px;">
									<span style="color: #2c408f;font-size: 18px;">REFERRAL CODE : <br><br> 
									</span> 
									<span style="color: green;font-weight: bold;font-size: 18px;">'.$referral_code.'</span>
								</td>
							</tr>';
				$body .='<tr>
				<td style="padding: 10px 20px;">
					<table style="width:100%;background-color: #fff;">
						<tbody>
							<tr>
								<td style="padding: 10px 20px; text-align: center;">
									<p>Powered by <a href="https://lifecareinsurance.ca/">Lifecareinsurance</a></p>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			</tbody>
			</table>
			</body>
			</html>';
 
		  $mail->Body = $body; 
		  $mail->send();
		  //echo 'Message has been sent';
	  } catch (Exception $e) {
		  echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	  }

	


	echo json_encode(['result'=> 'Values saved', 'status' => 1]);
	wp_die();

}
add_action('wp_ajax_process_final_quote', 'process_final_quote');
add_action('wp_ajax_nopriv_process_final_quote', 'process_final_quote');


// define the acf_the_content callback 
function filter_acf_the_content( $value ) { 
    // make filter magic happen here... 
    return $value; 
}; 
         
// add the filter 
add_filter( 'acf_the_content', 'filter_acf_the_content', 111); 