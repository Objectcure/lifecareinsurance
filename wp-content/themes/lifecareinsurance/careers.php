<?php
 
 /* Template Name: Careers */ 
 
 get_header();
 ?>

<div class="section careers-section">
    <div class="container">
        <div class="career-left">
            <div class="career-image">
                <img src="<?php bloginfo('template_url'); ?>/assets/images/careerbg1.jpg" alt="">
            </div>
            <div class="left-content-top">
                <h3 class="text-pprimary">LIFE CARE: WHERE PASSION MEETS SUCCESS</h3>
                <p>We are your elevator to success. Join the wining team of Life Care because we care for your dreams </p>
            </div>
            <div class="left-content-bottom bg-primary">
                <p>Visit: <a href="#">https://lifecareinsurance.ca/contact/</a></p>
                <div class="side-text">
                    <h5>It is not just about building business, it’s about building legacy</h5>
                    <h5>A strong Financial foundation is necessary to build a strong future</h5>
                </div>
            </div>
            <div class="career-image">
                <img src="<?php bloginfo('template_url'); ?>/assets/images/careerbg2.jpg" alt="">
            </div>
        </div>
        <div class="career-right">
            <div class="bg-primary">
                <div class="right-content">
                    <div class="arrow-image">
                        <img src="<?php bloginfo('template_url'); ?>/assets/images/arrow.png" alt="">
                    </div>
                    <p>Do you have the driving force to <span>succeed in life?</span></p>
                    <p>Are you looking for a career to <span>create your own brand?</span></p>
                    <p>Do you know what you should do? <span>talk to us.</span></p>
                    <p>At Life Care <span>we aim high and big</span> -  and we like people who can aim high
                        with us. Join us on this journey and you will never look back. <span>We are your
                        elevator to success.</span></p>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Our Vision</h5>
                            <ul>
                                <li>Leader in managing Risk and Money for our clients with a high level of customer satisfaction.</li>
                                <li>Translate individual success to Financial center success by integrating the highly motivated team through skill development, education, training, and succession planning.</li>
                            </ul>
                            <h5>Management</h5>
                            <ul>
                                <li>Our office will bring unity in diversity. We will provide a fair and ethical working environment for everybody to succeed and create their own mark in the career.</li>
                            </ul>
                            <p><em>The Core management team follows the principles of :</em></p>
                            <ul>
                                <li>Fairness</li>
                                <li>Ethics</li>
                                <li>Excellence</li>
                                <li>Team Work</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h5>Our Mission</h5>
                            <p><em>Customer Service</em></p>
                            <ul>
                                <li>Providing excellent service to the community by securing the life of individuals; to protect the dependents, family members in case of death, disability and sickness.</li>
                            </ul>
                            <p><em>Sales Force</em></p>
                            <ul>
                                <li>Always hit the highest chord.</li>
                                <li>Our mission is to train our advisors on product planning, need analysis; financial planning and estate to provide customized solutions for our clients. Achieving and exceeding sales targets and setting examples for others to follow.</li>
                                <li>Our Sales Managers will work with each individual to establish career goals, joint field work, monitoring, compliance, excellent customer service.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-content-bottom">
                <ul>
                    <li>
                        <div class="list-icon"><img src="<?php bloginfo('template_url'); ?>/assets/images/bag.png" alt=""></div>
                        <p><span>You own your book of business </span>from day one. You receive all renewals and also have an opportunity to sell at retirement. We help you to buy more book of business in support of a growth-acquisition strategy. Get up to 7x for your block of business</p>
                    </li>
                    <li>
                        <div class="list-icon"><img src="<?php bloginfo('template_url'); ?>/assets/images/chair.png" alt=""></div>
                        <p><span>Build your own agency </span>and we will help your agents in Training, Compliance so that you focus on building more business.</p>
                    </li>
                    <li>
                        <div class="list-icon"><img src="<?php bloginfo('template_url'); ?>/assets/images/pen-paper.png" alt=""></div>
                        <p><span>A complete training program </span>has been designed to help you continue upgrading your skills and maintain your license of practice. Access to all major insurance companies and competitive compensation model.</p>
                    </li>
                    <li>
                        <div class="list-icon"><img src="<?php bloginfo('template_url'); ?>/assets/images/bulb.png" alt=""></div>
                        <p><span>We have in-house team who will create and execute marketing programs </span>by customizing sales materials – print & web advertisements, invitations, event posters, informational flyers, and cards for special occasions and social media marketing and also web development.</p>
                    </li>
                    <li>
                        <div class="list-icon"><img src="<?php bloginfo('template_url'); ?>/assets/images/money.png" alt=""></div>
                        <p><span>Sell your block of business</span>………………….If you want to sell your block Life Care will pay the highest price and or get your residuals for lifetime. More opportunities available.</p>
                    </li>
                </ul>
            </div>
            <div class="bottom-text">
                <h5>Guaranteed contractual buyout</h5>
                <p>Life care offers to buy <span>your book at 7 times multiple </span>and more</p>
                <p>And/or</p>
                <p>All your <span>written business</span> offers you <span>residual income </span>for life</p>
                <p>Be a successful brand with us and make a difference in yours and client’s life</p>
                <p class="text-right"><small><em>*Condition apply</em></small></p>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<?php
get_footer(); 