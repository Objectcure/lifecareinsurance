<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>
<!-- BANNER -->
<div id="slides" class="section banner">
		<ul class="slides-container">
			<li>
				<img src="<?php bloginfo('template_url'); ?>/assets/images/Family_planing.jpg" alt="">
				<div class="container">
					<div class="wrap-caption">
						<h2 class="caption-heading">
							Protect Your Family with Insurance
						</h2>
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>	
						<a href="#" class="btn btn-primary" title="LEARN MORE">LEARN MORE</a> <a href="#" class="btn btn-secondary" title="CONTACT US">CONTACT US</a>
					</div>
				</div>
			</li>
			<li>
				<img src="<?php bloginfo('template_url'); ?>/assets/images/Travel_insurance.jpg" alt="">
				<div class="container">
					<div class="wrap-caption">
						<h2 class="caption-heading">
							Quality Protection &amp; Easy to Claims
						</h2>
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>	
						<a href="#" class="btn btn-primary" title="LEARN MORE">LEARN MORE</a> <a href="#" class="btn btn-secondary" title="CONTACT US">CONTACT US</a>
					</div>
				</div>
			</li>
			<li>
				<img src="<?php bloginfo('template_url'); ?>/assets/images/Financial_planing.jpg" alt="">
				<div class="container">
					<div class="wrap-caption">
						<h2 class="caption-heading">
							Get An Insurance for Your Travel
						</h2>
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>	
						<a href="#" class="btn btn-primary" title="LEARN MORE">LEARN MORE</a> <a href="#" class="btn btn-secondary" title="CONTACT US">CONTACT US</a>
					</div>
				</div>
			</li>
			
		</ul>

		<nav class="slides-navigation">
			<div class="container">
				<a href="#" class="next">
					<i class="fa fa-angle-right"></i>
				</a>
				<a href="#" class="prev">
					<i class="fa fa-angle-left"></i>
				</a>
	      	</div>
	    </nav>
		
	</div>

	<!-- ABOUT FEATURE -->
	<div class="section feature section-border">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-4 col-md-4">
					<!-- BOX 1 -->
					<div class="box-icon-2">
						<div class="icon">
							<div class="fa fa-globe"></div>
						</div>
						<div class="body-content">
							<div class="heading">The Best in Asia</div>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 2 -->
					<div class="box-icon-2">
						<div class="icon">
							<div class="fa fa-umbrella"></div>
						</div>
						<div class="body-content">
							<div class="heading">Best Support</div>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 3 -->
					<div class="box-icon-2">
						<div class="icon">
							<div class="fa fa-users"></div>
						</div>
						<div class="body-content">
							<div class="heading">Professional Agents</div>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	 
	<!-- WHY -->
	<div class="section why section-border">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-6 col-md-6">
					<h2 class="section-heading">
						Why Insure?
					</h2>
					<div class="section-subheading">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div> 
					<ul class="checklist">
						<li>100% Secure, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</li>
						<li>Easy to claim, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet.</li>
						<li>More benefit nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
					</ul>
					<a href="#" class="btn btn-primary" title="FIND AN AGENTS">FIND AN AGENTS</a>
					<div class="margin-bottom-30"></div>
				</div>
				<div class="col-sm-6 col-md-6">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/600x400.jpg" alt="" class="img-responsive">
				</div>
				
			</div>
		</div>
	</div>
	 
	<!-- SERVICES -->
	<div class="section services section-border">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading center">
						Our Servcies
					</h2>
				</div>
			</div>
			<div class="row grid-services">
				<div class="col-sm-6 col-md-4">
					<div class="box-image-1">
						<div class="image">
							<a href="services-detail.html" title="House Insurance"><img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive"></a>
						</div>
						<div class="description">
							<h3 class="blok-title">House <br/>Insurance</h3>
							<a href="contact-quote.html" title="GET A QUOTE" class="btn btn-secondary">GET A QUOTE</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box-image-1">
						<div class="image">
							<a href="services-detail.html" title="House Insurance"><img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive"></a>
						</div>
						<div class="description">
							<h3 class="blok-title">Car <br/>Insurance</h3>
							<a href="contact-quote.html" title="GET A QUOTE" class="btn btn-secondary">GET A QUOTE</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box-image-1">
						<div class="image">
							<a href="services-detail.html" title="House Insurance"><img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive"></a>
						</div>
						<div class="description">
							<h3 class="blok-title">Life <br/>Insurance</h3>
							<a href="contact-quote.html" title="GET A QUOTE" class="btn btn-secondary">GET A QUOTE</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box-image-1">
						<div class="image">
							<a href="services-detail.html" title="House Insurance"><img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive"></a>
						</div>
						<div class="description">
							<h3 class="blok-title">Motorcylcle <br/>Insurance</h3>
							<a href="contact-quote.html" title="GET A QUOTE" class="btn btn-secondary">GET A QUOTE</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box-image-1">
						<div class="image">
							<a href="services-detail.html" title="House Insurance"><img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive"></a>
						</div>
						<div class="description">
							<h3 class="blok-title">Boat <br/>Insurance</h3>
							<a href="contact-quote.html" title="GET A QUOTE" class="btn btn-secondary">GET A QUOTE</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box-image-1">
						<div class="image">
							<a href="services-detail.html" title="House Insurance"><img src="<?php bloginfo('template_url'); ?>/assets/images/500x350.jpg" alt="" class="img-responsive"></a>
						</div>
						<div class="description">
							<h3 class="blok-title">Travel <br/>Insurance</h3>
							<a href="contact-quote.html" title="GET A QUOTE" class="btn btn-secondary">GET A QUOTE</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	 
	<!-- How -->
	<div class="section how section-border">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-6 col-md-6">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/600x400.jpg" alt="" class="img-responsive">
				</div>
				<div class="col-sm-6 col-md-6">
					<h2 class="section-heading">
						How It Works
					</h2>
					<div class="section-subheading">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div> 
					<div class="margin-bottom-50"></div>
					<dl class="hiw">
						<dt><span class="fa fa-hand-o-up"></span></dt>
						<dd><h4>Choose Your Insurance</h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
						<dt><span class="fa fa-users"></span></dt>
						<dd><h4>Select Your Agents</h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
						<dt><span class="fa fa-phone"></span></dt>
						<dd><h4>Contact Agents</h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
						
					</dl>
					
				</div>
				
			</div>
		</div>
	</div> 
	
	<!-- TESTIMONY --> 
	<div class="section testimony section-border">
		<div class="container">
			
			<div class="row">
				
				<div class="col-sm-7 col-md-8">

					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="section-title center cwhite">
								<h2 class="section-heading">What People Say</h2>
							</div>
						</div>
					</div>

					<div id="owl-testimony">
						<div class="item">
							<div class="box-testimony">
								<div class="quote-box">
									<blockquote>
									 We specialize in 23 different kinds of pest, and we're also qualified to handle a much wider range of pest control needs. If you have a pest problem that's not covered in our pest library.
									</blockquote>
									<p class="quote-name">
										Johnathan Doel <span>CEO Buka Kreasi</span>
									</p>                        
								</div>
							</div>
						</div>
						<div class="item">
							<div class="box-testimony">
								<div class="quote-box">
									<blockquote>
									 We specialize in 23 different kinds of pest, and we're also qualified to handle a much wider range of pest control needs. If you have a pest problem that's not covered in our pest library.
									</blockquote>
									<p class="quote-name">
										Johnathan Doel <span>CEO Buka Kreasi</span>
									</p>                        
								</div>
							</div>
						</div>
						<div class="item">
							<div class="box-testimony">
								<div class="quote-box">
									<blockquote>
									 We specialize in 23 different kinds of pest, and we're also qualified to handle a much wider range of pest control needs. If you have a pest problem that's not covered in our pest library.
									</blockquote>
									<p class="quote-name">
										Johnathan Doel <span>CEO Buka Kreasi</span>
									</p>                        
								</div>
							</div>
						</div>
						<div class="item">
							<div class="box-testimony">
								<div class="quote-box">
									<blockquote>
									 We specialize in 23 different kinds of pest, and we're also qualified to handle a much wider range of pest control needs. If you have a pest problem that's not covered in our pest library.
									</blockquote>
									<p class="quote-name">
										Johnathan Doel <span>CEO Buka Kreasi</span>
									</p>                        
								</div>
							</div>
						</div>
						
					</div>
					
				</div>

				<div class="col-sm-5 col-md-4">
					<div class="people">
						<img class="user-pic" src="<?php bloginfo('template_url'); ?>/assets/images/say-img.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- CLIENT -->
	<div class="section stat-client">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="section-heading center">
						<h2 class="section-heading">Our Partners</h2>
					</div>
				</div>
			</div>
			<div class="margin-bottom-50"></div>
			<div class="row">
				
				<div class="col-sm-3 col-md-3">
					<div class="client-img">
						<a href="#"><img src="<?php bloginfo('template_url'); ?>/assets/images/partners.png" alt="" class="img-responsive"></a>
					</div>
				</div>

				<div class="col-sm-3 col-md-3">
					<div class="client-img">
						<a href="#"><img src="<?php bloginfo('template_url'); ?>/assets/images/partners.png" alt="" class="img-responsive"></a>
					</div>
				</div>

				<div class="col-sm-3 col-md-3">
					<div class="client-img">
						<a href="#"><img src="<?php bloginfo('template_url'); ?>/assets/images/partners.png" alt="" class="img-responsive"></a>
					</div>
				</div>

				<div class="col-sm-3 col-md-3">
					<div class="client-img">
						<a href="#"><img src="<?php bloginfo('template_url'); ?>/assets/images/partners.png" alt="" class="img-responsive"></a>
					</div>
				</div>

				
			</div>
		</div>
	</div>
	
	<!-- CTA -->
	<div class="section cta">
		<div class="container">
			
			<div class="row">
				
				<div class="col-sm-12 col-md-7 col-md-offset-1">
					<h3 class="cta-desc">Get An Insurance Right Now & Save up to 10%</h3>
				</div>
				<div class="col-sm-12 col-md-3">
					<a href="contact.html" title="" class="btn btn-orange-cta pull-right btn-view-all">GET A QUOTE</a>
				</div>

			</div>
		</div>
	</div>


<?php
get_footer();
