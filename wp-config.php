<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);

define( 'DB_NAME', 'lifecare_lifecareinsurance_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'adminroot' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );


define('FS_METHOD','direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'dvng>9zi1U)[!fZznX{,fFW<GP^A_1;b 6`n<niF*-:Zm`3gmt#-PbH~{vFeUB0?' );
define( 'SECURE_AUTH_KEY',  'EjH/faU6%N. W1k,;0^$^ZuZ:cMs<j7LFt)h3;e|c x~]*[pDZK!}B[6H|cZot=w' );
define( 'LOGGED_IN_KEY',    'sAF;U^|9VN)m]71E{KebWCW]@|3jCAZRO8pVS&%PTJDjYV,-[.`n&Ff}wyXmybQQ' );
define( 'NONCE_KEY',        'z4!Ot6t+7oZ%/,guCP?_McJwwq^~_<+0f<?`^27bUbz*A{{~jcC S7{DFyN1,8=2' );
define( 'AUTH_SALT',        'P=94>Gv#w:eoudB&6mH=/:ygyI1w=N`x(9Ll8LoZFV(@3fxln-|T{d>M[31539c4' );
define( 'SECURE_AUTH_SALT', 'HGAMECA;+Z|=B)ESWl JC`uXDmcX,i/V6 W1<vtNVv,j<=xC9x(^`Eyh&K:9krOB' );
define( 'LOGGED_IN_SALT',   'y^@q#-vN7fHYR^._lH*FoPL/b+~;R.(zW:dH:iVYv@z<SQ%x3>{9TO{Z(RL-CFj)' );
define( 'NONCE_SALT',       'ax>>NlByf!o17O32Ad-;YOG*POLpn3|]LsMYZ05#03#YV_|0vmXq8llET|a{TK:B' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_lci_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
